#import "_StitchTypeProduct.h"

@interface StitchTypeProduct : _StitchTypeProduct {}
// Custom logic goes here.


- (NSString*)imageFilePath;
- (NSString*)sketchFilePath;

- (NSURL*)imageFileURL;
- (NSURL*)sketchFileURL;

- (NSString*)imageLocalFilePath;
- (NSString*)sketchLocalFilePath;

- (NSString*)imageBundledFilePath;
- (NSString*)sketchBundledFilePath;

@end
