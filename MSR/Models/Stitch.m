#import "Stitch.h"


@interface Stitch (){
    BTDataSync *sync;
}

// Private interface goes here.

@end


@implementation Stitch

// Custom logic goes here.
+ (S2SOrderedDictionary*)compositionsFilterValues{
    S2SOrderedDictionary *data = [[S2SOrderedDictionary alloc] init];
    
    [data setObject:@"" forKey:[NSString stringWithFormat:NSLocalizedString(@" -- All %@ --",@" -- Tutti i fornitori -- "),NSLocalizedString(@"compositions", @"compositions")]];
    
    NSSortDescriptor *sortByName = [[NSSortDescriptor alloc] initWithKey:@"composition" ascending:YES];
    NSFetchRequest *f = [Stitch fetchRequest];
    [f setSortDescriptors:[NSArray arrayWithObject:sortByName]];
    
    NSPredicate *p = nil;
    NSMutableString *format = [[NSMutableString alloc] init];
    
    [format appendFormat:@"(language == '%@')",[[BTDataSync shared] language]];
    
    p = [NSPredicate predicateWithFormat:format];
    [f setPredicate:p];
    
    for (Stitch *tmp in [Stitch objectsWithFetchRequest:f])
        [data setObject:tmp.composition forKey:tmp.composition];
    
    return data;
}

- (NSArray*)sortedPictures{
    
    if(!sync)
        sync = [BTDataSync shared];
    
    NSFetchRequest *f = [Picture fetchRequest];
    NSSortDescriptor *sortByName = [[NSSortDescriptor alloc] initWithKey:@"code" ascending:YES];
    [f setSortDescriptors:@[sortByName]];
    
    NSPredicate *p = nil;
    NSMutableString *format = [[NSMutableString alloc] init];
    
    [format appendFormat:@"(stitch_code == %@) AND (language == '%@')",self.code,[sync language]];
    
    p = [NSPredicate predicateWithFormat:format];
    [f setPredicate:p];
    
    return [Picture objectsWithFetchRequest:f];
    
}

- (NSArray*)yarns{
    
    NSPredicate *p = [NSPredicate predicateWithFormat:@"code IN (%@) AND (language == 'it')",self.yarnCodes];
    return [Yarn objectsWithPredicate:p];
}

- (Yarn*)yarn{
    
    NSArray *tmp = [self yarns];
    if(tmp && [tmp count] > 0)
        return [tmp firstObject];
    else
        return nil;
}

- (NSArray*)yarnCodes{
    NSMutableArray *tmp = [[self.yarn_codes componentsSeparatedByString:@"#"] mutableCopy];
    [tmp removeObject:@""];
    
    NSMutableArray *yarns = [[NSMutableArray alloc] init];
    for (NSString *code in tmp)
        [yarns addObject:[NSNumber numberWithInt:[code intValue]]];

    return yarns;
}

- (Supplier*)supplier{
    Supplier *s = nil;
    
    
    if(![self.supplier_codes isEqualToString:@"##"]){
        NSMutableArray *tmp = [[self.supplier_codes componentsSeparatedByString:@"#"] mutableCopy];
        [tmp removeObject:@""];
        
        NSString *code = [tmp firstObject];
        if(code){
            s = [Supplier objectWithPredicate:[NSPredicate predicateWithFormat:@"code = %@",code]];
        }
    }
    
    return s;
}

- (Product*)product{
    Product *s = nil;
    
    
    if(![self.product_codes isEqualToString:@"##"]){
        NSMutableArray *tmp = [[self.product_codes componentsSeparatedByString:@"#"] mutableCopy];
        [tmp removeObject:@""];
        
        NSString *code = [tmp firstObject];
        if(code){
            s = [Product objectWithPredicate:[NSPredicate predicateWithFormat:@"code = %@",code]];
        }
    }
    
    return s;
}

// Custom logic goes here.

- (BOOL)hasGenericPictureFile{
    
    Picture *p = [self genericPicture];
    
    if(!p)
        return NO;
    else{
        
        if(p.filePath != nil && [p.filePath rangeOfString:@"http"].location != NSNotFound)
            return YES;
        else
            return [[NSFileManager defaultManager] fileExistsAtPath:p.filePath];
        
        
    }

}

- (BOOL)hasDetailPictureFile{
    
    Picture *p = [self detailPicture];
    
    if(!p)
        return NO;
    else{
        
        if(p.filePath != nil && [p.filePath rangeOfString:@"http"].location != NSNotFound)
            return YES;
        else
            return [[NSFileManager defaultManager] fileExistsAtPath:p.filePath];
    }
}

- (Picture*)genericPicture{
    NSArray *pics = [self.pictures allObjects];
    
    if(pics && [pics count] > 0){
        for (Picture *p in pics) {
            if(!p.detail_pictureValue)
                return p;
        }
    }

    return nil;
}

- (Picture*)detailPicture{
    NSArray *pics = [self.pictures allObjects];
    
    if(pics && [pics count] > 0){
        for (Picture *p in pics) {
            if(p.detail_pictureValue)
                return p;
        }
    }

    return nil;

}

- (NSString*)genericPicturePath{
    Picture *p = [self genericPicture];
    return (!p ? nil : p.filePath);
}

- (NSString*)detailPicturePath{
    Picture *p = [self detailPicture];
    return (!p ? nil : p.filePath);
}

- (NSURL*)genericPictureURL{
    Picture *p = [self genericPicture];
    return p.fileURL;
}

- (NSURL*)detailPictureURL{
    Picture *p = [self detailPicture];
    return p.fileURL;
}

@end
