// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Stitch.m instead.

#import "_Stitch.h"

const struct StitchAttributes StitchAttributes = {
	.brand_code = @"brand_code",
	.code = @"code",
	.composition = @"composition",
	.created_at = @"created_at",
	.item_description = @"item_description",
	.language = @"language",
	.machine_code = @"machine_code",
	.product_codes = @"product_codes",
	.season_code = @"season_code",
	.stitch_type_code = @"stitch_type_code",
	.stitch_type_codes = @"stitch_type_codes",
	.stretch = @"stretch",
	.string_code = @"string_code",
	.supplier_codes = @"supplier_codes",
	.thinness = @"thinness",
	.updated_at = @"updated_at",
	.updated_images = @"updated_images",
	.vanise = @"vanise",
	.yarn_codes = @"yarn_codes",
	.year = @"year",
};

const struct StitchRelationships StitchRelationships = {
	.brand = @"brand",
	.machine = @"machine",
	.pictures = @"pictures",
	.season = @"season",
	.stitch_type = @"stitch_type",
};

const struct StitchFetchedProperties StitchFetchedProperties = {
};

@implementation StitchID
@end

@implementation _Stitch

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Stitch" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Stitch";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Stitch" inManagedObjectContext:moc_];
}

- (StitchID*)objectID {
	return (StitchID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"brand_codeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"brand_code"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"codeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"code"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"machine_codeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"machine_code"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"stitch_type_codeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"stitch_type_code"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"stretchValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"stretch"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"thinnessValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"thinness"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"updated_imagesValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"updated_images"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"vaniseValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"vanise"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"yearValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"year"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic brand_code;



- (int16_t)brand_codeValue {
	NSNumber *result = [self brand_code];
	return [result shortValue];
}

- (void)setBrand_codeValue:(int16_t)value_ {
	[self setBrand_code:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveBrand_codeValue {
	NSNumber *result = [self primitiveBrand_code];
	return [result shortValue];
}

- (void)setPrimitiveBrand_codeValue:(int16_t)value_ {
	[self setPrimitiveBrand_code:[NSNumber numberWithShort:value_]];
}





@dynamic code;



- (int16_t)codeValue {
	NSNumber *result = [self code];
	return [result shortValue];
}

- (void)setCodeValue:(int16_t)value_ {
	[self setCode:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveCodeValue {
	NSNumber *result = [self primitiveCode];
	return [result shortValue];
}

- (void)setPrimitiveCodeValue:(int16_t)value_ {
	[self setPrimitiveCode:[NSNumber numberWithShort:value_]];
}





@dynamic composition;






@dynamic created_at;






@dynamic item_description;






@dynamic language;






@dynamic machine_code;



- (int16_t)machine_codeValue {
	NSNumber *result = [self machine_code];
	return [result shortValue];
}

- (void)setMachine_codeValue:(int16_t)value_ {
	[self setMachine_code:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveMachine_codeValue {
	NSNumber *result = [self primitiveMachine_code];
	return [result shortValue];
}

- (void)setPrimitiveMachine_codeValue:(int16_t)value_ {
	[self setPrimitiveMachine_code:[NSNumber numberWithShort:value_]];
}





@dynamic product_codes;






@dynamic season_code;






@dynamic stitch_type_code;



- (int16_t)stitch_type_codeValue {
	NSNumber *result = [self stitch_type_code];
	return [result shortValue];
}

- (void)setStitch_type_codeValue:(int16_t)value_ {
	[self setStitch_type_code:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveStitch_type_codeValue {
	NSNumber *result = [self primitiveStitch_type_code];
	return [result shortValue];
}

- (void)setPrimitiveStitch_type_codeValue:(int16_t)value_ {
	[self setPrimitiveStitch_type_code:[NSNumber numberWithShort:value_]];
}





@dynamic stitch_type_codes;






@dynamic stretch;



- (BOOL)stretchValue {
	NSNumber *result = [self stretch];
	return [result boolValue];
}

- (void)setStretchValue:(BOOL)value_ {
	[self setStretch:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveStretchValue {
	NSNumber *result = [self primitiveStretch];
	return [result boolValue];
}

- (void)setPrimitiveStretchValue:(BOOL)value_ {
	[self setPrimitiveStretch:[NSNumber numberWithBool:value_]];
}





@dynamic string_code;






@dynamic supplier_codes;






@dynamic thinness;



- (int16_t)thinnessValue {
	NSNumber *result = [self thinness];
	return [result shortValue];
}

- (void)setThinnessValue:(int16_t)value_ {
	[self setThinness:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveThinnessValue {
	NSNumber *result = [self primitiveThinness];
	return [result shortValue];
}

- (void)setPrimitiveThinnessValue:(int16_t)value_ {
	[self setPrimitiveThinness:[NSNumber numberWithShort:value_]];
}





@dynamic updated_at;






@dynamic updated_images;



- (BOOL)updated_imagesValue {
	NSNumber *result = [self updated_images];
	return [result boolValue];
}

- (void)setUpdated_imagesValue:(BOOL)value_ {
	[self setUpdated_images:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveUpdated_imagesValue {
	NSNumber *result = [self primitiveUpdated_images];
	return [result boolValue];
}

- (void)setPrimitiveUpdated_imagesValue:(BOOL)value_ {
	[self setPrimitiveUpdated_images:[NSNumber numberWithBool:value_]];
}





@dynamic vanise;



- (BOOL)vaniseValue {
	NSNumber *result = [self vanise];
	return [result boolValue];
}

- (void)setVaniseValue:(BOOL)value_ {
	[self setVanise:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveVaniseValue {
	NSNumber *result = [self primitiveVanise];
	return [result boolValue];
}

- (void)setPrimitiveVaniseValue:(BOOL)value_ {
	[self setPrimitiveVanise:[NSNumber numberWithBool:value_]];
}





@dynamic yarn_codes;






@dynamic year;



- (int16_t)yearValue {
	NSNumber *result = [self year];
	return [result shortValue];
}

- (void)setYearValue:(int16_t)value_ {
	[self setYear:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveYearValue {
	NSNumber *result = [self primitiveYear];
	return [result shortValue];
}

- (void)setPrimitiveYearValue:(int16_t)value_ {
	[self setPrimitiveYear:[NSNumber numberWithShort:value_]];
}





@dynamic brand;

	

@dynamic machine;

	

@dynamic pictures;

	
- (NSMutableSet*)picturesSet {
	[self willAccessValueForKey:@"pictures"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"pictures"];
  
	[self didAccessValueForKey:@"pictures"];
	return result;
}
	

@dynamic season;

	

@dynamic stitch_type;

	






@end
