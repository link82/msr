// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Stitch.h instead.

#import <CoreData/CoreData.h>


extern const struct StitchAttributes {
	__unsafe_unretained NSString *brand_code;
	__unsafe_unretained NSString *code;
	__unsafe_unretained NSString *composition;
	__unsafe_unretained NSString *created_at;
	__unsafe_unretained NSString *item_description;
	__unsafe_unretained NSString *language;
	__unsafe_unretained NSString *machine_code;
	__unsafe_unretained NSString *product_codes;
	__unsafe_unretained NSString *season_code;
	__unsafe_unretained NSString *stitch_type_code;
	__unsafe_unretained NSString *stitch_type_codes;
	__unsafe_unretained NSString *stretch;
	__unsafe_unretained NSString *string_code;
	__unsafe_unretained NSString *supplier_codes;
	__unsafe_unretained NSString *thinness;
	__unsafe_unretained NSString *updated_at;
	__unsafe_unretained NSString *updated_images;
	__unsafe_unretained NSString *vanise;
	__unsafe_unretained NSString *yarn_codes;
	__unsafe_unretained NSString *year;
} StitchAttributes;

extern const struct StitchRelationships {
	__unsafe_unretained NSString *brand;
	__unsafe_unretained NSString *machine;
	__unsafe_unretained NSString *pictures;
	__unsafe_unretained NSString *season;
	__unsafe_unretained NSString *stitch_type;
} StitchRelationships;

extern const struct StitchFetchedProperties {
} StitchFetchedProperties;

@class Brand;
@class Machine;
@class Picture;
@class MacroSeason;
@class StitchType;






















@interface StitchID : NSManagedObjectID {}
@end

@interface _Stitch : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (StitchID*)objectID;





@property (nonatomic, strong) NSNumber* brand_code;



@property int16_t brand_codeValue;
- (int16_t)brand_codeValue;
- (void)setBrand_codeValue:(int16_t)value_;

//- (BOOL)validateBrand_code:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* code;



@property int16_t codeValue;
- (int16_t)codeValue;
- (void)setCodeValue:(int16_t)value_;

//- (BOOL)validateCode:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* composition;



//- (BOOL)validateComposition:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* created_at;



//- (BOOL)validateCreated_at:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* item_description;



//- (BOOL)validateItem_description:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* language;



//- (BOOL)validateLanguage:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* machine_code;



@property int16_t machine_codeValue;
- (int16_t)machine_codeValue;
- (void)setMachine_codeValue:(int16_t)value_;

//- (BOOL)validateMachine_code:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* product_codes;



//- (BOOL)validateProduct_codes:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* season_code;



//- (BOOL)validateSeason_code:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* stitch_type_code;



@property int16_t stitch_type_codeValue;
- (int16_t)stitch_type_codeValue;
- (void)setStitch_type_codeValue:(int16_t)value_;

//- (BOOL)validateStitch_type_code:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* stitch_type_codes;



//- (BOOL)validateStitch_type_codes:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* stretch;



@property BOOL stretchValue;
- (BOOL)stretchValue;
- (void)setStretchValue:(BOOL)value_;

//- (BOOL)validateStretch:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* string_code;



//- (BOOL)validateString_code:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* supplier_codes;



//- (BOOL)validateSupplier_codes:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* thinness;



@property int16_t thinnessValue;
- (int16_t)thinnessValue;
- (void)setThinnessValue:(int16_t)value_;

//- (BOOL)validateThinness:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* updated_at;



//- (BOOL)validateUpdated_at:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* updated_images;



@property BOOL updated_imagesValue;
- (BOOL)updated_imagesValue;
- (void)setUpdated_imagesValue:(BOOL)value_;

//- (BOOL)validateUpdated_images:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* vanise;



@property BOOL vaniseValue;
- (BOOL)vaniseValue;
- (void)setVaniseValue:(BOOL)value_;

//- (BOOL)validateVanise:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* yarn_codes;



//- (BOOL)validateYarn_codes:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* year;



@property int16_t yearValue;
- (int16_t)yearValue;
- (void)setYearValue:(int16_t)value_;

//- (BOOL)validateYear:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) Brand *brand;

//- (BOOL)validateBrand:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) Machine *machine;

//- (BOOL)validateMachine:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) NSSet *pictures;

- (NSMutableSet*)picturesSet;




@property (nonatomic, strong) MacroSeason *season;

//- (BOOL)validateSeason:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) StitchType *stitch_type;

//- (BOOL)validateStitch_type:(id*)value_ error:(NSError**)error_;





@end

@interface _Stitch (CoreDataGeneratedAccessors)

- (void)addPictures:(NSSet*)value_;
- (void)removePictures:(NSSet*)value_;
- (void)addPicturesObject:(Picture*)value_;
- (void)removePicturesObject:(Picture*)value_;

@end

@interface _Stitch (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveBrand_code;
- (void)setPrimitiveBrand_code:(NSNumber*)value;

- (int16_t)primitiveBrand_codeValue;
- (void)setPrimitiveBrand_codeValue:(int16_t)value_;




- (NSNumber*)primitiveCode;
- (void)setPrimitiveCode:(NSNumber*)value;

- (int16_t)primitiveCodeValue;
- (void)setPrimitiveCodeValue:(int16_t)value_;




- (NSString*)primitiveComposition;
- (void)setPrimitiveComposition:(NSString*)value;




- (NSDate*)primitiveCreated_at;
- (void)setPrimitiveCreated_at:(NSDate*)value;




- (NSString*)primitiveItem_description;
- (void)setPrimitiveItem_description:(NSString*)value;




- (NSString*)primitiveLanguage;
- (void)setPrimitiveLanguage:(NSString*)value;




- (NSNumber*)primitiveMachine_code;
- (void)setPrimitiveMachine_code:(NSNumber*)value;

- (int16_t)primitiveMachine_codeValue;
- (void)setPrimitiveMachine_codeValue:(int16_t)value_;




- (NSString*)primitiveProduct_codes;
- (void)setPrimitiveProduct_codes:(NSString*)value;




- (NSString*)primitiveSeason_code;
- (void)setPrimitiveSeason_code:(NSString*)value;




- (NSNumber*)primitiveStitch_type_code;
- (void)setPrimitiveStitch_type_code:(NSNumber*)value;

- (int16_t)primitiveStitch_type_codeValue;
- (void)setPrimitiveStitch_type_codeValue:(int16_t)value_;




- (NSString*)primitiveStitch_type_codes;
- (void)setPrimitiveStitch_type_codes:(NSString*)value;




- (NSNumber*)primitiveStretch;
- (void)setPrimitiveStretch:(NSNumber*)value;

- (BOOL)primitiveStretchValue;
- (void)setPrimitiveStretchValue:(BOOL)value_;




- (NSString*)primitiveString_code;
- (void)setPrimitiveString_code:(NSString*)value;




- (NSString*)primitiveSupplier_codes;
- (void)setPrimitiveSupplier_codes:(NSString*)value;




- (NSNumber*)primitiveThinness;
- (void)setPrimitiveThinness:(NSNumber*)value;

- (int16_t)primitiveThinnessValue;
- (void)setPrimitiveThinnessValue:(int16_t)value_;




- (NSDate*)primitiveUpdated_at;
- (void)setPrimitiveUpdated_at:(NSDate*)value;




- (NSNumber*)primitiveUpdated_images;
- (void)setPrimitiveUpdated_images:(NSNumber*)value;

- (BOOL)primitiveUpdated_imagesValue;
- (void)setPrimitiveUpdated_imagesValue:(BOOL)value_;




- (NSNumber*)primitiveVanise;
- (void)setPrimitiveVanise:(NSNumber*)value;

- (BOOL)primitiveVaniseValue;
- (void)setPrimitiveVaniseValue:(BOOL)value_;




- (NSString*)primitiveYarn_codes;
- (void)setPrimitiveYarn_codes:(NSString*)value;




- (NSNumber*)primitiveYear;
- (void)setPrimitiveYear:(NSNumber*)value;

- (int16_t)primitiveYearValue;
- (void)setPrimitiveYearValue:(int16_t)value_;





- (Brand*)primitiveBrand;
- (void)setPrimitiveBrand:(Brand*)value;



- (Machine*)primitiveMachine;
- (void)setPrimitiveMachine:(Machine*)value;



- (NSMutableSet*)primitivePictures;
- (void)setPrimitivePictures:(NSMutableSet*)value;



- (MacroSeason*)primitiveSeason;
- (void)setPrimitiveSeason:(MacroSeason*)value;



- (StitchType*)primitiveStitch_type;
- (void)setPrimitiveStitch_type:(StitchType*)value;


@end
