#import "Brand.h"


@interface Brand ()

// Private interface goes here.

@end


@implementation Brand

// Custom logic goes here.
+ (S2SOrderedDictionary*)filterValues{
    S2SOrderedDictionary *data = [[S2SOrderedDictionary alloc] init];
    
    [data setObject:@"" forKey:[NSString stringWithFormat:NSLocalizedString(@" -- All %@ --",@" -- Tutti i fornitori -- "),NSLocalizedString(@"brands", @"brands")]];
    
    NSSortDescriptor *sortByName = [[NSSortDescriptor alloc] initWithKey:@"item_description" ascending:YES];
    NSFetchRequest *f = [Brand fetchRequest];
    [f setSortDescriptors:[NSArray arrayWithObject:sortByName]];
    
    
    for (Brand *tmp in [Brand objectsWithFetchRequest:f]){
        NSLog(@"Code: %@ - Name: %@",tmp.code.stringValue,tmp.item_description);
        [data setObject:tmp.code.stringValue forKey:tmp.item_description];
    }
    
    
    return data;
}

@end
