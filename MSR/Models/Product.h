#import "_Product.h"
#import "S2SOrderedDictionary.h"

@interface Product : _Product {}
// Custom logic goes here.

+ (S2SOrderedDictionary*)filterValues;

@end
