#import "Supplier.h"


@interface Supplier ()

// Private interface goes here.

@end


@implementation Supplier

// Custom logic goes here.
+ (S2SOrderedDictionary*)filterValues{
    S2SOrderedDictionary *data = [[S2SOrderedDictionary alloc] init];
    
    [data setObject:@"" forKey:[NSString stringWithFormat:NSLocalizedString(@" -- All %@ --",@" -- Tutti i fornitori -- "),NSLocalizedString(@"suppliers", @"suppliers")]];
    
    NSSortDescriptor *sortByName = [[NSSortDescriptor alloc] initWithKey:@"item_description" ascending:YES];
    NSFetchRequest *f = [Supplier fetchRequest];
    [f setSortDescriptors:[NSArray arrayWithObject:sortByName]];
    
    
    for (Supplier *tmp in [Supplier objectsWithFetchRequest:f])
        [data setObject:tmp.code.stringValue forKey:tmp.item_description];
    
    return data;
}

- (NSString *)cleanedDescription{
    
    return [[[[[self.item_description stringByReplacingOccurrencesOfString:@"-non usare" withString:@""] stringByReplacingOccurrencesOfString:@"-NON USARE" withString:@""] stringByReplacingOccurrencesOfString:@"  " withString:@""]stringByReplacingOccurrencesOfString:@"\t" withString:@""] uppercaseString];
}

@end
