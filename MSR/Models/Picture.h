#import "_Picture.h"

@interface Picture : _Picture {}
// Custom logic goes here.

- (NSString*)filePath;
- (NSURL*)fileURL;
- (NSString*)localFilePath;
- (NSString*)bundledFilePath;

@end
