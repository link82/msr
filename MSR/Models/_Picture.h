// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Picture.h instead.

#import <CoreData/CoreData.h>


extern const struct PictureAttributes {
	__unsafe_unretained NSString *code;
	__unsafe_unretained NSString *created_at;
	__unsafe_unretained NSString *detail_picture;
	__unsafe_unretained NSString *item_description;
	__unsafe_unretained NSString *language;
	__unsafe_unretained NSString *stitch_code;
	__unsafe_unretained NSString *updated;
	__unsafe_unretained NSString *updated_at;
	__unsafe_unretained NSString *url;
} PictureAttributes;

extern const struct PictureRelationships {
	__unsafe_unretained NSString *stitch;
} PictureRelationships;

extern const struct PictureFetchedProperties {
} PictureFetchedProperties;

@class Stitch;











@interface PictureID : NSManagedObjectID {}
@end

@interface _Picture : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (PictureID*)objectID;





@property (nonatomic, strong) NSNumber* code;



@property int16_t codeValue;
- (int16_t)codeValue;
- (void)setCodeValue:(int16_t)value_;

//- (BOOL)validateCode:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* created_at;



//- (BOOL)validateCreated_at:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* detail_picture;



@property BOOL detail_pictureValue;
- (BOOL)detail_pictureValue;
- (void)setDetail_pictureValue:(BOOL)value_;

//- (BOOL)validateDetail_picture:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* item_description;



//- (BOOL)validateItem_description:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* language;



//- (BOOL)validateLanguage:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* stitch_code;



@property int16_t stitch_codeValue;
- (int16_t)stitch_codeValue;
- (void)setStitch_codeValue:(int16_t)value_;

//- (BOOL)validateStitch_code:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* updated;



@property BOOL updatedValue;
- (BOOL)updatedValue;
- (void)setUpdatedValue:(BOOL)value_;

//- (BOOL)validateUpdated:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* updated_at;



//- (BOOL)validateUpdated_at:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* url;



//- (BOOL)validateUrl:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) Stitch *stitch;

//- (BOOL)validateStitch:(id*)value_ error:(NSError**)error_;





@end

@interface _Picture (CoreDataGeneratedAccessors)

@end

@interface _Picture (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveCode;
- (void)setPrimitiveCode:(NSNumber*)value;

- (int16_t)primitiveCodeValue;
- (void)setPrimitiveCodeValue:(int16_t)value_;




- (NSDate*)primitiveCreated_at;
- (void)setPrimitiveCreated_at:(NSDate*)value;




- (NSNumber*)primitiveDetail_picture;
- (void)setPrimitiveDetail_picture:(NSNumber*)value;

- (BOOL)primitiveDetail_pictureValue;
- (void)setPrimitiveDetail_pictureValue:(BOOL)value_;




- (NSString*)primitiveItem_description;
- (void)setPrimitiveItem_description:(NSString*)value;




- (NSString*)primitiveLanguage;
- (void)setPrimitiveLanguage:(NSString*)value;




- (NSNumber*)primitiveStitch_code;
- (void)setPrimitiveStitch_code:(NSNumber*)value;

- (int16_t)primitiveStitch_codeValue;
- (void)setPrimitiveStitch_codeValue:(int16_t)value_;




- (NSNumber*)primitiveUpdated;
- (void)setPrimitiveUpdated:(NSNumber*)value;

- (BOOL)primitiveUpdatedValue;
- (void)setPrimitiveUpdatedValue:(BOOL)value_;




- (NSDate*)primitiveUpdated_at;
- (void)setPrimitiveUpdated_at:(NSDate*)value;




- (NSString*)primitiveUrl;
- (void)setPrimitiveUrl:(NSString*)value;





- (Stitch*)primitiveStitch;
- (void)setPrimitiveStitch:(Stitch*)value;


@end
