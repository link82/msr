// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Yarn.h instead.

#import <CoreData/CoreData.h>


extern const struct YarnAttributes {
	__unsafe_unretained NSString *code;
	__unsafe_unretained NSString *created_at;
	__unsafe_unretained NSString *item_description;
	__unsafe_unretained NSString *language;
	__unsafe_unretained NSString *string_code;
	__unsafe_unretained NSString *supplier_code;
	__unsafe_unretained NSString *supplier_name;
	__unsafe_unretained NSString *updated_at;
} YarnAttributes;

extern const struct YarnRelationships {
	__unsafe_unretained NSString *supplier;
} YarnRelationships;

extern const struct YarnFetchedProperties {
} YarnFetchedProperties;

@class Supplier;










@interface YarnID : NSManagedObjectID {}
@end

@interface _Yarn : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (YarnID*)objectID;





@property (nonatomic, strong) NSNumber* code;



@property int16_t codeValue;
- (int16_t)codeValue;
- (void)setCodeValue:(int16_t)value_;

//- (BOOL)validateCode:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* created_at;



//- (BOOL)validateCreated_at:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* item_description;



//- (BOOL)validateItem_description:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* language;



//- (BOOL)validateLanguage:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* string_code;



//- (BOOL)validateString_code:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* supplier_code;



//- (BOOL)validateSupplier_code:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* supplier_name;



//- (BOOL)validateSupplier_name:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* updated_at;



//- (BOOL)validateUpdated_at:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) Supplier *supplier;

//- (BOOL)validateSupplier:(id*)value_ error:(NSError**)error_;





@end

@interface _Yarn (CoreDataGeneratedAccessors)

@end

@interface _Yarn (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveCode;
- (void)setPrimitiveCode:(NSNumber*)value;

- (int16_t)primitiveCodeValue;
- (void)setPrimitiveCodeValue:(int16_t)value_;




- (NSDate*)primitiveCreated_at;
- (void)setPrimitiveCreated_at:(NSDate*)value;




- (NSString*)primitiveItem_description;
- (void)setPrimitiveItem_description:(NSString*)value;




- (NSString*)primitiveLanguage;
- (void)setPrimitiveLanguage:(NSString*)value;




- (NSString*)primitiveString_code;
- (void)setPrimitiveString_code:(NSString*)value;




- (NSString*)primitiveSupplier_code;
- (void)setPrimitiveSupplier_code:(NSString*)value;




- (NSString*)primitiveSupplier_name;
- (void)setPrimitiveSupplier_name:(NSString*)value;




- (NSDate*)primitiveUpdated_at;
- (void)setPrimitiveUpdated_at:(NSDate*)value;





- (Supplier*)primitiveSupplier;
- (void)setPrimitiveSupplier:(Supplier*)value;


@end
