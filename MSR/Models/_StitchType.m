// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to StitchType.m instead.

#import "_StitchType.h"

const struct StitchTypeAttributes StitchTypeAttributes = {
	.code = @"code",
	.created_at = @"created_at",
	.full_description = @"full_description",
	.item_description = @"item_description",
	.language = @"language",
	.leaf = @"leaf",
	.parent_code = @"parent_code",
	.string_code = @"string_code",
	.updated_at = @"updated_at",
};

const struct StitchTypeRelationships StitchTypeRelationships = {
	.childrens = @"childrens",
	.parent = @"parent",
	.products = @"products",
	.stitches = @"stitches",
};

const struct StitchTypeFetchedProperties StitchTypeFetchedProperties = {
};

@implementation StitchTypeID
@end

@implementation _StitchType

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"StitchType" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"StitchType";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"StitchType" inManagedObjectContext:moc_];
}

- (StitchTypeID*)objectID {
	return (StitchTypeID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"codeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"code"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"leafValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"leaf"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"parent_codeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"parent_code"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic code;



- (int16_t)codeValue {
	NSNumber *result = [self code];
	return [result shortValue];
}

- (void)setCodeValue:(int16_t)value_ {
	[self setCode:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveCodeValue {
	NSNumber *result = [self primitiveCode];
	return [result shortValue];
}

- (void)setPrimitiveCodeValue:(int16_t)value_ {
	[self setPrimitiveCode:[NSNumber numberWithShort:value_]];
}





@dynamic created_at;






@dynamic full_description;






@dynamic item_description;






@dynamic language;






@dynamic leaf;



- (BOOL)leafValue {
	NSNumber *result = [self leaf];
	return [result boolValue];
}

- (void)setLeafValue:(BOOL)value_ {
	[self setLeaf:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveLeafValue {
	NSNumber *result = [self primitiveLeaf];
	return [result boolValue];
}

- (void)setPrimitiveLeafValue:(BOOL)value_ {
	[self setPrimitiveLeaf:[NSNumber numberWithBool:value_]];
}





@dynamic parent_code;



- (int16_t)parent_codeValue {
	NSNumber *result = [self parent_code];
	return [result shortValue];
}

- (void)setParent_codeValue:(int16_t)value_ {
	[self setParent_code:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveParent_codeValue {
	NSNumber *result = [self primitiveParent_code];
	return [result shortValue];
}

- (void)setPrimitiveParent_codeValue:(int16_t)value_ {
	[self setPrimitiveParent_code:[NSNumber numberWithShort:value_]];
}





@dynamic string_code;






@dynamic updated_at;






@dynamic childrens;

	
- (NSMutableSet*)childrensSet {
	[self willAccessValueForKey:@"childrens"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"childrens"];
  
	[self didAccessValueForKey:@"childrens"];
	return result;
}
	

@dynamic parent;

	

@dynamic products;

	
- (NSMutableSet*)productsSet {
	[self willAccessValueForKey:@"products"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"products"];
  
	[self didAccessValueForKey:@"products"];
	return result;
}
	

@dynamic stitches;

	
- (NSMutableSet*)stitchesSet {
	[self willAccessValueForKey:@"stitches"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"stitches"];
  
	[self didAccessValueForKey:@"stitches"];
	return result;
}
	






@end
