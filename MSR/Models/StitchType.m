#import "StitchType.h"


@interface StitchType ()

// Private interface goes here.

@end


@implementation StitchType

// Custom logic goes here.

+ (NSArray*)rootObjects{
    NSFetchRequest *f = [StitchType fetchRequest];
    NSPredicate *p = nil;
    NSMutableString *format = [[NSMutableString alloc] init];
    
    [format appendFormat:@"(parent_code == %@) AND (code > 0) AND (language == '%@')",nil,[[BTDataSync shared] language]];
    
    p = [NSPredicate predicateWithFormat:format];
    [f setPredicate:p];
    
    return [StitchType objectsWithFetchRequest:f];
}


@end
