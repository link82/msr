#import "_Machine.h"
#import "S2SOrderedDictionary.h"

@interface Machine : _Machine {}
// Custom logic goes here.

+ (S2SOrderedDictionary*)filterValues;

@end
