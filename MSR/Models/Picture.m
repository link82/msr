#import "Picture.h"


@interface Picture ()

// Private interface goes here.
+ (NSString*)baseDirectory;

@end


@implementation Picture
 


+ (NSString*)baseDirectory{
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    return [NSString stringWithFormat:@"%@/stitches",documentsDirectory];
}

// Custom logic goes here.
- (NSString*)remoteFilePath{
    NSString *baseURL = [[BTDataSync shared] apiURL];
    NSString *path = [NSString stringWithFormat:@"%@/system/images/medium/%d.jpg",baseURL,self.codeValue];
    
    return path;
}


- (NSString*)localFilePath{
    NSString *path = [[Picture baseDirectory] stringByAppendingFormat:@"/%d.jpg",self.codeValue];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:NO])
        return path;
    else
        return nil;
}

- (NSString*)bundledFilePath{
    NSString *file_name = [[[[self.url componentsSeparatedByString:@"/"] lastObject] componentsSeparatedByString:@"?"] firstObject];
    NSString *path = [[[NSBundle mainBundle] bundlePath] stringByAppendingFormat:@"/images.bundle/%@",file_name];

    if([[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:NO])
        return path;
    else
        return nil;
}

- (NSString*)filePath{
    NSString *path = nil;
    
    if([[BTDataSync shared] forceRemoteAssets]){
        path = [self remoteFilePath];
    }
    
    if(!path)
        path = [self localFilePath];
    
    if(!path)
        path = [self bundledFilePath];
    
    if(!path)
        path = [self remoteFilePath];
    
    return path;
}

- (NSURL*)fileURL{
    NSString *path = nil;
    
    path = [self filePath];
    
    if(path){
        if([path rangeOfString:@"http"].location != NSNotFound)
            return [NSURL URLWithString:path];
        else
            return [NSURL fileURLWithPath:path];
    }
    else
        return nil;

}

@end
