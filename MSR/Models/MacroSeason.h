#import "_MacroSeason.h"
#import "S2SOrderedDictionary.h"

@interface MacroSeason : _MacroSeason {}
// Custom logic goes here.

+ (S2SOrderedDictionary*)filterValues;

@end
