#import "_StitchType.h"
#import "NSManagedObject+Additions.h"

@interface StitchType : _StitchType {}
// Custom logic goes here.

+ (NSArray*)rootObjects;

@end
