// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to StitchTypeProduct.m instead.

#import "_StitchTypeProduct.h"

const struct StitchTypeProductAttributes StitchTypeProductAttributes = {
	.brand = @"brand",
	.category = @"category",
	.code = @"code",
	.created_at = @"created_at",
	.image_url = @"image_url",
	.item_description = @"item_description",
	.language = @"language",
	.model = @"model",
	.product_code = @"product_code",
	.season = @"season",
	.sketch_url = @"sketch_url",
	.stitch_type_code = @"stitch_type_code",
	.stitch_type_string_code = @"stitch_type_string_code",
	.updated_at = @"updated_at",
	.updated_picture = @"updated_picture",
	.updated_sketch = @"updated_sketch",
	.year = @"year",
};

const struct StitchTypeProductRelationships StitchTypeProductRelationships = {
	.stitch_type = @"stitch_type",
};

const struct StitchTypeProductFetchedProperties StitchTypeProductFetchedProperties = {
};

@implementation StitchTypeProductID
@end

@implementation _StitchTypeProduct

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"StitchTypeProduct" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"StitchTypeProduct";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"StitchTypeProduct" inManagedObjectContext:moc_];
}

- (StitchTypeProductID*)objectID {
	return (StitchTypeProductID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"codeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"code"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"stitch_type_codeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"stitch_type_code"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"updated_pictureValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"updated_picture"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"updated_sketchValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"updated_sketch"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"yearValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"year"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic brand;






@dynamic category;






@dynamic code;



- (int16_t)codeValue {
	NSNumber *result = [self code];
	return [result shortValue];
}

- (void)setCodeValue:(int16_t)value_ {
	[self setCode:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveCodeValue {
	NSNumber *result = [self primitiveCode];
	return [result shortValue];
}

- (void)setPrimitiveCodeValue:(int16_t)value_ {
	[self setPrimitiveCode:[NSNumber numberWithShort:value_]];
}





@dynamic created_at;






@dynamic image_url;






@dynamic item_description;






@dynamic language;






@dynamic model;






@dynamic product_code;






@dynamic season;






@dynamic sketch_url;






@dynamic stitch_type_code;



- (int16_t)stitch_type_codeValue {
	NSNumber *result = [self stitch_type_code];
	return [result shortValue];
}

- (void)setStitch_type_codeValue:(int16_t)value_ {
	[self setStitch_type_code:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveStitch_type_codeValue {
	NSNumber *result = [self primitiveStitch_type_code];
	return [result shortValue];
}

- (void)setPrimitiveStitch_type_codeValue:(int16_t)value_ {
	[self setPrimitiveStitch_type_code:[NSNumber numberWithShort:value_]];
}





@dynamic stitch_type_string_code;






@dynamic updated_at;






@dynamic updated_picture;



- (BOOL)updated_pictureValue {
	NSNumber *result = [self updated_picture];
	return [result boolValue];
}

- (void)setUpdated_pictureValue:(BOOL)value_ {
	[self setUpdated_picture:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveUpdated_pictureValue {
	NSNumber *result = [self primitiveUpdated_picture];
	return [result boolValue];
}

- (void)setPrimitiveUpdated_pictureValue:(BOOL)value_ {
	[self setPrimitiveUpdated_picture:[NSNumber numberWithBool:value_]];
}





@dynamic updated_sketch;



- (BOOL)updated_sketchValue {
	NSNumber *result = [self updated_sketch];
	return [result boolValue];
}

- (void)setUpdated_sketchValue:(BOOL)value_ {
	[self setUpdated_sketch:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveUpdated_sketchValue {
	NSNumber *result = [self primitiveUpdated_sketch];
	return [result boolValue];
}

- (void)setPrimitiveUpdated_sketchValue:(BOOL)value_ {
	[self setPrimitiveUpdated_sketch:[NSNumber numberWithBool:value_]];
}





@dynamic year;



- (int16_t)yearValue {
	NSNumber *result = [self year];
	return [result shortValue];
}

- (void)setYearValue:(int16_t)value_ {
	[self setYear:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveYearValue {
	NSNumber *result = [self primitiveYear];
	return [result shortValue];
}

- (void)setPrimitiveYearValue:(int16_t)value_ {
	[self setPrimitiveYear:[NSNumber numberWithShort:value_]];
}





@dynamic stitch_type;

	






@end
