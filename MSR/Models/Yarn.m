#import "Yarn.h"


@interface Yarn ()

// Private interface goes here.

@end


@implementation Yarn

// Custom logic goes here.
+ (S2SOrderedDictionary*)filterValues{
    S2SOrderedDictionary *data = [[S2SOrderedDictionary alloc] init];
    

    [data setObject:@"" forKey:[NSString stringWithFormat:NSLocalizedString(@" -- All %@ --",@" -- Tutti i fornitori -- "),NSLocalizedString(@"yarns", @"yarns")]];
    
    NSSortDescriptor *sortByName = [[NSSortDescriptor alloc] initWithKey:@"item_description" ascending:YES];
    NSFetchRequest *f = [Yarn fetchRequest];
    [f setSortDescriptors:[NSArray arrayWithObject:sortByName]];
    
    for (Yarn *tmp in [Yarn objectsWithFetchRequest:f]){
        [data setObject:tmp.code.stringValue forKey:tmp.item_description];
    }
    return data;
}

@end
