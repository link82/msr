#import "_Yarn.h"
#import "S2SOrderedDictionary.h"

@interface Yarn : _Yarn {}
// Custom logic goes here.

+ (S2SOrderedDictionary*)filterValues;

@end
