#import "StitchTypeProduct.h"


@interface StitchTypeProduct ()

// Private interface goes here.
+ (NSString*)baseDirectory;

@end


@implementation StitchTypeProduct

// Custom logic goes here.


+ (NSString*)baseDirectory{
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    return documentsDirectory;
}




- (NSString*)imageRemoteFilePath{
    if(!self.image_url)
        return nil;
    
    NSString *baseURL = [[BTDataSync shared] apiURL];
    NSString *path = [NSString stringWithFormat:@"%@%@",baseURL,self.image_url];
    
    return path;
}

- (NSString*)sketchRemoteFilePath{
    if(!self.sketch_url)
        return nil;
    
    NSString *baseURL = [[BTDataSync shared] apiURL];
    NSString *path = [NSString stringWithFormat:@"%@%@",baseURL,self.sketch_url];
    
    return path;
}

- (NSString*)imageLocalFilePath{
    if(!self.image_url)
        return nil;
    
    NSString *path = [[StitchTypeProduct baseDirectory] stringByAppendingFormat:@"/products/images/%d.jpg",self.codeValue];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:NO])
        return path;
    else
        return nil;
}

- (NSString*)sketchLocalFilePath{
    if(!self.sketch_url)
        return nil;
    
    NSString *path = [[StitchTypeProduct baseDirectory] stringByAppendingFormat:@"/products/sketches/%d.JPG",self.codeValue];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:NO])
        return path;
    else
        return nil;
}

- (NSString*)imageBundledFilePath{
    if(!self.image_url)
        return nil;
    
    NSString *path = [[[NSBundle mainBundle] bundlePath] stringByAppendingFormat:@"/products.bundle/%d.jpg",self.codeValue];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:NO])
        return path;
    else
        return nil;
}

- (NSString*)sketchBundledFilePath{
    if(!self.sketch_url)
        return nil;
    NSString *path = [[[NSBundle mainBundle] bundlePath] stringByAppendingFormat:@"/sketches.bundle/%d.JPG",self.codeValue];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:NO])
        return path;
    else
        return nil;
}

- (NSString*)imageFilePath{
    NSString *path = nil;
    
    if([[BTDataSync shared] forceRemoteAssets]){
        NSLog(@"Using remote assets");
        path = [self imageRemoteFilePath];
    }
    
    if(!path)
        path = [self imageLocalFilePath];
    
    if(!path)
        path = [self imageBundledFilePath];
    
    if(!path)
        path = [self imageRemoteFilePath];
    
    return path;
}

- (NSString*)sketchFilePath{
    NSString *path = nil;
    
    if([[BTDataSync shared] forceRemoteAssets]){
        NSLog(@"Using remote assets");
        path = [self sketchRemoteFilePath];
    }
    
    if(!path)
        path = [self sketchLocalFilePath];
    
    if(!path)
        path = [self sketchBundledFilePath];
    
    if(!path)
        path = [self sketchRemoteFilePath];
    
    return path;
}

- (NSURL*)imageFileURL{
    NSString *path = nil;
    
    path = [self imageFilePath];
    
    if(path){
        if([path rangeOfString:@"http"].location != NSNotFound)
            return [NSURL URLWithString:path];
        else
            return [NSURL fileURLWithPath:path];
    }
    else
        return nil;
    
}

- (NSURL*)sketchFileURL{
    NSString *path = nil;
    
    path = [self sketchFilePath];
    
    if(path){
        if([path rangeOfString:@"http"].location != NSNotFound)
            return [NSURL URLWithString:path];
        else
            return [NSURL fileURLWithPath:path];
    }
    else
        return nil;
    
}


@end
