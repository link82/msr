// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Brand.h instead.

#import <CoreData/CoreData.h>


extern const struct BrandAttributes {
	__unsafe_unretained NSString *code;
	__unsafe_unretained NSString *created_at;
	__unsafe_unretained NSString *item_description;
	__unsafe_unretained NSString *string_code;
	__unsafe_unretained NSString *updated_at;
} BrandAttributes;

extern const struct BrandRelationships {
	__unsafe_unretained NSString *stitches;
} BrandRelationships;

extern const struct BrandFetchedProperties {
} BrandFetchedProperties;

@class Stitch;







@interface BrandID : NSManagedObjectID {}
@end

@interface _Brand : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (BrandID*)objectID;





@property (nonatomic, strong) NSNumber* code;



@property int16_t codeValue;
- (int16_t)codeValue;
- (void)setCodeValue:(int16_t)value_;

//- (BOOL)validateCode:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* created_at;



//- (BOOL)validateCreated_at:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* item_description;



//- (BOOL)validateItem_description:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* string_code;



//- (BOOL)validateString_code:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* updated_at;



//- (BOOL)validateUpdated_at:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *stitches;

- (NSMutableSet*)stitchesSet;





@end

@interface _Brand (CoreDataGeneratedAccessors)

- (void)addStitches:(NSSet*)value_;
- (void)removeStitches:(NSSet*)value_;
- (void)addStitchesObject:(Stitch*)value_;
- (void)removeStitchesObject:(Stitch*)value_;

@end

@interface _Brand (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveCode;
- (void)setPrimitiveCode:(NSNumber*)value;

- (int16_t)primitiveCodeValue;
- (void)setPrimitiveCodeValue:(int16_t)value_;




- (NSDate*)primitiveCreated_at;
- (void)setPrimitiveCreated_at:(NSDate*)value;




- (NSString*)primitiveItem_description;
- (void)setPrimitiveItem_description:(NSString*)value;




- (NSString*)primitiveString_code;
- (void)setPrimitiveString_code:(NSString*)value;




- (NSDate*)primitiveUpdated_at;
- (void)setPrimitiveUpdated_at:(NSDate*)value;





- (NSMutableSet*)primitiveStitches;
- (void)setPrimitiveStitches:(NSMutableSet*)value;


@end
