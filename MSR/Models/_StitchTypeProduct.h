// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to StitchTypeProduct.h instead.

#import <CoreData/CoreData.h>


extern const struct StitchTypeProductAttributes {
	__unsafe_unretained NSString *brand;
	__unsafe_unretained NSString *category;
	__unsafe_unretained NSString *code;
	__unsafe_unretained NSString *created_at;
	__unsafe_unretained NSString *image_url;
	__unsafe_unretained NSString *item_description;
	__unsafe_unretained NSString *language;
	__unsafe_unretained NSString *model;
	__unsafe_unretained NSString *product_code;
	__unsafe_unretained NSString *season;
	__unsafe_unretained NSString *sketch_url;
	__unsafe_unretained NSString *stitch_type_code;
	__unsafe_unretained NSString *stitch_type_string_code;
	__unsafe_unretained NSString *updated_at;
	__unsafe_unretained NSString *updated_picture;
	__unsafe_unretained NSString *updated_sketch;
	__unsafe_unretained NSString *year;
} StitchTypeProductAttributes;

extern const struct StitchTypeProductRelationships {
	__unsafe_unretained NSString *stitch_type;
} StitchTypeProductRelationships;

extern const struct StitchTypeProductFetchedProperties {
} StitchTypeProductFetchedProperties;

@class StitchType;



















@interface StitchTypeProductID : NSManagedObjectID {}
@end

@interface _StitchTypeProduct : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (StitchTypeProductID*)objectID;





@property (nonatomic, strong) NSString* brand;



//- (BOOL)validateBrand:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* category;



//- (BOOL)validateCategory:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* code;



@property int16_t codeValue;
- (int16_t)codeValue;
- (void)setCodeValue:(int16_t)value_;

//- (BOOL)validateCode:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* created_at;



//- (BOOL)validateCreated_at:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* image_url;



//- (BOOL)validateImage_url:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* item_description;



//- (BOOL)validateItem_description:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* language;



//- (BOOL)validateLanguage:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* model;



//- (BOOL)validateModel:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* product_code;



//- (BOOL)validateProduct_code:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* season;



//- (BOOL)validateSeason:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* sketch_url;



//- (BOOL)validateSketch_url:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* stitch_type_code;



@property int16_t stitch_type_codeValue;
- (int16_t)stitch_type_codeValue;
- (void)setStitch_type_codeValue:(int16_t)value_;

//- (BOOL)validateStitch_type_code:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* stitch_type_string_code;



//- (BOOL)validateStitch_type_string_code:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* updated_at;



//- (BOOL)validateUpdated_at:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* updated_picture;



@property BOOL updated_pictureValue;
- (BOOL)updated_pictureValue;
- (void)setUpdated_pictureValue:(BOOL)value_;

//- (BOOL)validateUpdated_picture:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* updated_sketch;



@property BOOL updated_sketchValue;
- (BOOL)updated_sketchValue;
- (void)setUpdated_sketchValue:(BOOL)value_;

//- (BOOL)validateUpdated_sketch:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* year;



@property int16_t yearValue;
- (int16_t)yearValue;
- (void)setYearValue:(int16_t)value_;

//- (BOOL)validateYear:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) StitchType *stitch_type;

//- (BOOL)validateStitch_type:(id*)value_ error:(NSError**)error_;





@end

@interface _StitchTypeProduct (CoreDataGeneratedAccessors)

@end

@interface _StitchTypeProduct (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveBrand;
- (void)setPrimitiveBrand:(NSString*)value;




- (NSString*)primitiveCategory;
- (void)setPrimitiveCategory:(NSString*)value;




- (NSNumber*)primitiveCode;
- (void)setPrimitiveCode:(NSNumber*)value;

- (int16_t)primitiveCodeValue;
- (void)setPrimitiveCodeValue:(int16_t)value_;




- (NSDate*)primitiveCreated_at;
- (void)setPrimitiveCreated_at:(NSDate*)value;




- (NSString*)primitiveImage_url;
- (void)setPrimitiveImage_url:(NSString*)value;




- (NSString*)primitiveItem_description;
- (void)setPrimitiveItem_description:(NSString*)value;




- (NSString*)primitiveLanguage;
- (void)setPrimitiveLanguage:(NSString*)value;




- (NSString*)primitiveModel;
- (void)setPrimitiveModel:(NSString*)value;




- (NSString*)primitiveProduct_code;
- (void)setPrimitiveProduct_code:(NSString*)value;




- (NSString*)primitiveSeason;
- (void)setPrimitiveSeason:(NSString*)value;




- (NSString*)primitiveSketch_url;
- (void)setPrimitiveSketch_url:(NSString*)value;




- (NSNumber*)primitiveStitch_type_code;
- (void)setPrimitiveStitch_type_code:(NSNumber*)value;

- (int16_t)primitiveStitch_type_codeValue;
- (void)setPrimitiveStitch_type_codeValue:(int16_t)value_;




- (NSString*)primitiveStitch_type_string_code;
- (void)setPrimitiveStitch_type_string_code:(NSString*)value;




- (NSDate*)primitiveUpdated_at;
- (void)setPrimitiveUpdated_at:(NSDate*)value;




- (NSNumber*)primitiveUpdated_picture;
- (void)setPrimitiveUpdated_picture:(NSNumber*)value;

- (BOOL)primitiveUpdated_pictureValue;
- (void)setPrimitiveUpdated_pictureValue:(BOOL)value_;




- (NSNumber*)primitiveUpdated_sketch;
- (void)setPrimitiveUpdated_sketch:(NSNumber*)value;

- (BOOL)primitiveUpdated_sketchValue;
- (void)setPrimitiveUpdated_sketchValue:(BOOL)value_;




- (NSNumber*)primitiveYear;
- (void)setPrimitiveYear:(NSNumber*)value;

- (int16_t)primitiveYearValue;
- (void)setPrimitiveYearValue:(int16_t)value_;





- (StitchType*)primitiveStitch_type;
- (void)setPrimitiveStitch_type:(StitchType*)value;


@end
