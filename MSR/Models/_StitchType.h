// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to StitchType.h instead.

#import <CoreData/CoreData.h>


extern const struct StitchTypeAttributes {
	__unsafe_unretained NSString *code;
	__unsafe_unretained NSString *created_at;
	__unsafe_unretained NSString *full_description;
	__unsafe_unretained NSString *item_description;
	__unsafe_unretained NSString *language;
	__unsafe_unretained NSString *leaf;
	__unsafe_unretained NSString *parent_code;
	__unsafe_unretained NSString *string_code;
	__unsafe_unretained NSString *updated_at;
} StitchTypeAttributes;

extern const struct StitchTypeRelationships {
	__unsafe_unretained NSString *childrens;
	__unsafe_unretained NSString *parent;
	__unsafe_unretained NSString *products;
	__unsafe_unretained NSString *stitches;
} StitchTypeRelationships;

extern const struct StitchTypeFetchedProperties {
} StitchTypeFetchedProperties;

@class StitchType;
@class StitchType;
@class StitchTypeProduct;
@class Stitch;











@interface StitchTypeID : NSManagedObjectID {}
@end

@interface _StitchType : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (StitchTypeID*)objectID;





@property (nonatomic, strong) NSNumber* code;



@property int16_t codeValue;
- (int16_t)codeValue;
- (void)setCodeValue:(int16_t)value_;

//- (BOOL)validateCode:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* created_at;



//- (BOOL)validateCreated_at:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* full_description;



//- (BOOL)validateFull_description:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* item_description;



//- (BOOL)validateItem_description:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* language;



//- (BOOL)validateLanguage:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* leaf;



@property BOOL leafValue;
- (BOOL)leafValue;
- (void)setLeafValue:(BOOL)value_;

//- (BOOL)validateLeaf:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* parent_code;



@property int16_t parent_codeValue;
- (int16_t)parent_codeValue;
- (void)setParent_codeValue:(int16_t)value_;

//- (BOOL)validateParent_code:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* string_code;



//- (BOOL)validateString_code:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* updated_at;



//- (BOOL)validateUpdated_at:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *childrens;

- (NSMutableSet*)childrensSet;




@property (nonatomic, strong) StitchType *parent;

//- (BOOL)validateParent:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) NSSet *products;

- (NSMutableSet*)productsSet;




@property (nonatomic, strong) NSSet *stitches;

- (NSMutableSet*)stitchesSet;





@end

@interface _StitchType (CoreDataGeneratedAccessors)

- (void)addChildrens:(NSSet*)value_;
- (void)removeChildrens:(NSSet*)value_;
- (void)addChildrensObject:(StitchType*)value_;
- (void)removeChildrensObject:(StitchType*)value_;

- (void)addProducts:(NSSet*)value_;
- (void)removeProducts:(NSSet*)value_;
- (void)addProductsObject:(StitchTypeProduct*)value_;
- (void)removeProductsObject:(StitchTypeProduct*)value_;

- (void)addStitches:(NSSet*)value_;
- (void)removeStitches:(NSSet*)value_;
- (void)addStitchesObject:(Stitch*)value_;
- (void)removeStitchesObject:(Stitch*)value_;

@end

@interface _StitchType (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveCode;
- (void)setPrimitiveCode:(NSNumber*)value;

- (int16_t)primitiveCodeValue;
- (void)setPrimitiveCodeValue:(int16_t)value_;




- (NSDate*)primitiveCreated_at;
- (void)setPrimitiveCreated_at:(NSDate*)value;




- (NSString*)primitiveFull_description;
- (void)setPrimitiveFull_description:(NSString*)value;




- (NSString*)primitiveItem_description;
- (void)setPrimitiveItem_description:(NSString*)value;




- (NSString*)primitiveLanguage;
- (void)setPrimitiveLanguage:(NSString*)value;




- (NSNumber*)primitiveLeaf;
- (void)setPrimitiveLeaf:(NSNumber*)value;

- (BOOL)primitiveLeafValue;
- (void)setPrimitiveLeafValue:(BOOL)value_;




- (NSNumber*)primitiveParent_code;
- (void)setPrimitiveParent_code:(NSNumber*)value;

- (int16_t)primitiveParent_codeValue;
- (void)setPrimitiveParent_codeValue:(int16_t)value_;




- (NSString*)primitiveString_code;
- (void)setPrimitiveString_code:(NSString*)value;




- (NSDate*)primitiveUpdated_at;
- (void)setPrimitiveUpdated_at:(NSDate*)value;





- (NSMutableSet*)primitiveChildrens;
- (void)setPrimitiveChildrens:(NSMutableSet*)value;



- (StitchType*)primitiveParent;
- (void)setPrimitiveParent:(StitchType*)value;



- (NSMutableSet*)primitiveProducts;
- (void)setPrimitiveProducts:(NSMutableSet*)value;



- (NSMutableSet*)primitiveStitches;
- (void)setPrimitiveStitches:(NSMutableSet*)value;


@end
