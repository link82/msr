#import "Machine.h"


@interface Machine ()

// Private interface goes here.

@end


@implementation Machine

// Custom logic goes here.
+ (S2SOrderedDictionary*)filterValues{
    S2SOrderedDictionary *data = [[S2SOrderedDictionary alloc] init];
    
    [data setObject:@"" forKey:[NSString stringWithFormat:NSLocalizedString(@" -- All %@ --",@" -- Tutti i fornitori -- "),NSLocalizedString(@"machines", @"machines")]];
    
    NSSortDescriptor *sortByName = [[NSSortDescriptor alloc] initWithKey:@"item_description" ascending:YES];
    NSFetchRequest *f = [Machine fetchRequest];
    [f setSortDescriptors:[NSArray arrayWithObject:sortByName]];

    
    for (Machine *tmp in [Machine objectsWithFetchRequest:f])
        [data setObject:tmp.code.stringValue forKey:tmp.item_description];
    
    return data;
}

@end
