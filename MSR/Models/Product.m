#import "Product.h"


@interface Product ()

// Private interface goes here.

@end


@implementation Product

// Custom logic goes here.
+ (S2SOrderedDictionary*)filterValues{
    S2SOrderedDictionary *data = [[S2SOrderedDictionary alloc] init];
    

    [data setObject:@"" forKey:[NSString stringWithFormat:NSLocalizedString(@" -- All %@ --",@" -- Tutti i fornitori -- "),NSLocalizedString(@"products", @"products")]];
    
    NSSortDescriptor *sortByName = [[NSSortDescriptor alloc] initWithKey:@"item_description" ascending:YES];
    NSFetchRequest *f = [Product fetchRequest];
    [f setSortDescriptors:[NSArray arrayWithObject:sortByName]];
    
    NSArray *products = [Product objectsWithFetchRequest:f];
    for (Product *tmp in products)
        [data setObject:tmp.code.stringValue forKey:tmp.item_description];
    
    return data;
}

@end
