//
//  DataModels.h
//  MSR
//
//  Created by Davide Cenzi on 07/12/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#ifndef MSR_DataModels_h
#define MSR_DataModels_h

#import "MacroSeason.h"
#import "Stitch.h"
#import "Picture.h"
#import "Product.h"
#import "Yarn.h"
#import "Machine.h"
#import "StitchType.h"
#import "User.h"
#import "Brand.h"
#import "Supplier.h"
#import "StitchTypeProduct.h"

#endif
