// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to MacroSeason.m instead.

#import "_MacroSeason.h"

const struct MacroSeasonAttributes MacroSeasonAttributes = {
	.code = @"code",
	.created_at = @"created_at",
	.item_description = @"item_description",
	.string_code = @"string_code",
	.updated_at = @"updated_at",
};

const struct MacroSeasonRelationships MacroSeasonRelationships = {
	.stitches = @"stitches",
};

const struct MacroSeasonFetchedProperties MacroSeasonFetchedProperties = {
};

@implementation MacroSeasonID
@end

@implementation _MacroSeason

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"MacroSeason" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"MacroSeason";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"MacroSeason" inManagedObjectContext:moc_];
}

- (MacroSeasonID*)objectID {
	return (MacroSeasonID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"codeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"code"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic code;



- (int16_t)codeValue {
	NSNumber *result = [self code];
	return [result shortValue];
}

- (void)setCodeValue:(int16_t)value_ {
	[self setCode:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveCodeValue {
	NSNumber *result = [self primitiveCode];
	return [result shortValue];
}

- (void)setPrimitiveCodeValue:(int16_t)value_ {
	[self setPrimitiveCode:[NSNumber numberWithShort:value_]];
}





@dynamic created_at;






@dynamic item_description;






@dynamic string_code;






@dynamic updated_at;






@dynamic stitches;

	
- (NSMutableSet*)stitchesSet {
	[self willAccessValueForKey:@"stitches"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"stitches"];
  
	[self didAccessValueForKey:@"stitches"];
	return result;
}
	






@end
