#import "MacroSeason.h"


@interface MacroSeason ()

// Private interface goes here.

@end


@implementation MacroSeason

// Custom logic goes here.
+ (S2SOrderedDictionary*)filterValues{
    S2SOrderedDictionary *data = [[S2SOrderedDictionary alloc] init];
    
    [data setObject:@"" forKey:[NSString stringWithFormat:NSLocalizedString(@" -- All %@ --",@" -- Tutti i fornitori -- "),NSLocalizedString(@"seasons", @"seasons")]];
    
    NSSortDescriptor *sortByName = [[NSSortDescriptor alloc] initWithKey:@"item_description" ascending:YES];
    NSFetchRequest *f = [MacroSeason fetchRequest];
    [f setSortDescriptors:[NSArray arrayWithObject:sortByName]];
    
    for (MacroSeason *tmp in [MacroSeason objectsWithFetchRequest:f])
        [data setObject:tmp.string_code forKey:tmp.item_description];
    
    return data;
}

@end
