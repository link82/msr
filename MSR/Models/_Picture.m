// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Picture.m instead.

#import "_Picture.h"

const struct PictureAttributes PictureAttributes = {
	.code = @"code",
	.created_at = @"created_at",
	.detail_picture = @"detail_picture",
	.item_description = @"item_description",
	.language = @"language",
	.stitch_code = @"stitch_code",
	.updated = @"updated",
	.updated_at = @"updated_at",
	.url = @"url",
};

const struct PictureRelationships PictureRelationships = {
	.stitch = @"stitch",
};

const struct PictureFetchedProperties PictureFetchedProperties = {
};

@implementation PictureID
@end

@implementation _Picture

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Picture" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Picture";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Picture" inManagedObjectContext:moc_];
}

- (PictureID*)objectID {
	return (PictureID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"codeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"code"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"detail_pictureValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"detail_picture"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"stitch_codeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"stitch_code"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"updatedValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"updated"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic code;



- (int16_t)codeValue {
	NSNumber *result = [self code];
	return [result shortValue];
}

- (void)setCodeValue:(int16_t)value_ {
	[self setCode:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveCodeValue {
	NSNumber *result = [self primitiveCode];
	return [result shortValue];
}

- (void)setPrimitiveCodeValue:(int16_t)value_ {
	[self setPrimitiveCode:[NSNumber numberWithShort:value_]];
}





@dynamic created_at;






@dynamic detail_picture;



- (BOOL)detail_pictureValue {
	NSNumber *result = [self detail_picture];
	return [result boolValue];
}

- (void)setDetail_pictureValue:(BOOL)value_ {
	[self setDetail_picture:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveDetail_pictureValue {
	NSNumber *result = [self primitiveDetail_picture];
	return [result boolValue];
}

- (void)setPrimitiveDetail_pictureValue:(BOOL)value_ {
	[self setPrimitiveDetail_picture:[NSNumber numberWithBool:value_]];
}





@dynamic item_description;






@dynamic language;






@dynamic stitch_code;



- (int16_t)stitch_codeValue {
	NSNumber *result = [self stitch_code];
	return [result shortValue];
}

- (void)setStitch_codeValue:(int16_t)value_ {
	[self setStitch_code:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveStitch_codeValue {
	NSNumber *result = [self primitiveStitch_code];
	return [result shortValue];
}

- (void)setPrimitiveStitch_codeValue:(int16_t)value_ {
	[self setPrimitiveStitch_code:[NSNumber numberWithShort:value_]];
}





@dynamic updated;



- (BOOL)updatedValue {
	NSNumber *result = [self updated];
	return [result boolValue];
}

- (void)setUpdatedValue:(BOOL)value_ {
	[self setUpdated:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveUpdatedValue {
	NSNumber *result = [self primitiveUpdated];
	return [result boolValue];
}

- (void)setPrimitiveUpdatedValue:(BOOL)value_ {
	[self setPrimitiveUpdated:[NSNumber numberWithBool:value_]];
}





@dynamic updated_at;






@dynamic url;






@dynamic stitch;

	






@end
