#import "_Stitch.h"
#import "S2SOrderedDictionary.h"
#import "Yarn.h"
#import "Supplier.h"
#import "Product.h"

@interface Stitch : _Stitch {}
// Custom logic goes here.

- (BOOL)hasGenericPictureFile;
- (BOOL)hasDetailPictureFile;

- (Picture*)genericPicture;
- (Picture*)detailPicture;
- (NSArray*)sortedPictures;

- (NSString*)genericPicturePath;
- (NSString*)detailPicturePath;

- (NSURL*)genericPictureURL;
- (NSURL*)detailPictureURL;

- (NSArray*)yarns;
- (Yarn*)yarn;
- (NSArray*)yarnCodes;
- (Supplier*)supplier;
- (Product*)product;

+ (S2SOrderedDictionary*)compositionsFilterValues;


@end
