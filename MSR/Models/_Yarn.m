// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Yarn.m instead.

#import "_Yarn.h"

const struct YarnAttributes YarnAttributes = {
	.code = @"code",
	.created_at = @"created_at",
	.item_description = @"item_description",
	.language = @"language",
	.string_code = @"string_code",
	.supplier_code = @"supplier_code",
	.supplier_name = @"supplier_name",
	.updated_at = @"updated_at",
};

const struct YarnRelationships YarnRelationships = {
	.supplier = @"supplier",
};

const struct YarnFetchedProperties YarnFetchedProperties = {
};

@implementation YarnID
@end

@implementation _Yarn

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Yarn" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Yarn";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Yarn" inManagedObjectContext:moc_];
}

- (YarnID*)objectID {
	return (YarnID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"codeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"code"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic code;



- (int16_t)codeValue {
	NSNumber *result = [self code];
	return [result shortValue];
}

- (void)setCodeValue:(int16_t)value_ {
	[self setCode:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveCodeValue {
	NSNumber *result = [self primitiveCode];
	return [result shortValue];
}

- (void)setPrimitiveCodeValue:(int16_t)value_ {
	[self setPrimitiveCode:[NSNumber numberWithShort:value_]];
}





@dynamic created_at;






@dynamic item_description;






@dynamic language;






@dynamic string_code;






@dynamic supplier_code;






@dynamic supplier_name;






@dynamic updated_at;






@dynamic supplier;

	






@end
