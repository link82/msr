//
//  S2SOrderedDictionary.h
//  Geoplast
//
//  Created by Davide Cenzi on 18/08/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface S2SOrderedDictionary : NSObject{
    NSMutableDictionary *dictionary;
    NSMutableArray *keys_array;
    NSMutableArray *values_array;
}

- (void)removeAllObjects;
- (id)initWithCapacity:(NSUInteger)capacity;
- (void)setObject:(id)anObject forKey:(id)aKey;
- (void)removeObjectForKey:(id)aKey;
- (NSUInteger)count;
- (NSEnumerator *)keyEnumerator;
- (NSArray *)allKeys;

- (id)objectForKey:(id)aKey;
- (id)objectAtIndex:(NSUInteger)index;
- (NSString*)keyAtIndex:(NSUInteger)index;
- (NSUInteger)indexForKey:(NSString*)key;

//new methods
- (NSUInteger)indexOfObject:(id)object;
- (NSString*)keyOfObject:(id)object;

@end
