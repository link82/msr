//
//  BTFont+Overrides.m
//  MSR
//
//  Created by Davide Cenzi on 17/12/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import "BTFont+Overrides.h"

@implementation UIFont (Overrides)

/*
+ (void)load
{
    Method orig = class_getClassMethod([UIFont class], @selector(systemFontOfSize:));
    Method swiz = class_getClassMethod([UIFont class], @selector(_systemFontOfSize:));
    method_exchangeImplementations(orig, swiz);
    
    orig = class_getClassMethod([UIFont class], @selector(boldSystemFontOfSize:));
    swiz = class_getClassMethod([UIFont class], @selector(_boldSystemFontOfSize:));
    method_exchangeImplementations(orig, swiz);
    
    orig = class_getClassMethod([UIFont class], @selector(italicSystemFontOfSize:));
    swiz = class_getClassMethod([UIFont class], @selector(_italicSystemFontOfSize:));
    method_exchangeImplementations(orig, swiz);
}
 */

+ (UIFont *)_systemFontOfSize:(CGFloat)fontSize
{
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0f)
    {
        //Call original implementation.
        return [self _systemFontOfSize:fontSize];
    }
    
    return [UIFont fontWithName:@"Futura-CondensedMedium" size:fontSize];
}

+ (UIFont *)_boldSystemFontOfSize:(CGFloat)fontSize
{
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0f)
    {
        //Call original implementation.
        return [self _systemFontOfSize:fontSize];
    }
    
    return [UIFont fontWithName:@"Futura-CondensedExtraBold" size:fontSize];
}

+ (UIFont *)_italicSystemFontOfSize:(CGFloat)fontSize
{
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0f)
    {
        //Call original implementation.
        return [self _systemFontOfSize:fontSize];
    }
    
    return [UIFont fontWithName:@"Futura-CondensedMedium" size:fontSize];
}

@end