//
//  BTActionSheet.h
//  NeedleIOS
//
//  Created by Davide Cenzi on 29/12/13.
//  Copyright (c) 2013 Davide Cenzi. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^BTActionSheetOnPress)(UIActionSheet *actionSheet, NSInteger pressedIndex);
typedef void(^BTActionSheetOnCancel)(UIActionSheet *actionSheet);

@interface BTActionSheet : UIActionSheet <UIActionSheetDelegate>{
    BTActionSheetOnPress _onPress;
    BTActionSheetOnCancel _onCancel;
}

- (instancetype)initWithTitle:(NSString *)title cancelButtonTitle:(NSString *)cancelButtonTitle destructiveButtonTitle:(NSString *)destructiveButtonTitle otherButtonTitles:(NSString *)otherButtonTitles onPressBlock:(BTActionSheetOnPress)onPress onCancelBlock:(BTActionSheetOnCancel)onCancel;

@end
