//
//  BTQueue.m
//  PhotoMailer
//
//  Created by Davide Cenzi on 05/01/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import "BTQueue.h"

@implementation BTQueue

// Stack methods

- (void)enqueue:(id)object {
    [self addObject:object];
}

- (id)dequeue {
    
    NSUInteger count = [self count];
    if (count > 0) {
        id returnObject = [self objectAtIndex:0];
        [self removeObjectAtIndex:0];
        return returnObject;
    }
    else {
        return nil;
    }
}

@end
