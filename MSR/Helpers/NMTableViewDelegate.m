//
//  NMTableViewDelegate.m
//  Needle
//
//  Created by Davide Cenzi on 03/08/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import "NMTableViewDelegate.h"

@implementation NMTableViewDelegate

- (id)initWithTableView:(UITableView*)tableView sectionsBlock:(NMNumberOfSectionsBlock)sectionsBlock rowsBlock:(NMNumberOfRowsInSectionBlock)rowsBlock cellsBlock:(NMCellAtIndexPathBlock)cellsBlock selectsBlock:(NMSelectAtIndexPathBlock)selectsBlock{
    
    self = [super init];
    
    if(self){
        _tableView = tableView;
        [_tableView setDelegate:self];
        [_tableView setDataSource:self];
        
        _sectionBlock = sectionsBlock;
        _rowsBlock = rowsBlock;
        _cellsBlock = cellsBlock;
        _selectsBlock = selectsBlock;
    }
    return self;
}

#pragma  mark - setters for optional blocks

- (void)setCellHeightsBlock:(NMCellHeightsBlock)heightsBlock{
    _heightsBlock = heightsBlock;
}

- (void)setHeadersHeightsBlock:(NMHeaderHeightsBlock)heightsBlock{
    _headersHeightsBlock = heightsBlock;
}
- (void)setFootersHeightsBlock:(NMFooterHeightsBlock)heightsBlock{
    _footersHeightsBlock = heightsBlock;
}

- (void)setHeadersViewBlock:(NMHeaderViewBlock)viewBlock{
    _headerViewBlock = viewBlock;
}

- (void)setFootersViewBlock:(NMFooterViewBlock)viewBlock{
    _footerViewBlock = viewBlock;
}

#pragma mark -

#pragma mark - Table view data source - MANDATORY CONFIG

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    if(_sectionBlock != nil)
        return _sectionBlock(tableView);
    else
        return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if(_rowsBlock != nil)
        return _rowsBlock(tableView,section);
    else
        return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell;
    
    if(_cellsBlock != nil){
        cell = _cellsBlock(tableView,indexPath);
    }
    else{
        
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        // Configure the cell...
        [self configureCell:cell atIndexPath:indexPath];
    }
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    //NSManagedObject *managedObject = [[self.fetchedResultsController fetchedObjects] objectAtIndex:indexPath.section];
    
    cell.textLabel.text = @"Missing cell configuration block";
    cell.detailTextLabel.text = @"";
    
}


#pragma mark -

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(_selectsBlock)
        _selectsBlock(tableView,indexPath);
}

#pragma mark - Optional config


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(_heightsBlock != nil){
        return _heightsBlock(tableView,indexPath);
    }
    else
        return 50.0;
    
}

//Headers and footers

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(_headersHeightsBlock != nil)
        return _headersHeightsBlock(tableView,section);
    else
        return 0.0;
}


- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

    if(_headerViewBlock)
        return _headerViewBlock(tableView,section);
    else
        return nil;
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if(_footersHeightsBlock != nil)
        return _footersHeightsBlock(tableView,section);
    else
        return 0.0;
}

- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    if(_footerViewBlock)
        return _footerViewBlock(tableView,section);
    else
        return nil;

}




/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark -


@end
