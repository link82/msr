//
//  NMFieldDelegate.m
//  Needle
//
//  Created by Davide Cenzi on 01/08/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import "NMFieldDelegate.h"

@interface NMFieldDelegate ()

- (void)cancel;
- (void)prevField;
- (void)nextField;
- (void)listen;
- (void)stopListening;
- (void)scrollToField;

@end

@implementation NMFieldDelegate
@synthesize currentField = _currentField;

- (id)initWithFields:(NSArray*)fields fieldChangeBlock:(NMChangedTextFieldBlock)changeBlock
   shouldReturnBlock:(NMTextFieldShouldReturnBlock)returnBlock
      shouldEndBlock:(NMTextFieldShouldEndBlock)endBlock
        withAutoScrollOnView:(id)view{

    self = [super init];
    
    if(self){
        _useToolbar = NO;
        _fieldsDelegate = YES;
        _fields = fields;
        _contentView = view;
        _autoScroll = (_contentView != nil);
        
        _changeBlock = changeBlock;
        _returnBlock = returnBlock;
        _endBlock = endBlock;
        
        if(_fields != nil){
            for (UITextField * field in _fields) {
                [field setDelegate:self];
            }
        }
        
        if(_autoScroll)
            [self listen];
    }
    
    return self;
    
}

- (void)addField:(UITextField *)field{
    if(field != nil){
        if(_fieldsDelegate)
            [field setDelegate:self];
        
        _fields = [_fields arrayByAddingObject:field];
    }
}


- (void)setFieldsDelegate:(BOOL)fieldsDelegate{
    _fieldsDelegate = fieldsDelegate;
}

- (void)useToolbar:(BOOL)enableToolbar{
    _useToolbar = enableToolbar;
}

- (void)dealloc{
    [self stopListening];
    self.currentField = nil;
}

/** Keyboard tracking functions **/

- (void)listen{
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)stopListening{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)scrollToField{
    if(_currentField == nil || _contentView == nil)
        return;
    
    CGRect fieldFrame = _currentField.frame;
    if([_contentView isKindOfClass:[UITableView class]] && _currentField.superview != _contentView){
        fieldFrame = [_currentField convertRect:_currentField.frame toView:_contentView];
    }
    [_contentView scrollRectToVisible:fieldFrame animated:YES];
    
}

- (void)keyboardWasShown:(NSNotification *)notification
{

    
    if(_contentView == nil)
        return;
    
    if(!_currentField)
        return;
    
    if([_fields indexOfObject:_currentField] == NSNotFound)
        return;
    
    
    CGFloat toolbarHeight = (_useToolbar ? 44.0 : 0.0);

    if([_contentView isKindOfClass:[UIScrollView class]]){
        UIScrollView *v = (UIScrollView*)_contentView;
        
        // Step 1: Get the size of the keyboard.
        CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        // Step 2: Adjust the bottom content inset of your scroll view by the keyboard height.
        UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height + toolbarHeight, 0.0);
        v.contentInset = contentInsets;
        v.scrollIndicatorInsets = contentInsets;
        
        // Step 3: Scroll the target text field into view.
        CGRect aRect = v.frame;
        aRect.size.height -= (keyboardSize.height + toolbarHeight);
        
        [self scrollToField];

        
    }
    
}

- (void) keyboardWillHide:(NSNotification *)notification {
    
    if(_contentView == nil)
        return;
    
    if(_currentField == nil || [_fields indexOfObject:_currentField] == NSNotFound)
        return;
    
    if([_contentView isKindOfClass:[UIScrollView class]]){
        UIScrollView *v = (UIScrollView*)_contentView;
        UIEdgeInsets contentInsets = UIEdgeInsetsZero;
        v.contentInset = contentInsets;
        v.scrollIndicatorInsets = contentInsets;
        
    }
    
    _currentField = nil;
}



#pragma mark - Textfield delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    
    if(_returnBlock)
        return _returnBlock(textField);
    else
        return NO;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{

    [textField resignFirstResponder];
    
    //forse è ridondante ma fixaa un problema con cancel
    //_currentField = textField;
    
    if(_endBlock)
        return _endBlock(textField);
    else
        return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{

    if([_fields indexOfObject:textField] != NSNotFound)
        _currentField = textField;
    
    if(_useToolbar){
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Cancel",@"") style:UIBarButtonItemStyleBordered target:self action:@selector(cancel)],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           
                           [[UIBarButtonItem alloc]initWithTitle: NSLocalizedString(@"Prev",@"") style:UIBarButtonItemStyleDone target:self action:@selector(prevField)],
                           [[UIBarButtonItem alloc]initWithTitle: NSLocalizedString(@"Next",@"") style:UIBarButtonItemStyleDone target:self action:@selector(nextField)],
                           nil];
    [numberToolbar sizeToFit];
    textField.inputAccessoryView = numberToolbar;
    }

    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    //if(_currentField == textField)
    //    _currentField = nil;
    
    //[textField resignFirstResponder];
}

- (void)cancel{
    if(_currentField != nil)
        [self textFieldShouldEndEditing:_currentField];
}

- (void)prevField{
    
    UITextField * oldField = _currentField;
    
    //is it the last one?
    if(_currentField == nil && [_fields count] > 0){
        _currentField = [_fields objectAtIndex:0];
    }
    else{
        
        NSUInteger index = [_fields indexOfObject:oldField];
        if((index == 0 || index == NSNotFound) && [_fields count] > 0){
            _currentField = [_fields objectAtIndex:[_fields count] -1];
        }
        else{
            _currentField = [_fields objectAtIndex:index-1];
        }
    }
    
    if(_changeBlock)
        _changeBlock(oldField,_currentField);
    
    
    [_currentField becomeFirstResponder];
    [self scrollToField];
}

- (void)nextField{
    
    UITextField * prevField = _currentField;
    
    //is it the last one?
    if(_currentField == nil && [_fields count] > 0){
        _currentField = [_fields objectAtIndex:0];
    }
    else{
        
        int index = [_fields indexOfObject:prevField];
        if(index == (_fields.count -1) || index == NSNotFound){
            _currentField = [_fields objectAtIndex:0];
        }
        else{
            _currentField = [_fields objectAtIndex:index+1];
        }
    }
    
    if(_changeBlock)
        _changeBlock(prevField,_currentField);
    
    
    [_currentField becomeFirstResponder];
    [self scrollToField];
}

#pragma mark -

@end
