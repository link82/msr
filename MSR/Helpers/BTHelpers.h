//
//  NMVarious.h
//  Needle
//
//  Created by Davide Cenzi on 01/08/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTHelpers : NSObject

+ (BOOL)hasFourInchDisplay;

+ (NSString*)appVersion;
+ (NSString*)appBuildNr;


+ (BOOL)hasCydia;
+ (BOOL)jailbrokenDevice;

+ (UIAlertView *)alertText:(NSString*)text withTitle:(NSString*)title;

+ (UIImage *)convertImageToGrayScale:(UIImage *)image;

//Validators
+ (BOOL)validEmail:(NSString*)email;
+ (BOOL)validString:(NSString*)string withMinLength:(int)min andMaxLength:(int)max;
+ (BOOL)numericString:(NSString*)string;

@end
