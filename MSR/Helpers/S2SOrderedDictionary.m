//
//  S2SOrderedDictionary.m
//  Geoplast
//
//  Created by Davide Cenzi on 18/08/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "S2SOrderedDictionary.h"

@implementation S2SOrderedDictionary

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
        dictionary = [[NSMutableDictionary alloc] init];
        keys_array = [[NSMutableArray alloc] init];
        values_array = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (id)initWithCapacity:(NSUInteger)capacity
{
    self = [super init];
    if (self != nil)
    {
        dictionary = [[NSMutableDictionary alloc] initWithCapacity:capacity];
        keys_array = [[NSMutableArray alloc] initWithCapacity:capacity];
        values_array = [[NSMutableArray alloc] initWithCapacity:capacity];
    }
    return self;
}

- (void)removeAllObjects{
    if (dictionary && values_array && keys_array) {
        [dictionary removeAllObjects];
        [keys_array removeAllObjects];
        [values_array removeAllObjects];
    }
}

- (void)setObject:(id)anObject forKey:(id)aKey
{
    if (![dictionary objectForKey:aKey])
    {
        [keys_array addObject:aKey];
        [values_array addObject:anObject];
    }
    else{
        [values_array replaceObjectAtIndex:[keys_array indexOfObject:aKey] withObject:anObject];
    }
    [dictionary setObject:anObject forKey:aKey];
}

- (void)removeObjectForKey:(id)aKey
{
    id object = [dictionary objectForKey:aKey];
    [dictionary removeObjectForKey:aKey];
    [keys_array removeObject:aKey];
    [values_array removeObject:object];
    
}

- (NSUInteger)count
{
    return [dictionary count];
}

- (id)objectForKey:(id)aKey
{
    return [dictionary objectForKey:aKey];
}

- (NSEnumerator *)keyEnumerator
{
    return [keys_array objectEnumerator];
}

- (NSArray *)allKeys{
    return keys_array;
}

- (NSArray*)allObjects{
    return values_array;
}

#pragma mark - new stuff

- (NSUInteger)indexOfObject:(id)object{
    if([values_array containsObject:object])
        return [values_array indexOfObject:object];
    else
        return -1;
}

- (NSString*)keyOfObject:(id)object{
    int index = [self indexOfObject:object];
    if(index > -1)
        return [keys_array objectAtIndex:index];
    else
        return nil;
}

#pragma mark -

- (NSUInteger)indexForKey:(NSString*)key{
    if([keys_array containsObject:key])
        return [keys_array indexOfObject:key];
    else
        return -1;
}

- (NSString*)keyAtIndex:(NSUInteger)index{
    return [[[keys_array objectEnumerator] allObjects] objectAtIndex:index];
}

- (id)objectAtIndex:(NSUInteger)index{
    return [dictionary objectForKey:[[[keys_array objectEnumerator] allObjects] objectAtIndex:index]];
}

@end
