//
//  BTLanguage.m
//  MSR
//
//  Created by Davide Cenzi on 12/12/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import "BTLanguage.h"
#import "BTAlert.h"

@implementation BTLanguage{
    BTAlert *al;
}

+ (instancetype) shared{
    static BTLanguage *_sharedObject = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedObject = [[self alloc] init];
    });
    
    return _sharedObject;
}

- (void)changeLanguageTo:(NSString*)languageId withReboot:(BOOL)withReboot{
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSMutableArray *languages = [[def objectForKey:@"AppleLanguages"] mutableCopy];
    NSString *current_lang = [languages objectAtIndex:0];
    
    if([current_lang isEqualToString:languageId]){
        NSLog(@"Language %@ already active",current_lang);
    }
    else{
        [languages removeObject:languageId];
        [languages insertObject:languageId atIndex:0];
        [def setObject:languages forKey:@"AppleLanguages"];
        [def synchronize];
    }
    
    current_lang = [[def objectForKey:@"AppleLanguages"] objectAtIndex:0];
    NSLog(@"Current language %@",current_lang);
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CHANGED_LANGUAGE" object:nil];
    
    if(withReboot){
        
        al = [[BTAlert alloc] initWithTitle:NSLocalizedString(@"Confirm application reload",@"change language confirm title") text:NSLocalizedString(@"Your application will close wait a couple of seconds then open it again. Proceed?",@"Change language confirm text") style:UIAlertViewStyleDefault onConfirm:^(UIAlertView *alert) {
            /*
            double delayInSeconds = 2.0;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [[NSNotificationCenter defaultCenter] postNotificationName:@"RELOAD_UI" object:nil];
            });
            */
            
            UILocalNotification *notification = [[UILocalNotification alloc] init];
            notification.fireDate = [[NSDate date] dateByAddingTimeInterval:5];
            notification.timeZone = [NSTimeZone defaultTimeZone];
            notification.alertBody = NSLocalizedString(@"Language changed",@"notification text");
            notification.applicationIconBadgeNumber = 0;
            [[UIApplication sharedApplication] scheduleLocalNotification:notification];

            //NSURL *destURL = [NSURL URLWithString:[NSString stringWithFormat:@"msr://msr?language=new"]];
            abort();
            
        } onDismiss:^(UIAlertView *alert) {
            ;
        }];
        
        [al show];
    }
    
       

}

- (NSString*)currentLanguage{
    return [[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"] objectAtIndex:0];
}



@end
