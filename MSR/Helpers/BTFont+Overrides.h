//
//  BTFont+Overrides.h
//  MSR
//
//  Created by Davide Cenzi on 17/12/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (Overrides)
+ (UIFont *)_systemFontOfSize:(CGFloat)fontSize;
+ (UIFont *)_boldSystemFontOfSize:(CGFloat)fontSize;
+ (UIFont *)_italicSystemFontOfSize:(CGFloat)fontSize;
@end
