//
//  BTSwitchButton.m
//  MSR
//
//  Created by Davide Cenzi on 20/12/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import "BTSwitchButton.h"

@interface BTSwitchButton (){
    BOOL _checked;
    BTSwitchButtonOnChange _onChangeBlock;
}


@end

@implementation BTSwitchButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib{
    //Storyboard support
    [self addTarget:self action:@selector(tappedButton:) forControlEvents:UIControlEventTouchUpInside];
}

- (id)initWithTitle:(NSString*)title onChangeBlock:(BTSwitchButtonOnChange)onChangeBlock initialState:(BOOL)checkedState{
    self = [super init];
    
    if(self){
        _checked = checkedState;
        _onChangeBlock = onChangeBlock;
        [self setTitle:title forState:UIControlStateNormal];
        
        //need to be invoked upon start to apply changes due to current status
        if(_onChangeBlock)
            _onChangeBlock(self, _checked);
        
        [self addTarget:self action:@selector(tappedButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

- (IBAction)tappedButton:(id)sender{
    _checked = !_checked;
    
    if(_onChangeBlock)
        _onChangeBlock(self, _checked);
}

- (void)setOnChangeBlock:(BTSwitchButtonOnChange)onChangeBlock{
    _onChangeBlock = onChangeBlock;
    
    //need to be invoked upon start to apply changes due to current status
    if(_onChangeBlock)
        _onChangeBlock(self, _checked);
}

- (BOOL)checked{
    return  _checked;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
