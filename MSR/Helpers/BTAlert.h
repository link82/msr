//
//  BTAlert.h
//  iKeyring
//
//  Created by Davide Cenzi on 25/11/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^BTAlertOnConfirmAction)(UIAlertView *alert);
typedef void (^BTAlertOnDismissAction)(UIAlertView *alert);

@interface BTAlert : UIAlertView <UIAlertViewDelegate>{
    
    NSString * _confirmText;
    NSString * _dismissText;
}

@property(nonatomic,strong) BTAlertOnDismissAction  onDismiss;
@property(nonatomic,strong) BTAlertOnConfirmAction  onConfirm;

- (id)initWithTitle:(NSString *)title text:(NSString*)text style:(UIAlertViewStyle)style onConfirm:(BTAlertOnConfirmAction)confirm
                 onDismiss:(BTAlertOnDismissAction)dismiss;

- (id)initWithTitle:(NSString *)title text:(NSString*)text style:(UIAlertViewStyle)style confirmText:(NSString*)confirmText dismissText:(NSString*)dismissText  onConfirm:(BTAlertOnConfirmAction)confirm
          onDismiss:(BTAlertOnDismissAction)dismiss;

@end
