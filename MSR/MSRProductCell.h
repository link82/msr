//
//  MSRProductCell.h
//  MSR
//
//  Created by Davide Cenzi on 16/01/14.
//  Copyright (c) 2014 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MSRProductCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *stitch_image;
@property (nonatomic, weak) IBOutlet UIImageView *sketch_image;
@property (nonatomic, weak) IBOutlet UIImageView *product_image;

@property (weak, nonatomic) IBOutlet UIView *model_wrapper;
//infos
@property (nonatomic, weak) IBOutlet UILabel *year_label;
@property (nonatomic, weak) IBOutlet UILabel *year_value;
@property (nonatomic, weak) IBOutlet UILabel *season_label;
@property (nonatomic, weak) IBOutlet UILabel *season_value;
@property (nonatomic, weak) IBOutlet UILabel *brand_label;
@property (nonatomic, weak) IBOutlet UILabel *brand_value;
@property (nonatomic, weak) IBOutlet UILabel *model_label;
@property (nonatomic, weak) IBOutlet UILabel *model_value;
@property (weak, nonatomic) IBOutlet UIView *brandView;

@end
