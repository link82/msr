//
//  BTNetworking.m
//  MobileCatalogTemplate
//
//  Created by Davide Cenzi on 16/07/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import "BTNetworking.h"

@implementation BTNetworking

- (instancetype)initWithBaseUrl:(NSString *)baseUrl modelName:(NSString *)modelName
{
    self = [super init];
    
    if (self)
    {
        self.baseUrl = baseUrl;
        self.modelName = modelName;
        
        [self configureNetworking];
        [self configureMapping];
    }
    
    return self;
}


- (void)configureNetworking
{
    // Log debugging info about Core Data
    
    RKLogConfigureByName("RestKit/CoreData", RKLogLevelWarning);
    
    //Loging about the Entity Mapping
    RKLogConfigureByName("RestKit/ObjectMapping", RKLogLevelWarning)
    
    // Logging Networking
    RKLogConfigureByName("RestKit/Network", RKLogLevelWarning);
    
    // Serialize to JSON
    [RKObjectManager sharedManager].requestSerializationMIMEType = RKMIMETypeJSON;
    [RKMIMETypeSerialization registerClass:[RKNSJSONSerialization class] forMIMEType:@"text/plain"];
    
    // Model
    RKObjectManager* objectManager = [RKObjectManager managerWithBaseURL:[NSURL URLWithString:self.baseUrl]];
    NSURL *modelURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:self.modelName ofType:@"momd"]];
    
    // NOTE: Due to an iOS 5 bug, the managed object model returned is immutable.
    NSManagedObjectModel *managedObjectModel = [[[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL] mutableCopy];
    RKManagedObjectStore *managedObjectStore = [[RKManagedObjectStore alloc] initWithManagedObjectModel:managedObjectModel];
    
    
    // Initialize the Core Data stack
    [managedObjectStore createPersistentStoreCoordinator];
    
    //Not used
    NSError *error = nil;
    NSString *path = [RKApplicationDataDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.sqlite", self.modelName]];
    NSString *seedPath = [[NSBundle mainBundle] pathForResource:@"Needle_seed" ofType:@"sqlite"];
    
    //disabling seeding
    seedPath = nil;
    
    [managedObjectStore addSQLitePersistentStoreAtPath:path fromSeedDatabaseAtPath:seedPath withConfiguration:nil options:nil error:&error];
    [managedObjectStore createManagedObjectContexts];
    
    
    // Set the default store shared instance
    objectManager.managedObjectStore = managedObjectStore;
    
    // Configure a managed object cache to ensure we do not create duplicate objects
    objectManager.managedObjectStore.managedObjectCache = [[RKInMemoryManagedObjectCache alloc] initWithManagedObjectContext:managedObjectStore.persistentStoreManagedObjectContext];
    
    
    //set spinner when client is busy
    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
}


- (void)configureMapping
{
    // Setting della Basic Authentication per poter accedere alle API.
    //[[RKObjectManager sharedManager].HTTPClient setAuthorizationHeaderWithUsername:@"link82" password:@"test"];
    
    NSArray *available_languages = [[BTDataSync shared] availableLanguages];
    
    // Recupero oggetti per mappatura
    
    RKManagedObjectStore *store = [RKObjectManager sharedManager].managedObjectStore;
    RKObjectManager *manager = [RKObjectManager sharedManager];
    
    // Status codes
    NSIndexSet *errorStatusCodes = RKStatusCodeIndexSetForClass(RKStatusCodeClassClientError);
    NSIndexSet *successfulStatusCodes = RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful);
    
    
    
    /****** START SUPPLIER MAPPING ******/
    
    // Supplier
    RKEntityMapping *supplierMapping = [RKEntityMapping mappingForEntityForName:NSStringFromClass([Supplier class]) inManagedObjectStore:store];
    [supplierMapping setIdentificationAttributes:@[@"code"]];
    
    [supplierMapping addAttributeMappingsFromDictionary:@{
                                                          @"id":@"code",
                                                          @"description":@"item_description",
                                                          @"code":@"string_code",
                                                          @"created_at":@"created_at",
                                                          @"updated_at":@"updated_at"
                                                          }];
    
    
    //User Response descriptor
    RKResponseDescriptor *supplierResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:supplierMapping
                                                                                                        method:RKRequestMethodGET pathPattern:@"/it/suppliers" keyPath:nil statusCodes:successfulStatusCodes];
    [manager addResponseDescriptor:supplierResponseDescriptor];
    
    //Route
    [manager.router.routeSet addRoute:[RKRoute
                                       routeWithClass:[Supplier class]
                                       pathPattern:@"/it/suppliers/:code"
                                       method:RKRequestMethodGET]];
    
    /****** END SUPPLIERS MAPPING ******/
    
    
    
    /****** START YARN MAPPING ******/
    
    // Yarn
    RKEntityMapping *yarnMapping = [RKEntityMapping mappingForEntityForName:NSStringFromClass([Yarn class]) inManagedObjectStore:store];
    [yarnMapping setIdentificationAttributes:@[ @"code", @"language" ]];
    
    [yarnMapping addAttributeMappingsFromDictionary:@{
                                                      @"id":@"code",
                                                      @"code":@"string_code",
                                                      @"description":@"item_description",
                                                      @"supplier_code":@"supplier_code",
                                                      @"supplier_name":@"supplier_name",
                                                      @"language":@"language",
                                                      @"created_at":@"created_at",
                                                      @"updated_at":@"updated_at"
                                                      }];
    
    [yarnMapping addConnectionForRelationship:@"supplier" connectedBy:@{@"supplier_code": @"string_code"}];
    
    //Yarn
 
        RKResponseDescriptor *yarnResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:yarnMapping
                                                                                                    method:RKRequestMethodGET pathPattern:@"/:language/yarns" keyPath:nil statusCodes:successfulStatusCodes];
        [manager addResponseDescriptor:yarnResponseDescriptor];
 
    //Yarn Route
    [manager.router.routeSet addRoute:[RKRoute
                                       routeWithClass:[Yarn class]
                                       pathPattern:@"/:language/yarns/:code"
                                       method:RKRequestMethodGET]];
    /****** END YARN MAPPING ******/
    
    /****** START MACHINE MAPPING ******/
    
    // Machine
    RKEntityMapping *machineMapping = [RKEntityMapping mappingForEntityForName:NSStringFromClass([Machine class]) inManagedObjectStore:store];
    [machineMapping setIdentificationAttributes:@[ @"code" ]];
    
    [machineMapping addAttributeMappingsFromDictionary:@{
                                                         @"id":@"code",
                                                         @"description":@"item_description",
                                                         @"code":@"string_code",
                                                         @"created_at":@"created_at",
                                                         @"updated_at":@"updated_at"
                                                         }];
    
    
    //User Response descriptor
        RKResponseDescriptor *machineResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:machineMapping
                                                                                                       method:RKRequestMethodGET pathPattern:@"/it/machines" keyPath:nil statusCodes:successfulStatusCodes];
        [manager addResponseDescriptor:machineResponseDescriptor];

    
    //Yarn Route
    [manager.router.routeSet addRoute:[RKRoute
                                       routeWithClass:[Machine class]
                                       pathPattern:@"/it/machines/:code"
                                       method:RKRequestMethodGET]];
    
    /****** END MACHINE MAPPING ******/
    
    /****** START MACRO SEASON MAPPING ******/
    
    // Macro Season
    RKEntityMapping *seasonMapping = [RKEntityMapping mappingForEntityForName:NSStringFromClass([MacroSeason class]) inManagedObjectStore:store];
    [seasonMapping setIdentificationAttributes:@[ @"code" ]];
    
    [seasonMapping addAttributeMappingsFromDictionary:@{
                                                        @"id":@"code",
                                                        @"description":@"item_description",
                                                        @"code":@"string_code",
                                                        @"created_at":@"created_at",
                                                        @"updated_at":@"updated_at"
                                                        }];
    

        //User Response descriptor
        RKResponseDescriptor *seasonResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:seasonMapping
                                                                                                      method:RKRequestMethodGET pathPattern:@"/it/macro_seasons" keyPath:nil statusCodes:successfulStatusCodes];
        [manager addResponseDescriptor:seasonResponseDescriptor];

    //Route
    [manager.router.routeSet addRoute:[RKRoute
                                       routeWithClass:[MacroSeason class]
                                       pathPattern:@"/it/macro_seasons/:code"
                                       method:RKRequestMethodGET]];
    
    
    /****** END MACRO SEASON MAPPING ******/
    
    
    /****** START BRAND MAPPING ******/
    
    // Brand
    RKEntityMapping *brandMapping = [RKEntityMapping mappingForEntityForName:NSStringFromClass([Brand class]) inManagedObjectStore:store];
    [brandMapping setIdentificationAttributes:@[ @"code"]];
    
    [brandMapping addAttributeMappingsFromDictionary:@{
                                                       @"id":@"code",
                                                       @"description":@"item_description",
                                                       @"code":@"string_code",
                                                       @"created_at":@"created_at",
                                                       @"updated_at":@"updated_at"
                                                       }];
    
    
    //User Response descriptor

        RKResponseDescriptor *brandResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:brandMapping
                                                                                                     method:RKRequestMethodGET pathPattern:@"/it/brands" keyPath:nil statusCodes:successfulStatusCodes];
        [manager addResponseDescriptor:brandResponseDescriptor];

    //Yarn Route
    [manager.router.routeSet addRoute:[RKRoute
                                       routeWithClass:[Brand class]
                                       pathPattern:@"/it/brands/:code"
                                       method:RKRequestMethodGET]];
    
    /****** END MACRO SEASON MAPPING ******/
    
    
    
    /****** START PRODUCT MAPPING ******/
    
    // Product
    RKEntityMapping *productMapping = [RKEntityMapping mappingForEntityForName:NSStringFromClass([Product class]) inManagedObjectStore:store];
    [productMapping setIdentificationAttributes:@[ @"code", @"language" ]];
    
    [productMapping addAttributeMappingsFromDictionary:@{
                                                         @"id":@"code",
                                                         @"description":@"item_description",
                                                         @"code":@"string_code",
                                                         @"language":@"language",
                                                         @"created_at":@"created_at",
                                                         @"updated_at":@"updated_at"
                                                         }];
    
    
    //User Response descriptor
        RKResponseDescriptor *productResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:productMapping
                                                                                                       method:RKRequestMethodGET pathPattern:@"/:language/products" keyPath:nil statusCodes:successfulStatusCodes];
        [manager addResponseDescriptor:productResponseDescriptor];

    //Yarn Route
    [manager.router.routeSet addRoute:[RKRoute
                                       routeWithClass:[Product class]
                                       pathPattern:@"/:language/products/:code"
                                       method:RKRequestMethodGET]];
    
    /****** END PRODUCT MAPPING ******/
    
    
    
    
    
    /****** START STITCH TYPE PRODUCTS MAPPING ******/
    
    // Stitch type
    RKEntityMapping *stitchTypeProductMapping = [RKEntityMapping mappingForEntityForName:NSStringFromClass([StitchTypeProduct class]) inManagedObjectStore:store];
    [stitchTypeProductMapping setIdentificationAttributes:@[ @"code", @"language" ]];
    
    [stitchTypeProductMapping addAttributeMappingsFromDictionary:@{
                                                                   @"id":@"code",
                                                                   @"language":@"language",
                                                                   @"brand":@"brand",
                                                                   @"category":@"category",
                                                                   @"description":@"item_description",
                                                                   @"model":@"model",
                                                                   @"product_code":@"product_code",
                                                                   @"season":@"season",
                                                                   @"stitch_type_id":@"stitch_type_code",
                                                                   @"stitch_type_code":@"stitch_type_string_code",
                                                                   @"updated_picture":@"updated_picture",
                                                                   @"updated_sketch":@"updated_sketch",
                                                                   @"year":@"year",
                                                                   @"sketch_url":@"sketch_url",
                                                                   @"image_url":@"image_url",
                                                                   @"language":@"language",
                                                                   @"created_at":@"created_at",
                                                                   @"updated_at":@"updated_at"
                                                                   }];
    
    [stitchTypeProductMapping addConnectionForRelationship:@"stitch_type" connectedBy:@{@"stitch_type_code": @"code", @"language":@"language"}];
    
    /****** END STITCH TYPE MAPPING ******/
    
    
    
    
    /****** START STITCH TYPE MAPPING ******/
    
    // Stitch type
    RKEntityMapping *stitchTypeMapping = [RKEntityMapping mappingForEntityForName:NSStringFromClass([StitchType class]) inManagedObjectStore:store];
    [stitchTypeMapping setIdentificationAttributes:@[ @"code", @"language" ]];
    
    [stitchTypeMapping addAttributeMappingsFromDictionary:@{
                                                            @"id":@"code",
                                                            @"language":@"language",
                                                            @"description":@"item_description",
                                                            @"code":@"string_code",
                                                            @"parent_id":@"parent_code",
                                                            @"leaf":@"leaf",
                                                            @"created_at":@"created_at",
                                                            @"updated_at":@"updated_at"
                                                            }];
    
    [stitchTypeMapping addConnectionForRelationship:@"parent" connectedBy:@{@"parent_code": @"code", @"language":@"language"}];
    
    [stitchTypeMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"stitch_type_products" toKeyPath:@"products" withMapping:stitchTypeProductMapping]];
    
    RKResponseDescriptor *stitchTypeResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:stitchTypeMapping
                                                                                                      method:RKRequestMethodGET pathPattern:@"/:language/stitch_types" keyPath:nil statusCodes:successfulStatusCodes];
    [manager addResponseDescriptor:stitchTypeResponseDescriptor];
    
    //Stitch Type Route
    [manager.router.routeSet addRoute:[RKRoute
                                       routeWithClass:[StitchType class]
                                       pathPattern:@"/:language/stitch_types/:code"
                                       method:RKRequestMethodGET]];
    
    /****** END STITCH TYPE MAPPING ******/
    
    
    /****** START PICTURE MAPPING ******/
    
    // Picture
    RKEntityMapping *pictureMapping = [RKEntityMapping mappingForEntityForName:NSStringFromClass([Picture class]) inManagedObjectStore:store];
    [pictureMapping setIdentificationAttributes:@[ @"code", @"language"]];
    
    [pictureMapping addAttributeMappingsFromDictionary:@{
                                                         @"id":@"code",
                                                         @"description":@"item_description",
                                                         @"stitch_id":@"stitch_code",
                                                         @"url":@"url",
                                                         @"detail_picture":@"detail_picture",
                                                         @"language":@"language",
                                                         @"updated":@"updated",
                                                         @"created_at":@"created_at",
                                                         @"updated_at":@"updated_at"
                                                         }];
    
    [pictureMapping addConnectionForRelationship:@"stitch" connectedBy:@{@"stitch_code": @"code", @"language":@"language"}];
    
    /****** END PICTURE MAPPING ******/
    
    /****** START STITCH MAPPING ******/
    
    // Stitch
    RKEntityMapping *stitchMapping = [RKEntityMapping mappingForEntityForName:NSStringFromClass([Stitch class]) inManagedObjectStore:store];
    [stitchMapping setIdentificationAttributes:@[ @"code", @"language" ]];
    
    [stitchMapping addAttributeMappingsFromDictionary:@{
                                                        @"id":@"code",
                                                        @"description":@"item_description",
                                                        @"code":@"string_code",
                                                        @"brand_id":@"brand_code",
                                                        @"supplier_codes":@"supplier_codes",
                                                        @"product_codes":@"product_codes",
                                                        @"season_code":@"season_code",
                                                        @"year":@"year",
                                                        @"stitch_type_id":@"stitch_type_code",
                                                        @"stitch_type_codes":@"stitch_type_codes",
                                                        @"yarn_codes":@"yarn_codes",
                                                        @"product_codes":@"product_codes",
                                                        @"composition":@"composition",
                                                        @"machine_id":@"machine_code",
                                                        @"stretch":@"stretch",
                                                        @"vanise":@"vanise",
                                                        @"thinness":@"thinness",
                                                        @"updated_images":@"updated_images",
                                                        @"language":@"language",
                                                        @"created_at":@"created_at",
                                                        @"updated_at":@"updated_at"
                                                        }];
    
    [stitchMapping addConnectionForRelationship:@"stitch_type" connectedBy:@{@"stitch_type_code": @"code", @"language":@"language"}];
    [stitchMapping addConnectionForRelationship:@"machine" connectedBy:@{@"machine_code": @"code"}];
    [stitchMapping addConnectionForRelationship:@"brand" connectedBy:@{@"brand_code": @"code"}];
    [stitchMapping addConnectionForRelationship:@"season" connectedBy:@{@"season_code": @"string_code"}];
    
    [stitchMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"pictures" toKeyPath:@"pictures" withMapping:pictureMapping]];
    
    
    //User Response descriptor
        RKResponseDescriptor *stitchResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:stitchMapping
                                                                                                      method:RKRequestMethodGET pathPattern:@"/:language/stitches" keyPath:nil statusCodes:successfulStatusCodes];
        [manager addResponseDescriptor:stitchResponseDescriptor];

    //Stitch Route
    [manager.router.routeSet addRoute:[RKRoute
                                       routeWithClass:[Stitch class]
                                       pathPattern:@"/:language/stitches/:code"
                                       method:RKRequestMethodGET]];
    
    /****** END STITCH MAPPING ******/
    
    
    if(false){
        //Error mapping
        RKObjectMapping *errorMapping = [RKObjectMapping mappingForClass:[RKErrorMessage class]];
        [errorMapping addPropertyMapping: [RKAttributeMapping attributeMappingFromKeyPath:nil toKeyPath:@"errorMessage"]];
        RKResponseDescriptor *errorDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:errorMapping method:RKRequestMethodGET pathPattern:nil keyPath:@"error" statusCodes:errorStatusCodes];
        
        [manager addResponseDescriptor:errorDescriptor];
    }
    
}


@end
