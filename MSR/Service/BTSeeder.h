//
//  BTSeeder.h
//  MobileCatalogTemplate
//
//  Created by Davide Cenzi on 22/06/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "BTDataModel.h"

typedef void(^BTSeederOnCompleteBlock)(NSError *error);

@interface BTSeeder : NSObject

//@property (nonatomic, strong) BTDataModel *dataModel;

//- (instancetype)initWithDataModel:(BTDataModel *)dataModel;
- (void)importDatabaseFromFile:(NSString*)filename onComplete:(BTSeederOnCompleteBlock)onComplete;

@end
