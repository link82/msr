//
//  BTNetworking.h
//  MobileCatalogTemplate
//
//  Created by Davide Cenzi on 16/07/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>
#import "DataModels.h"

@interface BTNetworking : NSObject


@property (nonatomic, copy) NSString *baseUrl;
@property (nonatomic, copy) NSString *modelName;


- (instancetype)initWithBaseUrl:(NSString *)baseUrl modelName:(NSString *)modelName;

- (void)configureNetworking;
- (void)configureMapping;

@end
