    //
//  BTDataSync.m
//  MobileCatalogTemplate
//
//  Created by Davide Cenzi on 22/06/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import "BTDataSync.h"
#include "OpenUDID.h"
#import <SDWebImage/SDImageCache.h>
#import <SDWebImage/SDWebImageManager.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import <SSZipArchive.h>
#import <AFNetworking/AFNetworking.h>
#import "BTAlert.h"
//#import "Reachability.h"



//private stuff
@interface BTDataSync() <SSZipArchiveDelegate>{
    AFHTTPClient *_httpClient;
    RKObjectManager *manager;
    NSUserDefaults *_defaults;
    NSArray *supportedLanguages;
    
    /* IMPORT LOGIC */
    NSMutableArray *importQueue;
    NSString *_importPath;
    int _importPage;
     
}

- (AFHTTPClient*)clientWithBaseURL:(NSURL *)url;
- (void)loadDefaults;

- (void)prepareParams:(NSDictionary*)params withMultipartData:(id<AFMultipartFormData>)formData withMimeType:(NSString*)mimetype;

- (void)importDataFromPath:(NSString*)path atPage:(int)page;
- (void)retrieveImagesArchiveAtURL:(NSString*)url;
- (void)unzipArchive:(NSString*)file_path;
- (void)unzipArchive:(NSString*)file_path toPath:(NSString*)destinationPath;

- (void)unsetUpdated;
- (void)retrieveS3ArchiveURL: (void (^)(NSString *remote_url))callback_block;

//private setters
- (BOOL)setDatabaseLastUpdate:(NSDate*)date;
- (BOOL)setStitchesPicturesLastUpdate:(NSDate*)date;
- (BOOL)setProductsPicturesLastUpdate:(NSDate*)date;
- (BOOL)setProductsSketchesLastUpdate:(NSDate*)date;


@end

@implementation BTDataSync
@synthesize context;

const NSString *available_currencies = @"USD,EUR";

+ (BTDataSync*) shared{
    static BTDataSync *_sharedObject = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedObject = [[self alloc] init];
    });
    
    return _sharedObject;
}

- (AFHTTPClient*)clientWithBaseURL:(NSURL *)url {

    AFHTTPClient * tmp = [[AFHTTPClient alloc] initWithBaseURL:url];
    [tmp registerHTTPOperationClass:[AFJSONRequestOperation class]];
    // Accept HTTP Header; see http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.1
	[tmp setDefaultHeader:@"Accept" value:@"application/json"];
    tmp.parameterEncoding = AFJSONParameterEncoding;
    return tmp;
}

- (id)init{
    self = [super init];
    _defaults = [NSUserDefaults standardUserDefaults];
    supportedLanguages = [NSArray arrayWithObjects:@"it",@"en",@"fr", nil];
    [self loadDefaults];
    [[SDImageCache sharedImageCache] addReadOnlyCachePath:@""];
    _network_status = AFNetworkReachabilityStatusUnknown;
    return self;
}

- (void)loadDefaults{
    
    _activeLanguage = [[BTLanguage shared] currentLanguage];
    //_activeLanguage = [_defaults objectForKey:@"user_language"];
    if(_activeLanguage == nil) _activeLanguage = @"en";
    
    //valid_currencies = [available_currencies componentsSeparatedByString:@","];
    //check if preferred currency is supported
    //if([valid_currencies indexOfObject:[locale objectForKey:NSLocaleCurrencyCode]] != NSNotFound)
    //    self.currency = [locale objectForKey:NSLocaleCurrencyCode];
    
    //can be nil
    _apiKey = [self settingsValueForKey:@"secretKey"];
    _curr_user_id = [self settingsValueForKey:@"userID"];
    
    NSLog(@"/******     LOADED SETTINGS         *****/");
    NSLog(@"/* Active UI lang: %@               *****/",[[BTLanguage shared] currentLanguage]);
    NSLog(@"/* Active data lang: %@             *****/",_activeLanguage);
    NSLog(@"/* Current user id: %@          *****/",_curr_user_id);
    NSLog(@"/* Api key: %@                  *****/",_apiKey);
    NSLog(@"/****************************************/");
    
}

#pragma mark - zip delegate methods

- (void)zipArchiveWillUnzipArchiveAtPath:(NSString *)path zipInfo:(unz_global_info)zipInfo{
    
}

- (void)zipArchiveDidUnzipArchiveAtPath:(NSString *)path zipInfo:(unz_global_info)zipInfo unzippedPath:(NSString *)unzippedPath{
    [UIApplication sharedApplication].idleTimerDisabled = NO;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DECOMPRESS_IMAGES_SUCCESS" object:nil];
}
/*
- (void)zipArchiveWillUnzipFileAtIndex:(NSInteger)fileIndex totalFiles:(NSInteger)totalFiles archivePath:(NSString *)archivePath fileInfo:(unz_file_info)fileInfo{
    NSLog(@"Going to unzip file %d/%d",fileIndex,totalFiles);
}
*/

- (void)zipArchiveDidUnzipFileAtIndex:(NSInteger)fileIndex totalFiles:(NSInteger)totalFiles archivePath:(NSString *)archivePath fileInfo:(unz_file_info)fileInfo{
    double progress = (double)(fileIndex / totalFiles);
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DECOMPRESS_IMAGES_PROGRESS" object:[NSNumber numberWithDouble:progress]];
}


#pragma mark -

#pragma mark - User account methods
/* User account stuff */
- (NSNumber*)currentUserID{
    return _curr_user_id;
}

- (int)current_user_id{
    if(self.currentUserID != nil)
        return self.currentUserID.intValue;
    else
        return 0;
}

- (BOOL)adminUser{
    NSLog(@"Current user id: %d",self.current_user_id);
    return (self.current_user_id > 0);
}

- (BOOL)signInWithUsername:(NSString *)username andPassword:(NSString *)password{
    
    
    NSLog(@"Actually authenticating locally!!");
    if([username isEqualToString:@"direzione"] && [password isEqualToString:@"MSRapp2014"]){
        _curr_user_id = [NSNumber numberWithInt:1];
        return YES;
    }
    return NO;
}

- (void)signOut{
    _curr_user_id = nil;
}

- (BOOL)signedIn{
    NSLog(@"Current user id: %d",self.current_user_id);
    return (self.current_user_id > 0);
}

/* NOT USED ACTUALLY */
 - (User*)currentUser{
 
 if(_curr_user_id != nil)
 return [User objectWithPredicate:[NSPredicate predicateWithFormat:@"code = %@",[_curr_user_id description]]];
 else
 return nil;
 }
 
- (void)signedInUser:(User*)me{
    
    if(me == nil)
        return;
    
    //set up user data
    [self saveSettingValue:me.user_secret forKey:@"secretKey"];
    [self saveSettingValue:me.code forKey:@"userID"];
    [self setHeaders];
    
    if(_deviceToken != nil)
        [self registerDevice:_deviceToken];
    
}

- (BOOL)loggedIn{
    return _curr_user_id != nil;
}

- (void)loginWithEmail:(NSString*)email andPassword:(NSString*)pass{
    //do something
    NSString *path = [NSString stringWithFormat:@"/users/login.json?email=%@&password=%@",email,pass];

    
    //prepare parameters (requestObject must be nil or it will try to serialize it)
    NSMutableURLRequest *request = [manager requestWithObject:nil method:RKRequestMethodPOST path:path parameters:nil];
    [request setValue:@"application/json" forHTTPHeaderField:@"content-type"];
    
    //success message
    NSString *successMessage = [self messageNameForObject:nil forAction:@"LOGIN" success:YES];
    if(successMessage == nil)
        successMessage = @"GENERIC_MESSAGE";
    //failure message
    NSString *failureMessage = [self messageNameForObject:nil forAction:@"LOGIN" success:NO];
    if(failureMessage == nil)
        failureMessage = @"ERROR_MESSAGE";
    
    RKManagedObjectRequestOperation *operation = [manager managedObjectRequestOperationWithRequest:request managedObjectContext:context success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult){
        // Success handler.
        NSManagedObject *obj = [mappingResult firstObject];
        
        NSLog(@"Request completed, successfully logged in: %@",successMessage);
        User *me = [mappingResult firstObject];
        
        //set up user data
        [self saveSettingValue:me.user_secret forKey:@"secretKey"];
        [self saveSettingValue:me.code forKey:@"userID"];
        _curr_user_id = me.code;
        [self setHeaders];
        
        
        [[NSNotificationCenter defaultCenter] postNotificationName:successMessage object:obj userInfo:nil];
        
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        // Error handler.
        [[NSNotificationCenter defaultCenter] postNotificationName:failureMessage object:error userInfo:nil];
    }];
    
    [operation start];

    
    
}

- (void)logout{
    //do something
    [self saveSettingValue:nil forKey:@"secretKey"];
    [self saveSettingValue:nil forKey:@"userID"];
    [self setHeaders]; //clean auth token header
    _curr_user_id = nil;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"USER_LOGGED_OUT" object:nil userInfo:nil];
}

- (void)resetPasswordForEmail:(NSString*)email{
    
    //NSURL *reqURL = [NSURL URLWithString:@"http://api.needle.me"];
    _httpClient.parameterEncoding = AFJSONParameterEncoding;
    
    
    NSDictionary *dict = [NSDictionary dictionaryWithObject:email forKey:@"email"];
    NSDictionary *wrapper = [NSDictionary dictionaryWithObject:dict forKey:@"user"];
    
    [_httpClient postPath:@"/auth/users/password.json" parameters:wrapper success:^(AFHTTPRequestOperation *op, id JSON){
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"PASSWORD_RESET_SUCCESS" object:nil userInfo:nil];
        
    } failure:^(AFHTTPRequestOperation *op, NSError *error){
        
        NSLog(@"Error requesting new password: %@",error.localizedDescription);
        
        UIAlertView *al = [[UIAlertView alloc] initWithTitle: @"Error requesting new password" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [al show];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"PASSWORD_RESET_FAILURE" object:nil userInfo:nil];
    }];
    
    
    
}
/* NOT USED ACTUALLY */
#pragma mark -

#pragma mark - Utility methods


/* User settings wrapper */
- (BOOL)saveSettingValue:(id)value forKey:(NSString*)key{
    
    if(value != nil)
        [_defaults setObject:value forKey:key];
    else
        [_defaults removeObjectForKey:key];
    
    return [_defaults synchronize];
}

- (id)settingsValueForKey:(NSString*)key{
    return [_defaults objectForKey:key];
}

- (BOOL)existImageBundle{
    NSString *bundle_path = [[[NSBundle mainBundle] bundlePath] stringByAppendingFormat:@"/images.bundle"];
    BOOL isDir = false;
    
    return [[NSFileManager defaultManager] fileExistsAtPath:bundle_path isDirectory:&isDir];
    
    
}

-(BOOL)existImagesInDocuments{
    
    NSFetchRequest *f = [Picture fetchRequest];
    NSSortDescriptor *s = [NSSortDescriptor sortDescriptorWithKey:@"code" ascending:YES];
    [f setSortDescriptors:@[s]];
    
    Picture *p = [Picture objectWithFetchRequest:f];
    
    if(p && [p localFilePath])
        return [[NSFileManager defaultManager] fileExistsAtPath:p.localFilePath];
    else
        return NO;
}

#pragma mark - database / pictures update helpers functions

- (NSDate*)databaseLastUpdate{
    return [self settingsValueForKey:@"database_last_update"];
}

- (NSDate*)stitchesPicturesLastUpdate{
    return [self settingsValueForKey:@"stitches_pictures_last_update"];
}

- (NSDate*)productsPicturesLastUpdate{
    return [self settingsValueForKey:@"products_pictures_last_update"];
}

- (NSDate*)productsSketchesLastUpdate{
    return [self settingsValueForKey:@"products_sketches_last_update"];
}

- (BOOL)setDatabaseLastUpdate:(NSDate*)date{
    return [self saveSettingValue:date forKey:@"database_last_update"];
}

- (BOOL)setStitchesPicturesLastUpdate:(NSDate*)date{
    return [self saveSettingValue:date forKey:@"stitches_pictures_last_update"];
}

- (BOOL)setProductsPicturesLastUpdate:(NSDate*)date{
    return [self saveSettingValue:date forKey:@"products_pictures_last_update"];
}

- (BOOL)setProductsSketchesLastUpdate:(NSDate*)date{
    return [self saveSettingValue:date forKey:@"products_sketches_last_update"];
}


#pragma mark -


- (void)retrieveImages{
    
    if([importQueue count] != 0){
        NSLog(@"Seeding database in progress can't proceed.");
        
        BTAlert *al = [[BTAlert alloc] initWithTitle:NSLocalizedString(@"Import already in progress", @"") text:NSLocalizedString(@"App update already in progress, please wait until completed.", @"") style:UIAlertViewStyleDefault onConfirm:nil onDismiss:nil];
        
        [al show];
        return;
    }
    
    //DISABLING AUTOLOCK SCREEN
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    
    //supplier must be the first model to import
    importQueue = [[NSMutableArray alloc] initWithArray:@[
                                                          @"/archives_count.json?archive=stitches_images",
                                                          @"/archives_count.json?archive=products_images",
                                                          @"/archives_count.json?archive=products_sketches",
                                                          ]];
    
    [self getImagesArchives];
    
}

- (void)getImagesArchives{
    
    
    if([importQueue count] == 0){
        //IMPORT FINISHED
        //ENABLING AUTOLOCK SCREEN
        [UIApplication sharedApplication].idleTimerDisabled = NO;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"IMPORT_COMPLETED" object:nil];
        return;
    }
    
    
    
    //change archive type, first get the archives count
    _importPath = [importQueue objectAtIndex:0];
    [importQueue removeObjectAtIndex:0];
    
    if([_importPath rangeOfString:@"zip"].location != NSNotFound){
        [self retrieveImagesArchiveAtURL:_importPath];
        return;
    }
    
    //otherwise is the archives count request url
    if(!_httpClient)
        _httpClient = [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:[self apiURL]]];
    
    _httpClient.parameterEncoding = AFJSONParameterEncoding;
    [_httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    [_httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    
    
    //REQUESTING TOTALS FOR VARIOUS RECORDS
    
    [_httpClient getPath:_importPath parameters:nil success:^(AFHTTPRequestOperation *op, NSDictionary* JSON){
        
        int archives_count = [[JSON objectForKey:@"count"] intValue];
        NSLog(@"Total count for %@: %d",_importPath,archives_count);
        
        
        NSString *archive_name = [_importPath stringByReplacingOccurrencesOfString:@"/archives_count.json?archive=" withString:@""];
        NSString *message_name = [NSString stringWithFormat:@"%@_COUNT",[archive_name uppercaseString]];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:message_name object:[JSON objectForKey:@"count"]];
        
        //insert into queue paths for archives to download
        for (int i = (archives_count); i > 0; i--) {
            NSString *archive_url = [NSString stringWithFormat:@"%@/system/archives/%@_%d.zip",self.apiURL,archive_name,i];
            [importQueue insertObject:archive_url atIndex:0];
        }
        
        //recall itself to verify what's next url in queue
        [self getImagesArchives];
        
        
    } failure:^(AFHTTPRequestOperation *op, NSError *error){
        NSLog(@"Count request failed: %@", error.localizedDescription);
        [[NSNotificationCenter defaultCenter] postNotificationName:@"IMPORT_FAILURE" object:error];
    }];
    
    //[self retrieveImagesArchiveAtURL:_importPath];
    
}

//get image archive from amazon S3
- (void)downloadAllImages{
    
    
    if([importQueue count] != 0){
        NSLog(@"Seeding database in progress can't proceed.");
        
        BTAlert *al = [[BTAlert alloc] initWithTitle:NSLocalizedString(@"Import already in progress", @"") text:NSLocalizedString(@"Database import already in progress, please wait until completed.", @"") style:UIAlertViewStyleDefault onConfirm:nil onDismiss:nil];
        
        [al show];
        return;
    }
    
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *file_path = [NSString stringWithFormat:@"%@/images.zip",documentsDirectory];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:file_path]){
        BTAlert *al = [[BTAlert alloc] initWithTitle:NSLocalizedString(@"Extract archive?", @"") text:NSLocalizedString(@"Image archive found inside application, maybe previous extraction failed. Would you like to extract it? Otherwise it will be downloaded again.", @"Extract confirm on previous downloaded archive") style:UIAlertViewStyleDefault onConfirm:^(UIAlertView *alert) {
            
            [self unzipArchive:file_path];
        } onDismiss:^(UIAlertView *alert){
            
            //download archive from amazon S3
            [self retrieveS3ArchiveURL:^(NSString *remote_url) {
                [self retrieveImagesArchiveAtURL:remote_url];
            }];
            
        }];
        
        [al show];
        
        return;
    }
    
    
    
    //download archive from amazon S3
    [self retrieveS3ArchiveURL:^(NSString *remote_url) {
        [self retrieveImagesArchiveAtURL:remote_url];
    }];
    
}

- (void)downloadUpdatedImages{
    
    if([importQueue count] != 0){
        NSLog(@"Seeding database in progress can't proceed.");
        
        BTAlert *al = [[BTAlert alloc] initWithTitle:NSLocalizedString(@"Import already in progress", @"") text:NSLocalizedString(@"Database import already in progress, please wait until completed.", @"") style:UIAlertViewStyleDefault onConfirm:nil onDismiss:nil];
        
        [al show];
        return;
    }
    
    //DISABLING AUTOLOCK SCREEN
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    
    //supplier must be the first model to import
    importQueue = [[NSMutableArray alloc] initWithArray:@[
                                                          [NSString stringWithFormat:@"%@%@",self.apiURL, @"/system/archives/updated_stitches_images.zip"],
                                                          [NSString stringWithFormat:@"%@%@",self.apiURL, @"/system/archives/updated_stitches_images.zip"],
                                                          [NSString stringWithFormat:@"%@%@",self.apiURL, @"/system/archives/updated_stitches_images.zip"],
                                                          ]];
    
    [self getImagesArchives];
    
}

- (void)unsetUpdated{
    
    NSPredicate *p = [NSPredicate predicateWithFormat:@"updated = %@",[NSNumber numberWithBool:YES]];
    NSArray *updated_pictures = [Picture objectsWithPredicate:p];
    
    NSError *err= nil;
    for (Picture *tmp in updated_pictures) {
        tmp.updated = [NSNumber numberWithBool:NO];
    }
    
    Picture *a = nil;
    if([updated_pictures count] > 0){
        a = [updated_pictures firstObject];
        [a.managedObjectContext save:&err];
    }
    
}

- (void)retrieveS3ArchiveURL: (void (^)(NSString *remote_url))callback_block{
    
    //DISABLING AUTOLOCK SCREEN
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    
        _httpClient = [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:[self apiURL]]];
        _httpClient.parameterEncoding = AFJSONParameterEncoding;
        [_httpClient setDefaultHeader:@"Accept" value:@"application/json"];
        [_httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
        
        [_httpClient getPath:@"/pictures/download_url" parameters:nil success:^(AFHTTPRequestOperation *op, NSDictionary* JSON){
 
            NSString *secure_url = [JSON objectForKey:@"secure_url"];
            NSLog(@"Received amazon secure url: %@",secure_url);
            
            //ENABLING AUTOLOCK SCREEN
            [UIApplication sharedApplication].idleTimerDisabled = NO;
            
            if(callback_block)
                callback_block(secure_url);
            
        } failure:^(AFHTTPRequestOperation *op, NSError *error){
            
            NSLog(@"Error verifying app activation: %@",error.localizedDescription);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DOWNLOAD_IMAGES_FAILURE" object:nil userInfo:nil];
            
            //DISABLING AUTOLOCK SCREEN
            [UIApplication sharedApplication].idleTimerDisabled = NO;

        }];
     
}

- (void)retrieveImagesArchiveAtURL:(NSString*)url{
    
    NSString *destinationFolder = @"";
    
    if([url rangeOfString:@"stitches"].location != NSNotFound){
        destinationFolder = [NSString stringWithFormat:@"%@/%@",[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0],@"stitches"];
    }
    else if([url rangeOfString:@"products_images"].location != NSNotFound){
        destinationFolder = [NSString stringWithFormat:@"%@/%@",[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0],@"products/images"];
    }
    else if([url rangeOfString:@"products_sketches"].location != NSNotFound){
        destinationFolder = [NSString stringWithFormat:@"%@/%@",[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0],@"products/sketches"];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DOWNLOAD_IMAGES_STARTED" object:nil];
    
    //DISABLING AUTOLOCK SCREEN
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *file_name = [[url componentsSeparatedByString:@"/"] lastObject];
        NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:file_name];
        NSLog(@"Retrieving images from %@",url);
        
        operation.outputStream = [NSOutputStream outputStreamToFileAtPath:path append:NO];
        
        [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
            double percentDone = ((double)(totalBytesRead) / (double)(totalBytesExpectedToRead));
            //NSLog(@"Current %% value %.2f (%lld bytes of %lld)",percentDone,totalBytesRead,totalBytesExpectedToRead);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DOWNLOAD_IMAGES_PROGRESS" object:[NSNumber numberWithDouble:percentDone]];
        }];
        
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DOWNLOAD_IMAGES_SUCCESS" object:nil];
            
            NSLog(@"Successfully downloaded file to %@", path);
            [self unzipArchive:path toPath:destinationFolder];

            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            
            if(operation.response.statusCode != 404)
                [[NSNotificationCenter defaultCenter] postNotificationName:@"DOWNLOAD_IMAGES_FAILURE" object:error];
            
            //DISABLING AUTOLOCK SCREEN
            //[UIApplication sharedApplication].idleTimerDisabled = NO;
            [self getImagesArchives];
        }];
        
        [operation start];
        
        });
    
}

- (void)unzipArchive:(NSString*)file_path{
    [self unzipArchive:file_path toPath:@""];
}

- (void)unzipArchive:(NSString*)file_path toPath:(NSString*)destinationPath{
    //needs absolute path
    if(!destinationPath || [destinationPath isEqualToString:@""])
        destinationPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];

    if(![[NSFileManager defaultManager] fileExistsAtPath:file_path]){
        NSLog(@"Image archive not found");
        [[NSNotificationCenter defaultCenter] postNotificationName:@"DECOMPRESS_IMAGES_FAILURE" object:nil];
        return;
    }
    
    BOOL isDir = NO;
    NSError *err;
    
    if(![[NSFileManager defaultManager] fileExistsAtPath:destinationPath isDirectory:&isDir]){
        NSLog(@"Missing destination folder, going to create it");
        [[NSFileManager defaultManager] createDirectoryAtPath:destinationPath withIntermediateDirectories:YES attributes:nil error:&err];
        
        if(err){
            NSLog(@"Unable to create destination path: %@",err.localizedDescription);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DECOMPRESS_IMAGES_FAILURE" object:err];
            return;
        }
            
    }
    
    
    
    if(![[NSFileManager defaultManager] fileExistsAtPath:destinationPath isDirectory:&isDir]){
        NSLog(@"Wrong destination folder");
        [[NSNotificationCenter defaultCenter] postNotificationName:@"DECOMPRESS_IMAGES_FAILURE" object:nil];
        return;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DECOMPRESS_IMAGES_STARTED" object:nil];
    dispatch_async(dispatch_get_main_queue(), ^{
        
        //DISABLING AUTOLOCK SCREEN
        [UIApplication sharedApplication].idleTimerDisabled = YES;
        
        @try { //FileAtPath:file_path toDestination:documentsDirectory delegate:self];
            NSError *err = nil;
            BOOL res = [SSZipArchive unzipFileAtPath:file_path toDestination:destinationPath overwrite:YES password:nil error:&err delegate:self];
            //[self unsetUpdated];
            if(!res){
                NSLog(@"Extraction failed: %@",err.localizedDescription);
                [[NSNotificationCenter defaultCenter] postNotificationName:@"DECOMPRESS_IMAGES_FAILURE" object:nil];
            }
            else{
                BOOL isDir = NO;
                NSError *err;
                if([[NSFileManager defaultManager] fileExistsAtPath:file_path isDirectory:&isDir]){
                    NSLog(@"Removing extracted archive");
                    [[NSFileManager defaultManager] removeItemAtPath:file_path error:&err];
                    
                    if(err){
                        NSLog(@"Warning: unable to delete archive: %@\n\n%@",file_path,err.localizedDescription);
                    }
                }
                
                //used only in images download
                [self getImagesArchives];
            }
            
            //ENABLING AUTOLOCK SCREEN
            //[UIApplication sharedApplication].idleTimerDisabled = NO;
        }
        @catch (NSException *exception) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DECOMPRESS_IMAGES_FAILURE" object:exception];
            
            //ENABLING AUTOLOCK SCREEN
            [UIApplication sharedApplication].idleTimerDisabled = NO;
        }
 
        
        
    });
    
}

- (BTSeeder *)seeder{
    return [[BTSeeder alloc] init];
}

- (void)syncWithRemote{

    //DISABLING AUTOLOCK SCREEN
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    
    NSLog(@"Syncing with server...");
    _httpClient = [self clientWithBaseURL:[NSURL URLWithString:[self apiURL]]];
    
    //flush database
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    //notify to viewControllers
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DATABASE_CLEANED" object:nil];
    
    
    //supplier must be the first model to import
    importQueue = [[NSMutableArray alloc] initWithArray:@[
                @"/macro_seasons/count",
                @"/suppliers/count",
                @"/yarns/count",
                @"/machines/count",
                @"/products/count",
                @"/brands/count",
                @"/stitch_types/count",
                @"/stitches/count",
                
                @"/macro_seasons",
                @"/suppliers",
                @"/yarns",
                @"/machines",
                @"/products",
                @"/brands",
                
                @"/stitch_types",
                @"/en/stitch_types",
                @"/fr/stitch_types",
                
                @"/stitches",
                @"/en/stitches",
                @"/fr/stitches"
                ]];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"IMPORT_STARTED" object:nil];
    
    [self importDataFromPath:nil atPage:1];
    
}

- (void)importDataFromPath:(NSString*)path atPage:(int)page{
    
    _importPath = path;
    _importPage = page;
    NSString *language = @"it";
    
    if(!_importPath){
        
        if([importQueue count] == 0){
            //IMPORT FINISHED
            //ENABLING AUTOLOCK SCREEN
            [UIApplication sharedApplication].idleTimerDisabled = NO;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"IMPORT_COMPLETED" object:nil];
            return;
        }
        
        //change model, first get the objects count
        _importPath = [importQueue objectAtIndex:0];
        [importQueue removeObjectAtIndex:0];
        
        _httpClient.parameterEncoding = AFJSONParameterEncoding;
        [_httpClient setDefaultHeader:@"Accept" value:@"application/json"];
        [_httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
        
        language = [self extractLanguageFromRequestPath:_importPath];
        
        //REQUESTING TOTALS FOR VARIOUS RECORDS
        if([_importPath rangeOfString:@"count"].location != NSNotFound){
            [_httpClient getPath:_importPath parameters:nil success:^(AFHTTPRequestOperation *op, NSDictionary* JSON){
                
                NSLog(@"Total count for %@: %d",_importPath,[[JSON objectForKey:@"count"] intValue]);

                NSString *model_name = [self extractModelName:_importPath];
                //[NSString stringWithFormat:@"%@_COUNT",[[[_importPath stringByReplacingOccurrencesOfString:@"/" withString:@""] stringByReplacingOccurrencesOfString:@"count" withString:@""] uppercaseString]];
                
                NSLog(@"Requesting localized version in %@ for %@",language,model_name);
                
                [[NSNotificationCenter defaultCenter] postNotificationName:model_name object:[JSON objectForKey:@"count"]];
                
                //next import queue entry
                [self importDataFromPath:nil atPage:_importPage];
                
            } failure:^(AFHTTPRequestOperation *op, NSError *error){
                NSLog(@"Count request failed: %@", error.localizedDescription);
                [[NSNotificationCenter defaultCenter] postNotificationName:@"IMPORT_FAILURE" object:error];
            }];
            
            return;
        }
        
    }
    
    NSLog(@"Requesting items for %@", _importPath);
    
    language = [self extractLanguageFromRequestPath:_importPath];
    
    //recursive call
    [self requestDataAtPath:_importPath forAction:@"INDEX" withMethod:RKRequestMethodGET withParams:@{@"limit":@"100",@"page":[NSNumber numberWithInt:_importPage]} successBlock:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        
        NSString *model = [self extractModelName:_importPath];
        //[[_importPath stringByReplacingOccurrencesOfString:@"/" withString:@""] uppercaseString];
        NSLog(@"IMPORTED %@ AT PAGE %d",_importPath,_importPage);
        
        //IMPORT_MODEL_PAGE_NR_SUCCESS
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"IMPORT_%@_PAGE_SUCCESS",model] object:@{@"page":[NSNumber numberWithInt:_importPage]}];

        //reset parameters so a new model will be imported
        if (mappingResult.count == 0){
            NSLog(@"Completed import for %@",model);
            
            [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"IMPORT_%@_COMPLETED",model] object:nil];
            
            _importPage = 1;
            _importPath = nil;
        }
        else{
            _importPage++;
        }
        
        [self importDataFromPath:_importPath atPage:_importPage];
        
    } failureBlock:^(RKObjectRequestOperation *operation, NSError *error) {
        NSLog(@"IMPORT FAILED FOR %@ AT PAGE %d",_importPath,_importPage);
        
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"IMPORT_%@_PAGE_FAILURE",[[_importPath stringByReplacingOccurrencesOfString:@"/" withString:@""] uppercaseString]] object:nil];
        
        //proceed with next model
        return;
        [self importDataFromPath:nil atPage:1];
    }];

    
}

- (NSString*)extractLanguageFromRequestPath:(NSString*)url_path{
    
    NSString *language = @"it";
    
    if([[self availableLanguages] indexOfObject:[url_path substringWithRange:(NSRange){1,2}]] != NSNotFound){
        language = [url_path substringWithRange:(NSRange){1,2}];
        NSLog(@"Found language: %@",language);
        url_path = [url_path substringFromIndex:3];
        
    }//specified locale
    return language;
}

- (NSString*)extractModelName:(NSString*)url_path{
    
    NSString *language = [self extractLanguageFromRequestPath:url_path];
    
    NSString *model_name = @"";
    
    if([url_path rangeOfString:@"count"].location != NSNotFound)
        model_name = [NSString stringWithFormat:@"%@_COUNT",[[[url_path stringByReplacingOccurrencesOfString:@"/" withString:@""] stringByReplacingOccurrencesOfString:@"count" withString:@""] uppercaseString]];
    else
        [[url_path stringByReplacingOccurrencesOfString:@"/" withString:@""] uppercaseString];
    
    if(![language isEqualToString:@"it"])
        model_name = [[language uppercaseString] stringByAppendingFormat:@"_%@",model_name];
        
    return model_name;
    
}

- (NSArray*)availableLanguages{
    return supportedLanguages;
}


- (NSString*)language{
    return (_activeLanguage == nil || [_activeLanguage isEqualToString:@""] ? [self systemLanguage] : _activeLanguage);
}

- (NSString*)systemLanguage{
    return [[[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"] objectAtIndex:0] substringToIndex:2];
}

- (void)setLanguage:(NSString*)language{
    if(language != _activeLanguage){
    
        //Going to tell view controllers to stop
        [[NSNotificationCenter defaultCenter] postNotificationName:@"CHANGING_LANGUAGE" object:nil userInfo:nil];
        [[NSURLCache sharedURLCache] removeAllCachedResponses];
        
        //reload all data
        _activeLanguage = language;
        [self saveSettingValue:_activeLanguage forKey:@"user_language"];

        [self syncWithRemote];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"CHANGED_LANGUAGE" object:nil userInfo:nil];
    }
    
}

- (void) sendPushNotificationtoUserWithID:(NSString*)userID withMessage:(NSString*)message{
    
    if(!userID)
        return;
#ifdef PFQuery
    // Create our Installation query
    PFQuery *pushQuery = [PFInstallation query];
    [pushQuery whereKey:@"uid" equalTo:userID];
    
    // Send push notification to query
    [PFPush sendPushMessageToQueryInBackground:pushQuery
                                   withMessage:message];
    [SVProgressHUD show];
    [SVProgressHUD dismissWithSuccess:@"Sent!" afterDelay:2.0];
#endif
}

- (void)registerDevice:(NSString*)deviceToken{
    
    if(![self loggedIn])
        return;
    
    //keep a reference
    _deviceToken = deviceToken;
    
    NSString *openUDID = [OpenUDID value];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:deviceToken forKey:@"device_token"];
    [defaults setObject:openUDID forKey:@"openUDID"];
    [defaults synchronize];

#ifdef PFInstallation
    //Parse push notifications
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceToken:deviceToken];
    
    //collect additional data
    NSString *locale = [[NSLocale currentLocale] localeIdentifier];
    
    NSString *user_id = ([self currentUser] != nil ? [[[self currentUser] code] description] : @"");
    
    //NSString *user_id = @"0";
    NSString *device_name = [[UIDevice currentDevice] name];
    NSString *os_version = [[UIDevice currentDevice] systemVersion];
    NSString *jailbroken = ([NMHelpers jailbrokenDevice] ? @"YES" : @"NO");
    NSString *language = [BTLanguage currentLanguage];
    NSArray *default_channels = @[@"default"];
    
    NSDictionary *dict = [NSDictionary dictionaryWithObjects:@[locale,device_name,user_id,openUDID,os_version,jailbroken,language,default_channels] forKeys:@[@"locale",@"device_name",@"uid",@"unique_id",@"os_version",@"jailbroken",@"language",@"channels"]];
    
    [currentInstallation setValuesForKeysWithDictionary:dict];
    [currentInstallation saveInBackground];
#endif
    
#ifdef TestFlight
    //setup test flight
    [TestFlight setDeviceIdentifier:[OpenUDID value]];
    [TestFlight setVersion:[NMHelpers.appBuildNr intValue]];
    [TestFlight takeOff:@"2b90afc8-de19-4999-96a7-0ccb5b1b4da6"];
#endif
    
}

- (void)setEnvironment:(NSString*)env{
    _environment = env;
    
    networking = [[BTNetworking alloc] initWithBaseUrl:[self apiURL] modelName:@"MSR"];
    [self setHeaders];
    manager = [RKObjectManager sharedManager];
    
    context =  manager.managedObjectStore.persistentStoreManagedObjectContext;

    //update connectivity flag automatically
    [manager.HTTPClient setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        _network_status = status;
        if (status == AFNetworkReachabilityStatusNotReachable) { 
            NSLog(@"[REACHABILITY] Connection not available.");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"NOT_CONNECTED" object:nil];
        }
        else{
            NSLog(@"[REACHABILITY] Connection available.");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"CONNECTED" object:nil];
        }
    }];

    if([self networkAvailable])
        [self syncWithRemote];
}

- (NSString*)environment{
    return _environment;
}

- (NSString*)apiURL{
    
    if([_environment isEqualToString:@"development"]) //ubuntu development VM
        return @"http://localhost:3000";
    else if([_environment isEqualToString:@"staging"]) //local network / VPN
        return @"http://192.168.1.225:5000";
    else if([_environment isEqualToString:@"production"]) //local network / VPN
        return @"http://miles.bluetouchdev.com:5000";
    else //old APIs
        return @"";
}

- (void)setHeaders{
    
    RKObjectManager *m = [RKObjectManager sharedManager];
    
    if([self loggedIn] && (_apiKey != nil)) //resetto l'header
        [m.HTTPClient setDefaultHeader:@"X-MSR-Access-Token" value:_apiKey];
    else
        [m.HTTPClient setDefaultHeader:@"X-MSR-Access-Token" value:@""];

    
}

#pragma mark - RestKit methods

- (RKObjectManager*)manager{
   return manager;
}

- (void)reachabilityStatusChanged:(NSNotification*)notification{
    
    if([self networkAvailable])
        [self syncWithRemote];
    else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NOT_CONNECTED" object:nil userInfo:nil];
    }
}

- (BOOL)networkAvailable{
    return (_network_status != AFNetworkReachabilityStatusNotReachable && _network_status != AFNetworkReachabilityStatusUnknown);
}

#pragma mark - CRUD VERBS METHODS

- (void)listObjectsForClass:(Class)class atPath:(NSString*)customPath forAction:(NSString*)customActioName withParams:(NSDictionary*)params{
    [self listObjectsForClass:class atPath:customPath forAction:customActioName withParams:params onSuccess:nil onFailure:nil];
}

- (void)listObjectsForClass:(Class)class atPath:(NSString*)customPath forAction:(NSString*)customActioName withParams:(NSDictionary*)params onSuccess:(void (^)(NSArray *objects))successBlock onFailure:(void (^)(NSError *error))failureBlock{
    
    NSManagedObject* object = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass(class) inManagedObjectContext:context];
    
    if(object == nil)
        return;
    
    if(![self networkAvailable]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NOT_CONNECTED" object:nil];
        return;
    }
    
    //add user params to default params
    NSMutableDictionary *_params = [NSMutableDictionary dictionaryWithDictionary:[self defaultParams]];
    if(params != nil)
        [_params addEntriesFromDictionary:params];
    
    NSString *action = (customActioName == nil ? @"INDEX" : customActioName);
    
    //success message
    NSString *successMessage = [self messageNameForObject:object forAction:action success:YES];
    if(successMessage == nil)
        successMessage = @"GENERIC_MESSAGE";
    //failure message
    NSString *failureMessage = [self messageNameForObject:object forAction:action success:NO];
    if(failureMessage == nil)
        failureMessage = @"ERROR_MESSAGE";
        
    [RKObjectManager.sharedManager getObjectsAtPath:customPath parameters:params success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult)
     {
         // Success handler.
         NSLog(@"Request completed, sending: %@",successMessage);
         if(successBlock)
             successBlock(mappingResult.array);
         else
             [[NSNotificationCenter defaultCenter] postNotificationName:successMessage object:mappingResult.array userInfo:nil];
         
     } failure:^(RKObjectRequestOperation *operation, NSError *error) {
         // Error handler.
         NSLog(@"Request failed");
         if(failureBlock)
             failureBlock(error);
         else
             [[NSNotificationCenter defaultCenter] postNotificationName:failureMessage object:error userInfo:nil];
     }];
    
}

- (void)getObjectAsync:(NSManagedObject *)object withParams:(NSDictionary*)params{
    
    if(object == nil)
        return;
    
    if(![self networkAvailable]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NOT_CONNECTED" object:nil];
        return;
    }
    
    //add user params to default params
    NSMutableDictionary *_params = [NSMutableDictionary dictionaryWithDictionary:[self defaultParams]];
    if(params != nil)
        [_params addEntriesFromDictionary:params];
    
    NSNumber *code = [object valueForKey:@"code"];
    
    if(code == nil || code.intValue < 1){
        RKLogDebug(@"Received not persisted object");
        return;
    }
    
    
    //choose if POST or PUT
    
    NSString *action = @"GET";
    
    //success message
    NSString *successMessage = [self messageNameForObject:object forAction:action success:YES];
    if(successMessage == nil)
        successMessage = @"GENERIC_MESSAGE";
    //failure message
    NSString *failureMessage = [self messageNameForObject:object forAction:action success:NO];
    if(failureMessage == nil)
        failureMessage = @"ERROR_MESSAGE";
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"REQUEST_STARTED" object:nil];
    
    
    [manager getObject:object path:nil parameters:params success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult)
     {
         // Success handler.
         NSManagedObject *obj = [mappingResult firstObject];
         
         NSLog(@"Request completed, sending: %@",successMessage);
         [[NSNotificationCenter defaultCenter] postNotificationName:successMessage object:obj userInfo:nil];
     }
     failure:^(RKObjectRequestOperation *operation, NSError *error) {
         // Error handler.
         [[NSNotificationCenter defaultCenter] postNotificationName:failureMessage object:error userInfo:nil];
     }];
    
    
}

- (void)requestDataAtPath:(NSString*)path forAction:(NSString*)actionName withMethod:(RKRequestMethod)method withParams:(NSDictionary*)params{
    [self requestDataAtPath:path forAction:actionName withMethod:method withParams:params successBlock:nil failureBlock:nil];
}
    
- (void)requestDataAtPath:(NSString*)path forAction:(NSString*)actionName withMethod:(RKRequestMethod)method withParams:(NSDictionary*)params successBlock:(void (^)(RKObjectRequestOperation *operation, RKMappingResult *mappingResult))success failureBlock:(void (^)(RKObjectRequestOperation *operation, NSError *error))failure{
    
    NSMutableURLRequest *request = [manager requestWithObject:nil method:method path:path parameters:params];
    [request setValue:@"application/json" forHTTPHeaderField:@"content-type"];
    
    //success message
    NSString *successMessage = [self messageNameForObject:nil forAction:actionName success:YES];
    if(successMessage == nil)
        successMessage = @"GENERIC_MESSAGE";
    //failure message
    NSString *failureMessage = [self messageNameForObject:nil forAction:actionName success:NO];
    if(failureMessage == nil)
        failureMessage = @"ERROR_MESSAGE";
    
    
    if(success == nil){
        
        success = ^void(RKObjectRequestOperation *operation, RKMappingResult *mappingResult){
            // Generic success handler.
            NSManagedObject *obj = [mappingResult firstObject];
            NSLog(@"Request completed for %@: %@",actionName, successMessage);
            
            [[NSNotificationCenter defaultCenter] postNotificationName:successMessage object:obj userInfo:nil];
        };
    }
    
    if(failure == nil){
    
        failure = ^void(RKObjectRequestOperation *operation, NSError *error){
            // Generic rror handler.
            [[NSNotificationCenter defaultCenter] postNotificationName:failureMessage object:error userInfo:nil];
        };
    }
    
    RKManagedObjectRequestOperation *operation = [manager managedObjectRequestOperationWithRequest:request managedObjectContext:context success:success failure:failure];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"REQUEST_STARTED" object:nil];
    [operation start];
    
}

- (void)saveObjectAsync:(NSManagedObject *)object{
    [self saveObjectAsync:object withParams:nil withAttachments:nil mimeType:nil];
}

- (void)saveObjectAsync:(NSManagedObject *)object withParams:(NSDictionary*)params{
    [self saveObjectAsync:object withParams:params withAttachments:nil mimeType:nil];
}

- (void)saveObjectAsync:(NSManagedObject *)object withParams:(NSDictionary*)params withAttachments:(NSDictionary*)attachments mimeType:(NSString*)mimetype{
    
    if(object == nil)
        return;
    
    if(![self networkAvailable]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NOT_CONNECTED" object:nil];
        return;
    }
    
    //add user params to default params
    NSMutableDictionary *_params = [NSMutableDictionary dictionaryWithDictionary:[self defaultParams]];
    if(params != nil)
        [_params addEntriesFromDictionary:params];
    
    //choose if POST or PUT
    
    NSNumber *code = [object valueForKey:@"code"];
    NSInteger method = RKRequestMethodPUT; //update object
    NSString *action = @"PUT";
    
    if(code == nil || [code intValue] < 1){ //new object
        method = RKRequestMethodPOST;
        action = @"POST";
    };
    
    
    
    //prepare parameters
    NSMutableURLRequest *request = [manager multipartFormRequestWithObject:object method:RKRequestMethodPOST
                                             path:nil parameters:params
                        constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
     {
         if(attachments != nil)
             [self prepareParams:attachments withMultipartData:formData withMimeType:mimetype];
     }];
    
    
    //success message
    NSString *successMessage = [self messageNameForObject:object forAction:action success:YES];
    if(successMessage == nil)
        successMessage = @"GENERIC_MESSAGE";
    //failure message
    NSString *failureMessage = [self messageNameForObject:object forAction:action success:NO];
    if(failureMessage == nil)
        failureMessage = @"ERROR_MESSAGE";
    
    RKManagedObjectRequestOperation *operation = [manager managedObjectRequestOperationWithRequest:request managedObjectContext:
                                                  context success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult)
     {
         // Success handler.
         NSManagedObject *obj = [mappingResult firstObject];
         
         NSLog(@"Request completed, sending: %@",successMessage);
         [[NSNotificationCenter defaultCenter] postNotificationName:successMessage object:obj userInfo:nil];
         
     } failure:^(RKObjectRequestOperation *operation, NSError *error) {
         // Error handler.
         [[NSNotificationCenter defaultCenter] postNotificationName:failureMessage object:error userInfo:nil];
     }];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"REQUEST_STARTED" object:nil];
    [operation start];
}

- (void)deleteObjectAsync:(NSManagedObject *)object{
    
    if(object == nil)
        return;
    
    if(![self networkAvailable]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NOT_CONNECTED" object:nil];
        return;
    }
    
    /*
    [manager deleteObject:object usingBlock:^(RKObjectLoader *loader){
        
        loader.onDidFailLoadWithError = [self failureBlockForObject:object forAction:@"DELETE"];
        loader.onDidLoadObject = [self successBlockForObject:object forAction:@"DELETE"];
        
        loader.onDidLoadResponse = ^(RKResponse *res){
            RKLogDebug(@"Received response: %@",res.bodyAsString);
        };
        
    }];
     */
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"REQUEST_STARTED" object:nil];
}





#pragma mark -

- (NSDictionary*)defaultParams{
    NSString *language = [self language];
    
    return [NSDictionary dictionaryWithObject:language forKey:@"lang"];
}

- (void)prepareParams:(NSDictionary*)params withMultipartData:(id<AFMultipartFormData>)formData withMimeType:(NSString*)mimetype{
    
    if(params != nil){
        
        
        for (NSString *key in params.allKeys) {
            /*
            [formData appendPartWithFileData:UIImageJPEGRepresentation(obj.photo, 0.7)
                                        name:@"expense[photo_file]"
                                    fileName:@"photo.jpg"
                                    mimeType:mimetype];
             */
            
            
            if([[params objectForKey:key] isKindOfClass:[UIImage class]]){ //attached UIImage
                UIImage *tmp = (UIImage*)[params objectForKey:key];
                [formData appendPartWithFormData:UIImageJPEGRepresentation(tmp, 1.0) name:key];
                
            }
            else if([[params objectForKey:key] isKindOfClass:[NSData class]]){ //binary file?
                NSData *tmp = (NSData*)[params objectForKey:key];
                [formData appendPartWithFormData:tmp name:key];
            }
            else{ //simple data
                NSLog(@"SKIPPING PARAMETER '%@'",key);
            }
        }
        
    }
    
}

- (NSString*)messageNameForObject:(NSManagedObject*)object forAction:(NSString*)action success:(BOOL)success{
    
    NSString *name = @"";
    
    if(object != nil)
        name = NSStringFromClass(object.class).uppercaseString;
    
    NSString *messageName = nil;
    
    if ([action isEqualToString:@"CREATE"]) {
        messageName = [NSString stringWithFormat:@"CREATE_%@_%@",name,(success ? @"SUCCESS" : @"FAILURE")];
    }
    else if ([action isEqualToString:@"READ"] || [action isEqualToString:@"GET"]) {
        messageName = [NSString stringWithFormat:@"READ_%@_%@",name,(success ? @"SUCCESS" : @"FAILURE")];
    }
    else if ([action isEqualToString:@"UPDATE"]) {
        messageName = [NSString stringWithFormat:@"UPDATE_%@_%@",name,(success ? @"SUCCESS" : @"FAILURE")];
    }
    else if ([action isEqualToString:@"DELETE"]) {
        messageName = [NSString stringWithFormat:@"DELETE_%@_%@",name,(success ? @"SUCCESS" : @"FAILURE")];
    }
    else if ([action isEqualToString:@"INDEX"]) {
        messageName = [NSString stringWithFormat:@"INDEX_%@_%@",name,(success ? @"SUCCESS" : @"FAILURE")];
    }
    else{ //custom action
        if([name isEqualToString:@""])
            messageName = [NSString stringWithFormat:@"%@_%@",action.uppercaseString, (success ? @"SUCCESS" : @"FAILURE")];
        else
            messageName = [NSString stringWithFormat:@"%@_%@_%@",action.uppercaseString,name, (success ? @"SUCCESS" : @"FAILURE")];
    }
    

    LogTrace(@"Message created: %@",messageName);
    
    return messageName;
}


@end
