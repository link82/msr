//
//  MSRStitchCell.h
//  MSR
//
//  Created by Davide Cenzi on 21/12/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MSRStitchCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UIImageView *stitchImage;
@property (nonatomic, weak) IBOutlet UILabel *codeLabel;
@property (nonatomic, weak) IBOutlet UILabel *supplierLabel;
@property (nonatomic, weak) IBOutlet UILabel *yarnLabel;

@end
