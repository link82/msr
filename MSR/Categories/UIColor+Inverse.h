//
//  UIColor+Inverse.h
//  Needle
//
//  Created by Davide Cenzi on 21/08/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor(Inverse)

- (UIColor*)inverseColor;

@end
