//
//  NSString+UrlAddictions.h
//  Needle
//
//  Created by Davide Cenzi on 23/08/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (UrlAddictions)

-(NSString *)urlEncodedString;
-(NSString *)urlDecodedString;

@end