 //
//  main.m
//  MSR
//
//  Created by Davide Cenzi on 07/12/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MSRAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MSRAppDelegate class]));
    }
}
    