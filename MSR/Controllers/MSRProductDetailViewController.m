//
//  MSRProductDetailViewController.m
//  MSR
//
//  Created by Davide Cenzi on 19/01/14.
//  Copyright (c) 2014 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import "MSRProductDetailViewController.h"
#import "MSRZoomViewController.h"

@interface MSRProductDetailViewController ()

@end

@implementation MSRProductDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)cleanView{
    /*
    [self.titleSketchLabel setFont:[UIFont fontWithName:@"Futura-CondensedMedium" size:20.0]];
    [self.titleImageLabel setFont:[UIFont fontWithName:@"Futura-CondensedMedium" size:20.0]];
    [self.descriptionLabelTitle setFont:[UIFont fontWithName:@"Futura-CondensedMedium" size:20.0]];
    [self.categoryLabelTitle setFont:[UIFont fontWithName:@"Futura-CondensedMedium" size:20.0]];
    [self.seasonLabelTitle setFont:[UIFont fontWithName:@"Futura-CondensedMedium" size:20.0]];
    [self.brandLabelTitle setFont:[UIFont fontWithName:@"Futura-CondensedMedium" size:20.0]];
    [self.modelLabelTitle setFont:[UIFont fontWithName:@"Futura-CondensedMedium" size:20.0]];
    [self.yearLabelTitle setFont:[UIFont fontWithName:@"Futura-CondensedMedium" size:20.0]];
    
    [self.descriptionLabel setFont:[UIFont fontWithName:@"Futura-CondensedMedium" size:20.0]];
    [self.categoryLabel setFont:[UIFont fontWithName:@"Futura-CondensedMedium" size:20.0]];
    [self.seasonLabel setFont:[UIFont fontWithName:@"Futura-CondensedMedium" size:20.0]];
    [self.brandLabel setFont:[UIFont fontWithName:@"Futura-CondensedMedium" size:20.0]];
    [self.modelLabel setFont:[UIFont fontWithName:@"Futura-CondensedMedium" size:20.0]];
    [self.yearLabel setFont:[UIFont fontWithName:@"Futura-CondensedMedium" size:20.0]];
     */
    
    
    self.stitchTypeLabel.text = @"";
    self.bigTitleLabel.text = @"";
    self.descriptionLabel.text = @"";
    self.categoryLabel.text = @"";
    self.yearLabel.text = @"";
    self.seasonLabel.text = @"";
    self.brandLabel.text = @"";
    self.modelLabel.text = @"";
    
    [self.sketchView setImage:[UIImage imageNamed:@"logo_big.png"]];
    [self.productView setImage:[UIImage imageNamed:@"logo_big.png"]];
}


- (void)setupView{
    
    [self cleanView];
    
    if(!self.choosenProduct)
        return;
    
    
    self.stitchTypeLabel.text = [[NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"Stitch type", @""),self.choosenProduct.stitch_type.item_description] uppercaseString];
    
        
    if([BTDataSync shared].signedIn && self.brandName != nil)
        self.bigTitleLabel.text = [NSString stringWithFormat:@"%@ - %@",self.brandName,self.choosenProduct.model];
    else
        self.bigTitleLabel.text = [NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"models.stitch_type_products.code", @"product code"), self.choosenProduct.model];
        
        
    self.descriptionLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.descriptionLabel.text = [self.choosenProduct.item_description uppercaseString];
    
    //self.descriptionLabel.text = @"dhasi hsd ahisd ahsid ahsidsahdi ahdias hdai dhasidhaisdh ashdi ahidisa dhasi dahidahsid ahi";
    
    CGSize max = CGSizeMake(245.0, 150.0);
    CGSize s = [NMHelpers effectiveSizeForText:self.descriptionLabel.text
                                      withFont:[UIFont fontWithName:@"Futura-CondensedMedium" size:20.0]
                              contrainedToSize:max];
    
    double delayInSeconds = 0.3;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        [UIView animateWithDuration:0.3 animations:^{
            
            
            self.descriptionLabel.numberOfLines = floorf(s.height / 20.0);
            self.descriptionLabel.frame = CGRectMake(self.descriptionLabel.frame.origin.x,
                                                     self.descriptionLabel.frame.origin.y,
                                                     self.descriptionLabel.frame.size.width,
                                                     s.height);
            NSLog(@"Number of lines: %d - height: %f",self.descriptionLabel.numberOfLines, self.descriptionLabel.frame.size.height);
        }];
        
    });
    
    
    
    
    
    
    
    self.categoryLabel.text = [self.choosenProduct.category uppercaseString];
    self.yearLabel.text = [self.choosenProduct.year.description uppercaseString];
    self.seasonLabel.text = [self.choosenProduct.season uppercaseString];
    self.brandLabel.text = [self.brandName uppercaseString];
    self.modelLabel.text = [self.choosenProduct.model uppercaseString];
    
    if(self.choosenProduct.sketchFilePath){
        [self.sketchView setImageWithURL:self.choosenProduct.sketchFileURL placeholderImage:[UIImage imageNamed:@"logo_big.png"]];
    }
    else
        [self.sketchView setImage:[UIImage imageNamed:@"logo_big.png"]];
    
    
    if(self.choosenProduct.imageFilePath){
        [self.productView setImageWithURL:self.choosenProduct.imageFileURL placeholderImage:[UIImage imageNamed:@"logo_big.png"]];
    }
    else
        [self.productView setImage:[UIImage imageNamed:@"logo_big.png"]];
    
    
}

- (IBAction)showPicture:(id)sender{
    
    if(!self.choosenProduct || !self.choosenProduct.imageFilePath)
        return;
    
    [self performSegueWithIdentifier:@"showZoom" sender:self];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier isEqualToString:@"showZoom"]){
        MSRZoomViewController *z = [segue destinationViewController];
        z.currentImage = self.productView.image;
        z.titleText = [NSLocalizedString(@"Product image",@"") uppercaseString];
    }
    
}

- (void)checkBrandsVisibility{
    //NSLog(@"Checking brand visibility based on user authentication: %hhd",[BTDataSync shared].signedIn);
    if(![BTDataSync shared].signedIn){
        [UIView animateWithDuration:0.3 animations:^{
            CGRect f = self.model_wrapper.frame;
            [self.model_wrapper setFrame:CGRectMake(f.origin.x, 126.0, f.size.width, f.size.height)];
            [self.brandView setHidden:YES];
        }];
    }
    else{
        [UIView animateWithDuration:0.3 animations:^{
            
            CGRect f = self.model_wrapper.frame;
            [self.model_wrapper setFrame:CGRectMake(f.origin.x, 155.0, f.size.width, f.size.height)];
            [self.brandView setHidden:NO];
        }];
    }
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self checkBrandsVisibility];
}
 
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self setupView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
