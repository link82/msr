//
//  MSRImportViewController.h
//  MSR
//
//  Created by Davide Cenzi on 11/12/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import "BTNoficationViewController.h"
#import "MDRadialProgressView.h"
#import "MDRadialProgressTheme.h"
#import "MDRadialProgressLabel.h"
#import "BTOverlayImageView.h"
#import "BTAlert.h"
@interface MSRImportViewController : BTNoficationViewController{

}

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet BTOverlayImageView *imageView;

- (IBAction)downloadImages:(id)sender;

- (IBAction)increase:(id)sender;

@end
