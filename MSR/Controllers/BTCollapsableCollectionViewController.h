//
//  BTCollapsableCollectionViewController.h
//  MSR
//
//  Created by Davide Cenzi on 04/06/14.
//  Copyright (c) 2014 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import "BTCollectionViewController.h"

@interface BTCollapsableCollectionViewController : BTCollectionViewController

@end
