//
//  MSRItemsViewController.m
//  MSR
//
//  Created by Davide Cenzi on 13/12/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import "MSRItemsViewController.h"
#import "MEDynamicTransition.h"
#import "UIViewController+ECSlidingViewController.h"
#import "MSRTreeViewController.h"
#import "MSRStitchCell.h"
#import "MSRStitchViewController.h"
#import "MSRDownloadViewController.h"
#import "MSRLockScreenViewController.h"

#import <QuartzCore/QuartzCore.h>
#import "BTAlert.h"

#define kMaxIdleTimeSeconds 60.0

@interface MSRItemsViewController (){
    Stitch *selectedStitch;
    NSString *importType;
    
    BOOL first_load;
    NSTimer *idleTimer;
}

@property (nonatomic, strong) UIPanGestureRecognizer *dynamicTransitionPanGesture;
@property (strong, nonatomic) UIPopoverController *masterPopoverController;
- (void)configureView;

@end

@implementation MSRItemsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (NSPredicate*)generateFilters{
    
    NSPredicate *p = nil;
    NSMutableString *format = [[NSMutableString alloc] init];
    //default condition
    [format appendFormat:@"(code > 0) AND (language == '%@')",[[BTDataSync shared] language]];
    
    if(_filters && ([_filters count] > 0)){
        
        
        
        
        for (NSString *key in _filters.allKeys) {
            
            if(![[_filters objectForKey:key] isKindOfClass:[NSNumber class]] && [[_filters objectForKey:key] isEqualToString:@""])
                continue;
            
            if([key isEqualToString:@"query"]){
                
                [format appendString:@" AND ("];
                //[format appendFormat:@"(%@ CONTAINS[cd] '%@')",@"string_code", [_filters objectForKey:key]];
                [format appendFormat:@"(%@ = %@)",@"string_code", [_filters objectForKey:key]];
                [format appendFormat:@" OR (%@ CONTAINS[cd] '%@')",@"item_description", [_filters objectForKey:key]];
                [format appendFormat:@" OR (%@ CONTAINS[cd] '%@')",@"composition", [_filters objectForKey:key]];
                //[format appendFormat:@" OR (%@ CONTAINS[cd] '%@')",@"supplier_codes", [NSString stringWithFormat:@"#%@#",[_filters objectForKey:key]]];
                [format appendString:@")"];
                break; //exit for loop
            }

            if([key rangeOfString:@"codes"].location != NSNotFound)
                [format appendFormat:@" AND (%@ CONTAINS[cd] '%@')",key, [NSString stringWithFormat:@"#%@#",[_filters objectForKey:key]]];
            else if([[_filters objectForKey:key] isKindOfClass:[NSNumber class]])
                [format appendFormat:@" AND (%@ == %@)",key, [_filters objectForKey:key]];
            else
                [format appendFormat:@" AND (%@ == '%@')",key, [_filters objectForKey:key]];
        }

    }
    NSLog(@"Format: %@",format);
    p = [NSPredicate predicateWithFormat:format];

    return p;
}

#pragma mark - Overrides template methods

- (NSFetchRequest*)fetchRequest{
    
    NSFetchRequest * f = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:[self modelName] inManagedObjectContext:self.managedObjectContext];
    [f setEntity:entity];
    
    // Set the batch size to a suitable number.
    [f setFetchBatchSize:20];
    
    //USE FILTERS HERE
    if(_filters && [_filters count] > 0)
        [f setPredicate:[self generateFilters]];

    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"item_description" ascending:NO];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    [f setSortDescriptors:sortDescriptors];
    
    return f;
}

- (NSString*)remotePath{
    return @""; //not used
}

- (NSString*)modelName{
    return @"Stitch";
}

#pragma mark - collectionView delegate / datasource methods overrides


- (void)configureCell:(UICollectionViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    MSRStitchCell *c = (MSRStitchCell*)cell;
    
    Stitch *object = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    [c.contentView setBackgroundColor:[UIColor grayColor]];
    c.codeLabel.text = [[object valueForKey:@"string_code"] uppercaseString];
    
    Yarn *yarn = [object yarn];
    if(yarn){
        c.yarnLabel.text = [[yarn item_description] uppercaseString];
        c.supplierLabel.text = [yarn.supplier cleanedDescription];
    }
    else{
        c.yarnLabel.text = NSLocalizedString(@"NO YARN FOUND",@"");
        c.supplierLabel.text = NSLocalizedString(@"NO SUPPLIER FOUND",@"");
    }
    
    if(object.hasGenericPictureFile){
        //NSLog(@"Loading image: %@",object.genericPictureURL);
        [c.stitchImage setImageWithURL:object.genericPictureURL placeholderImage:[UIImage imageNamed:@"logo_big.png"]];
    }
    else
        [c.stitchImage setImage:[UIImage imageNamed:@"logo_big.png"]];
    
}


/*
- (UICollectionViewCell *)collectionView:(UIcollectionView *)collectionView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}
 

 - (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
 // TODO: Deselect item
 }
 
*/

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    selectedStitch = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    if(self.slidingViewController.topViewController.view.frame.origin.x != 0.0)
        [self menuButtonTapped:self];
    
    double delayInSeconds = 0.2;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self performSegueWithIdentifier:@"details" sender:self];
    });
    
    
}

#pragma mark -

#pragma mark - Managing the detail item


- (void)setFilters:(NSDictionary *)filtersDictionary{
    _filters = filtersDictionary;
    [self configureView];
    
    if(self.managedObjectContext)
        self.resultsLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%d STITCHES FOUND.",@"nr of results"), [[[self fetchedResultsController] fetchedObjects] count]];
    else
        self.resultsLabel.text = @"";
}

- (NSDictionary *)filtersDictionary{
    return _filters;
}

- (void)configureView
{
    // Update the user interface for the detail item.
  
    self.fetchedResultsController = nil;
    [self.collectionView reloadData];

}

#pragma mark -

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"details"]){
        MSRStitchViewController *tmp = [segue destinationViewController];
        tmp.stitch = selectedStitch;
    }
    else if([segue.identifier isEqualToString:@"downloadView"]){
        MSRDownloadViewController *tmp = [segue destinationViewController];
        tmp.importType = importType;
    }
}

#pragma mark - Template methods overrides

//Override these methods
- (NSArray*)notificationNames{
    return [[super notificationNames] arrayByAddingObjectsFromArray:@[@"SEEDING_COMPLETED", @"SLIDING", @"SLIDED",@"DECOMPRESS_IMAGES_SUCCESS",@"IMPORT_COMPLETED",@"SEEDING_COMPLETED",@"START_IMAGE_UPDATE", @"USER_LOGGED_IN",@"USER_LOGGED_OUT"]];
}

- (void)handleNotification:(NSNotification *)notification{
    
    if([notification.name isEqualToString:@"SEEDING_COMPLETED"] || [notification.name isEqualToString:@"IMPORT_COMPLETED"] || [notification.name isEqualToString:@"DECOMPRESS_IMAGES_SUCCESS"]){
        [self.collectionView reloadData];
    }
    else if([notification.name isEqualToString:@"SLIDING"]){
        [self lockMenu];
    }
    else if([notification.name isEqualToString:@"SLIDED"]){
        [self unlockMenu];
    }
    else if([notification.name isEqualToString:@"START_IMAGE_UPDATE"]){
        [self launchUpdateImages];
    }
    else if([notification.name isEqualToString:@"USER_LOGGED_IN"]  || [notification.name isEqualToString:@"USER_LOGGED_OUT"]){
        NSLog(@"Need to update views");
    }
}

// ---
#pragma mark - t


- (void)lockMenu{
    [self.menuButton setEnabled:NO];
}

- (void)unlockMenu{
    [self.menuButton setEnabled:YES];
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    //self.slidingViewController.anchorLeftPeekAmount = 20.0;
    //self.slidingViewController.anchorLeftRevealAmount = 20.0;
    //self.slidingViewController.anchorRightPeekAmount = 20.0;
    self.slidingViewController.anchorRightRevealAmount = 320.0;
    
    if ([(NSObject *)self.slidingViewController.delegate isKindOfClass:[MEDynamicTransition class]]) {
        MEDynamicTransition *dynamicTransition = (MEDynamicTransition *)self.slidingViewController.delegate;
        if (!self.dynamicTransitionPanGesture) {
            self.dynamicTransitionPanGesture = [[UIPanGestureRecognizer alloc] initWithTarget:dynamicTransition action:@selector(handlePanGesture:)];
        }
        
        //[self.navigationController.view removeGestureRecognizer:self.slidingViewController.panGesture];
        //[self.navigationController.view addGestureRecognizer:self.dynamicTransitionPanGesture];
    } else {
        //[self.navigationController.view removeGestureRecognizer:self.dynamicTransitionPanGesture];
        //[self.navigationController.view addGestureRecognizer:self.slidingViewController.panGesture];
    }
    
    [self.collectionView reloadData];
    
    if(first_load)
        [self showLockScreen];
    
    [self resetIdleTimer];

}

- (void)showLockScreen{

    if([self.navigationController.presentedViewController.class isSubclassOfClass:[MSRLockScreenViewController class]]){
        return;
    }
    else if (![self.navigationController.presentedViewController.class isSubclassOfClass:[MSRItemsViewController class]]){
        //pop current view
        [self dismissViewControllerAnimated:NO completion:nil];
    }
    
    
    MSRLockScreenViewController *lockScreen = [self.storyboard instantiateViewControllerWithIdentifier:@"lockScreen"];
    [self.navigationController presentViewController:lockScreen animated:!first_load completion:^{
        
    }];
    
    first_load = NO;
    
}

#pragma mark Handling idle timeout

- (void)resetIdleTimer {
//    if (!idleTimer) {
//        idleTimer = [NSTimer scheduledTimerWithTimeInterval:kMaxIdleTimeSeconds
//                                                      target:self
//                                                    selector:@selector(idleTimerExceeded)
//                                                    userInfo:nil
//                                                     repeats:NO];
//    }
//    else {
//        if (fabs([idleTimer.fireDate timeIntervalSinceNow]) < kMaxIdleTimeSeconds-1.0) {
//            [idleTimer setFireDate:[NSDate dateWithTimeIntervalSinceNow:kMaxIdleTimeSeconds]];
//        }
//    }
}

- (void)idleTimerExceeded {

    idleTimer = nil;
    [self showLockScreen];
    [self resetIdleTimer];
}

- (UIResponder *)nextResponder {
    [self resetIdleTimer];
    return [super nextResponder];
}

#pragma mark -

#pragma mark - IBActions

- (IBAction)menuButtonTapped:(id)sender {
    
    [self.menuButton setEnabled:NO];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SLIDING" object:nil];
    
    if(self.slidingViewController.topViewController.view.frame.origin.x == 0.0){
        
        [self.slidingViewController anchorTopViewToRightAnimated:YES onComplete:^{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"SLIDED" object:nil];
        }];
    }
    else{
     
        [self.slidingViewController resetTopViewAnimated:YES onComplete:^{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"SLIDED" object:nil];
        }];
        
    }
    
}

- (IBAction)imoportButtonTapper:(id)sender{
    idleTimer = nil;
    [self performSegueWithIdentifier:@"import" sender:self];
}

- (void)launchUpdateImages{
    importType = @"UPDATE";
    [self performSegueWithIdentifier:@"downloadView" sender:self];
}

- (void)checkAuth{
    if(!self.dataSync.loggedIn){
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Login"
                                                            message:nil
                                                           delegate:self
                                                  cancelButtonTitle:@"Guest user"
                                                  otherButtonTitles:@"Sign in", nil];
        
        alertView.alertViewStyle = UIAlertViewStyleLoginAndPasswordInput;
        [alertView show];
    }

}

#pragma mark - alertview delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    //Index1 = Sign in
    //Index0 = Guest

    if(buttonIndex==1)
    {

        NSString *userName = [alertView textFieldAtIndex:0].text;
        NSString *password = [alertView textFieldAtIndex:1].text;
        
        if([self.dataSync signInWithUsername:userName andPassword:password])
            [[NSNotificationCenter defaultCenter] postNotificationName:@"USER_LOGGED_IN" object:nil];
        else{
            [SVProgressHUD showErrorWithStatus:@"Invalid credentials"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"USER_LOGGED_OUT" object:nil];
        }
    }
    else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"USER_LOGGED_OUT" object:nil];
    }
}

#pragma mark -

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    first_load = YES;
    [self resetIdleTimer];
    
	// Do any additional setup after loading the view.
    self.collectionView.backgroundColor = [UIColor clearColor];
    importType = @"IMPORT";
    
    self.title = @"MILES STITCH RESEARCH";
    self.navigationItem.leftBarButtonItem = [NMHelpers iOS7ButtonWithImage:[UIImage imageNamed:@"logo_little.png"] ForTarget:self andAction:@selector(menuButtonTapped:)];
    
    
    //temporary disabled
    //if([self.dataSync.environment isEqualToString:@"production"])
    //    self.navigationItem.rightBarButtonItem = nil;
    
    /*
    if(![self.dataSync existImageBundle] && ![self.dataSync existImagesInDocuments]){
        
        NSNumber *imported_assets = [self.dataSync settingsValueForKey:@"imported_assets"];
        if(!imported_assets){
            BTAlert *al = [[BTAlert alloc] initWithTitle:@"Import images" text:@"Catalog images are not pre-imported inside the app, would you like to download them now?" style:UIAlertViewStyleDefault onConfirm:^(UIAlertView *alert) {
                
                [self performSegueWithIdentifier:@"downloadView" sender:self];
                
            } onDismiss:nil];
            
            [al show];
        }
    }
     */
    
    //[self checkAuth];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
