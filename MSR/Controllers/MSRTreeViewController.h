//
//  MSRTreeViewController.h
//  MSR
//
//  Created by Davide Cenzi on 11/12/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RATreeView.h"
#import "MSRItemsViewController.h"
#import "ActionSheetPicker.h"
#import "BTSwitchButton.h"
#import "MSRLanguageViewController.h"

@interface MSRTreeViewController : BTNoficationViewController <RATreeViewDataSource,RATreeViewDelegate, UIScrollViewDelegate>

@property (strong, nonatomic) MSRItemsViewController *detailViewController;
@property (nonatomic, weak) RATreeView *treeView;

//@property (weak, nonatomic) IBOutlet UITextField *searchField;
@property (weak, nonatomic) IBOutlet UITextField *codeField;
@property (weak, nonatomic) IBOutlet UITextField *yearField;
@property (weak, nonatomic) IBOutlet UIButton *compositionButton;
@property (weak, nonatomic) IBOutlet UIButton *yarnButton;
@property (weak, nonatomic) IBOutlet UIButton *seasonButton;
@property (weak, nonatomic) IBOutlet UIButton *supplierButton;
@property (weak, nonatomic) IBOutlet UIButton *completedProductButton;
@property (weak, nonatomic) IBOutlet UIButton *brandButton;
@property (weak, nonatomic) IBOutlet UIButton *machineButton;
@property (weak, nonatomic) IBOutlet UITextField *thinnessField;
@property (weak, nonatomic) IBOutlet BTSwitchButton *stretchButton;
@property (weak, nonatomic) IBOutlet BTSwitchButton *vaniseButton;
@property (weak, nonatomic) IBOutlet UIView *slidingView;

@property (weak, nonatomic) IBOutlet UIView *brandView;

//@property (weak, nonatomic) IBOutlet UIButton *searchButton;
@property (weak, nonatomic) IBOutlet UIButton *chooseLanguageButton;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIImageView *flagView;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIPopoverController *popover;

//ibactions

- (IBAction)pickSeason:(id)sender;
- (IBAction)pickSupplier:(id)sender;
- (IBAction)pickYarn:(id)sender;
- (IBAction)pickProduct:(id)sender;
- (IBAction)pickComposition:(id)sender;
- (IBAction)pickBrand:(id)sender;
- (IBAction)pickMachine:(id)sender;
- (IBAction)pressedOnButton:(id)sender;
//- (IBAction)toggleSearch:(id)sender;

- (IBAction)checkAuth:(id)sender;

@end
