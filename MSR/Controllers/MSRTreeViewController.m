//
//  MSRTreeViewController.m
//  MSR
//
//  Created by Davide Cenzi on 11/12/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import "MSRTreeViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "MSRTreeCell.h"
#import "NMFieldDelegate.h"
#import "S2SOrderedDictionary.h"



#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface MSRTreeViewController (){
    NSMutableDictionary *filtersFields;
    NSString *season_code, *yarn_codes, *product_codes, *brand_code, *machine_code, *composition_code, *supplier_codes;
    NSNumber *vanise, *stretch;
    
    NMFieldDelegate *fieldsDelegate;
    S2SOrderedDictionary *machine_filters, *macro_season_filters, *product_filters, *brand_filters, *yarn_filters, *composition_filters, *supplier_filters;
    
    BOOL checked_user;
  
}
@property (strong, nonatomic) id expanded;
@property (strong, nonatomic) NSArray *data;
@property (nonatomic, strong) UINavigationController *transitionsNavigationController;

- (void)prepareFilters;
- (void)clearFilters;
- (void)applyFilters;
- (void)loadFiltersData;
- (void)setTitle:(NSString *)title forButton:(UIButton*)button;
- (void)updateFlag;

@end

@implementation MSRTreeViewController

- (void)awakeFromNib
{
    self.preferredContentSize = CGSizeMake(320.0, 600.0);
    [super awakeFromNib];
}

- (void)setupView{
    
    NSLog(@"Actual language: %@",[[BTDataSync shared] language]);
    self.data = [StitchType rootObjects];
    
    NSLog(@"Current width: %f",self.view.frame.size.width);
    
    CGRect frame = CGRectMake(self.view.frame.origin.x, 474.0, self.view.frame.size.width, self.view.frame.size.width - 474.0);
    
    RATreeView *treeView = [[RATreeView alloc] initWithFrame:frame];
    
    treeView.delegate = self;
    treeView.dataSource = self;
    treeView.separatorStyle = RATreeViewCellSeparatorStyleNone;

    [treeView reloadData];
    [treeView setBackgroundColor:[UIColor colorWithRed:30.0 / 255.0 green:30.0 / 255.0 blue:30.0 / 255.0 alpha:1.0]];
    treeView.autoresizingMask = UIViewAutoresizingNone;
    
    self.treeView = treeView;
    [self.view addSubview:treeView];
    
    [self clearFilters];
    
    //setup textfields delegate
    fieldsDelegate = [[NMFieldDelegate alloc] initWithFields:@[self.codeField,self.yearField, self.thinnessField] fieldChangeBlock:^(id textField, id nextField) {
        
    } shouldReturnBlock:^BOOL(id textField) {
        return NO;
    } shouldEndBlock:^BOOL(id textField) {
        
        UITextField *tmp = textField;
        NSString *alert = nil;
        
        if(![tmp.text isEqualToString:@""] && [tmp.text rangeOfString:@" "].location != NSNotFound){
            alert = NSLocalizedString(@"Space characters are not allowed",@"error text text field");
        }
        
        //year field, not empty, not numeric value
        if(([tmp isEqual:self.yearField] || [tmp isEqual:self.thinnessField] )&& ![NMHelpers numericString:tmp.text]){
            alert = NSLocalizedString(@"Please insert a valid year",@"error text text field");
        }
        
        if(alert){
            
            double delayInSeconds = 0.5;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                
                UIAlertView *al = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Invalid value",@"error title text field") message:alert delegate:Nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [al show];
                
            });
            
            
            return NO;
        }
        else{
            [self applyFilters];
            return YES;
        }
        
    } withAutoScrollOnView:NO];
    
    self.codeField.keyboardType = UIKeyboardTypePhonePad;
    self.thinnessField.keyboardType = UIKeyboardTypePhonePad;
    
    
    //setup BTSwitchButtons blocks
    BTSwitchButtonOnChange vaniseOnChange = ^(BTSwitchButton *button, BOOL checked){

        if(checked){
            [button setImage:[UIImage imageNamed:@"radio_button_select.png"] forState:UIControlStateNormal];
            vanise = [NSNumber numberWithBool:checked];
        }
        else{
            [button setImage:nil forState:UIControlStateNormal];
            vanise = nil;
        }
        
        [self applyFilters];
    };
    
    [self.vaniseButton setOnChangeBlock:vaniseOnChange];
    
    BTSwitchButtonOnChange stretchOnChange = ^(BTSwitchButton *button, BOOL checked){
        
        if(checked){
            [button setImage:[UIImage imageNamed:@"radio_button_select.png"] forState:UIControlStateNormal];
            stretch = [NSNumber numberWithBool:checked];
        }
        else{
            [button setImage:nil forState:UIControlStateNormal];
            stretch = nil;
        }
        
        [self applyFilters];
    };
    
    [self.stretchButton setOnChangeBlock:stretchOnChange];
    
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, 768.0); //+1 la fa scrollare
	self.scrollView.maximumZoomScale = 1.0; //puo' variare se voglio renderla zoomabile
	self.scrollView.minimumZoomScale = 1.0;
	self.scrollView.clipsToBounds = YES;
	self.scrollView.delegate = self;
    
    //[self.searchButton setBackgroundImage:[UIImage imageGlyphNamed:@"search"] forState:UIControlStateNormal];
    
    [self updateFlag];
}

- (void)clearFilters{
    //setup empty filter fields
    filtersFields = [[NSMutableDictionary alloc] init];
    [filtersFields setObject:@"" forKey:@"query"];
    [filtersFields setObject:@"" forKey:@"string_code"];
    [filtersFields setObject:@"" forKey:@"year"];
    [filtersFields setObject:@"" forKey:@"composition"];
    [filtersFields setObject:@"" forKey:@"season_code"];
    
    [filtersFields setObject:@"" forKey:@"stitch_type_codes"];
    [filtersFields setObject:@"" forKey:@"supplier_codes"];
    [filtersFields setObject:@"" forKey:@"yarn_codes"];
    [filtersFields setObject:@"" forKey:@"product_codes"];
    
    [filtersFields setObject:@"" forKey:@"brand_code"];
    [filtersFields setObject:@"" forKey:@"machine_code"];
    [filtersFields setObject:@"" forKey:@"thinness"];
    [filtersFields setObject:@"" forKey:@"machine_code"];
    [filtersFields setObject:@"" forKey:@"stretch"];
    [filtersFields setObject:@"" forKey:@"vanise"];
}

- (void)loadFiltersData{
    
    machine_filters = [Machine filterValues];
    macro_season_filters = [MacroSeason filterValues];
    product_filters = [Product filterValues];
    yarn_filters = [Yarn filterValues];
    composition_filters = [Stitch compositionsFilterValues];
    brand_filters = [Brand filterValues];
    supplier_filters = [Supplier filterValues];
    
    [self checkBrandsVisibility];
}

- (void)checkBrandsVisibility{
    NSLog(@"Checking brand visibility based on user authentication: %hhd",self.dataSync.signedIn);
    //BOOL wasHidden = self.brandView.hidden;
    CGRect frame = self.slidingView.frame;
    
    if(!self.dataSync.signedIn){
        if(self.slidingView.frame.origin.y ==  299.0)
            return;
        
        [UIView animateWithDuration:0.3 animations:^{
            [self.slidingView setFrame:CGRectMake(frame.origin.x, 299.0, frame.size.width, frame.size.height)];
            [self.brandView setHidden:YES];
        }];
    }
    else{
        if(self.slidingView.frame.origin.y ==  338.0)
            return;
        [UIView animateWithDuration:0.3 animations:^{
            [self.slidingView setFrame:CGRectMake(frame.origin.x, 338.0, frame.size.width, frame.size.height)];
            [self.brandView setHidden:NO];
        }];


    }
    
    
}


#pragma mark - user auth
- (IBAction)checkAuth:(id)sender{
    if(!self.dataSync.loggedIn){
        checked_user = YES;
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Login",@"")
                                                            message:nil
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"Guest user",@"")
                                                  otherButtonTitles:NSLocalizedString(@"Sign in",@""), nil];
        
        alertView.alertViewStyle = UIAlertViewStyleLoginAndPasswordInput;
        [alertView show];
    }
    else{
        [self.dataSync signOut];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"USER_LOGGED_OUT" object:nil];
        [self.loginButton setTitle:[NSLocalizedString(@"Login",@"") uppercaseString] forState:UIControlStateNormal];
        [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Successfully logged out", @"logout message")];
    }
    
}

#pragma mark - alertview delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    //Index1 = Sign in
    //Index0 = Guest
    
    
    NSLog(@"Alert View dismissed with button at index %d",buttonIndex);
    
    if(buttonIndex==1)
    {
        
        NSString *userName = [alertView textFieldAtIndex:0].text;
        NSString *password = [alertView textFieldAtIndex:1].text;
        
        if([self.dataSync signInWithUsername:userName andPassword:password]){
            [self.loginButton setTitle:[NSLocalizedString(@"Logout",@"") uppercaseString] forState:UIControlStateNormal];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"USER_LOGGED_IN" object:nil];
            [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Successfully logged in", @"login message")];
        }
        else{
            [self.loginButton setTitle:[NSLocalizedString(@"Login",@"") uppercaseString] forState:UIControlStateNormal];
            [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Invalid credentials",@"")];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"USER_LOGGED_OUT" object:nil];
        }
    }
    else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"USER_LOGGED_OUT" object:nil];
    }
}

#pragma mark -

#pragma mark - Pickers

/*
* NSString *season_code, *yarn_codes, *product_codes, *brand_code, machine_code;
* NSNumber *thinness, *stretch;
*/

- (void)setTitle:(NSString *)title forButton:(UIButton*)button{
    if([title rangeOfString:@" -- "].location == NSNotFound)
        [button setTitle:title forState:UIControlStateNormal];
    else
        [button setTitle:@"" forState:UIControlStateNormal];
}

#pragma mark - YHC picker delegate

-(void)selectedRow:(int)row withString:(NSString *)text{
    
    NSLog(@"%d",row);
   
}


#pragma mark -


- (IBAction)pickSeason:(id)sender{
   
    ActionStringDoneBlock done = ^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        [self setTitle:selectedValue forButton:self.seasonButton];
        season_code = [macro_season_filters objectForKey:selectedValue];
        [self applyFilters];
    };
    ActionStringCancelBlock cancel = ^(ActionSheetStringPicker *picker) {
        NSLog(@"Block Picker Canceled");
    };
    
    NSString *key = [macro_season_filters keyOfObject:season_code];
    NSUInteger selected_index = (key == nil || [macro_season_filters indexForKey:key] == -1 ? 0 : [macro_season_filters indexForKey:key]);

    ActionSheetStringPicker *picker = [[ActionSheetStringPicker alloc] initWithTitle:NSLocalizedString(@"Select a season",@"") rows:macro_season_filters.allKeys initialSelection:selected_index doneBlock:done cancelBlock:cancel origin:self.view];
    
    [picker setHideCancel:YES];
    [picker addCustomButtonWithTitle:NSLocalizedString(@"Reset",@"reset filter") value:@""];
    
    [picker showActionSheetPicker];
 
    
}

- (IBAction)pickSupplier:(id)sender{
    
    ActionStringDoneBlock done = ^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        [self setTitle:selectedValue forButton:self.supplierButton];
        supplier_codes = [supplier_filters objectForKey:selectedValue];
        [self applyFilters];
    };
    ActionStringCancelBlock cancel = ^(ActionSheetStringPicker *picker) {
        NSLog(@"Block Picker Canceled");
    };
    
    NSString *key = [supplier_filters keyOfObject:supplier_codes];
    NSUInteger selected_index = (key == nil || [supplier_filters indexForKey:key] == -1 ? 0 : [supplier_filters indexForKey:key]);
    
    ActionSheetStringPicker *picker = [[ActionSheetStringPicker alloc] initWithTitle:NSLocalizedString(@"Select a supplier",@"") rows:supplier_filters.allKeys initialSelection:selected_index doneBlock:done cancelBlock:cancel origin:self.view];
    
    [picker setUseSearchField:YES];
    [picker setHideCancel:YES];
    [picker addCustomButtonWithTitle:NSLocalizedString(@"Reset",@"reset filter") value:@""];
    
    [picker showActionSheetPicker];
}

- (IBAction)pickYarn:(id)sender{
    
    ActionStringDoneBlock done = ^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        [self setTitle:selectedValue forButton:self.yarnButton];
        yarn_codes = [yarn_filters objectForKey:selectedValue];
        [self applyFilters];
    };
    ActionStringCancelBlock cancel = ^(ActionSheetStringPicker *picker) {
        NSLog(@"Block Picker Canceled");
    };
    
    NSString *key = [yarn_filters keyOfObject:yarn_codes];
    NSUInteger selected_index = (key == nil || [yarn_filters indexForKey:key] == -1 ? 0 : [yarn_filters indexForKey:key]);
    
    ActionSheetStringPicker *picker = [[ActionSheetStringPicker alloc] initWithTitle:NSLocalizedString(@"Select a yarn",@"") rows:yarn_filters.allKeys initialSelection:selected_index doneBlock:done cancelBlock:cancel origin:self.view];
    
    [picker setUseSearchField:YES];
    [picker setHideCancel:YES];
    [picker addCustomButtonWithTitle:NSLocalizedString(@"Reset",@"reset filter") value:@""];
    
    [picker showActionSheetPicker];
}

- (IBAction)pickProduct:(id)sender{
 
    ActionStringDoneBlock done = ^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        [self setTitle:selectedValue forButton:self.completedProductButton];
        product_codes = [product_filters objectForKey:selectedValue];
        [self applyFilters];
    };
    ActionStringCancelBlock cancel = ^(ActionSheetStringPicker *picker) {
        NSLog(@"Block Picker Canceled");
    };
    
    NSString *key = [product_filters keyOfObject:product_codes];
    NSUInteger selected_index = (key == nil || [product_filters indexForKey:key] == -1 ? 0 : [product_filters indexForKey:key]);
    
    ActionSheetStringPicker *picker = [[ActionSheetStringPicker alloc] initWithTitle:NSLocalizedString(@"Select a final product",@"") rows:product_filters.allKeys initialSelection:selected_index doneBlock:done cancelBlock:cancel origin:self.view];
    
    [picker setUseSearchField:YES];
    [picker setHideCancel:YES];
    [picker addCustomButtonWithTitle:NSLocalizedString(@"Reset",@"reset filter") value:@""];
    
    [picker showActionSheetPicker];
}

- (IBAction)pickComposition:(id)sender{
    
    ActionStringDoneBlock done = ^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        [self setTitle:selectedValue forButton:self.compositionButton];
        composition_code = [composition_filters objectForKey:selectedValue];
        [self applyFilters];
    };
    ActionStringCancelBlock cancel = ^(ActionSheetStringPicker *picker) {
        NSLog(@"Block Picker Canceled");
    };
    
    NSString *key = [composition_filters keyOfObject:composition_code];
    NSUInteger selected_index = (key == nil || [composition_filters indexForKey:key] == -1 ? 0 : [composition_filters indexForKey:key]);
    
    ActionSheetStringPicker *picker = [[ActionSheetStringPicker alloc] initWithTitle:NSLocalizedString(@"Select a composition",@"") rows:composition_filters.allKeys initialSelection:selected_index doneBlock:done cancelBlock:cancel origin:self.view];
    
    [picker setUseSearchField:YES];
    [picker setHideCancel:YES];
    [picker addCustomButtonWithTitle:NSLocalizedString(@"Reset",@"reset filter") value:@""];
    
    [picker showActionSheetPicker];
}

- (IBAction)pickBrand:(id)sender{
    
    ActionStringDoneBlock done = ^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        NSLog(@"Selected value %@",selectedValue);
        [self setTitle:selectedValue forButton:self.brandButton];
        brand_code = [brand_filters objectForKey:selectedValue];
        NSLog(@"Selected value %@ - %@",selectedValue,brand_code);
        [self applyFilters];
    };
    ActionStringCancelBlock cancel = ^(ActionSheetStringPicker *picker) {
        NSLog(@"Block Picker Canceled");
    };
    
    NSString *key = [brand_filters keyOfObject:brand_code];
    NSUInteger selected_index = (key == nil || [brand_filters indexForKey:key] == -1 ? 0 : [brand_filters indexForKey:key]);
    
    ActionSheetStringPicker *picker = [[ActionSheetStringPicker alloc] initWithTitle:NSLocalizedString(@"Select a brand",@"") rows:brand_filters.allKeys initialSelection:selected_index doneBlock:done cancelBlock:cancel origin:self.view];
    
    [picker setUseSearchField:YES];
    [picker setHideCancel:YES];
    [picker addCustomButtonWithTitle:NSLocalizedString(@"Reset",@"reset filter") value:@""];
    
    [picker showActionSheetPicker];
    
}

- (IBAction)pickMachine:(id)sender{
    
    ActionStringDoneBlock done = ^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        [self setTitle:selectedValue forButton:self.machineButton];
        machine_code = [machine_filters objectForKey:selectedValue];
        [self applyFilters];
    };
    ActionStringCancelBlock cancel = ^(ActionSheetStringPicker *picker) {
        NSLog(@"Block Picker Canceled");
    };
    
    NSString *key = [machine_filters keyOfObject:machine_code];
    NSUInteger selected_index = (key == nil || [machine_filters indexForKey:key] == -1 ? 0 : [machine_filters indexForKey:key]);
    
    ActionSheetStringPicker *picker = [[ActionSheetStringPicker alloc] initWithTitle:NSLocalizedString(@"Select a machine",@"") rows:machine_filters.allKeys initialSelection:selected_index doneBlock:done cancelBlock:cancel origin:self.view];
    
    [picker setUseSearchField:YES];
    [picker setHideCancel:YES];
    [picker addCustomButtonWithTitle:NSLocalizedString(@"Reset",@"reset filter") value:@""];
    
    [picker showActionSheetPicker];
    
}

- (IBAction)pressedOnButton:(id)sender{
    
    UIButton *btn = sender;
    NSUInteger tag = btn.tag;
    
    switch (tag) {
        case 0:
            [self.codeField becomeFirstResponder];
            break;
        case 1:
            [self.yearField becomeFirstResponder];
            break;
        case 2:
            [self pickSeason:self];
            break;
        case 3:
            [self pickComposition:self];
            break;
        case 4:
            [self pickYarn:self];
            break;
        case 5:
            [self pickProduct:self];
            break;
        case 6:
            [self pickSupplier:self];
            break;
        case 7:
            [self pickBrand:self];
            break;
        case 8:
            [self pickMachine:self];
            break;
        case 9:
            [self.thinnessField becomeFirstResponder];
            break;
        case 10:
            [self.stretchButton sendActionsForControlEvents: UIControlEventTouchUpInside];
            break;
        case 11:
            [self.vaniseButton sendActionsForControlEvents: UIControlEventTouchUpInside];
            break;
        default:
            break;
    }
}

- (void)updateFlag{
    
    NSString *image_name = @"united_kingdom_flag";
    NSString *lang = [[BTLanguage shared] currentLanguage];
                      
    if([lang isEqualToString:@"it"])
        image_name = @"italy_flag";
    else if([lang isEqualToString:@"fr"])
        image_name = @"france_flag";
                      
    [self.flagView setImage:[UIImage imageNamed:image_name]];
    [self.chooseLanguageButton setTitle:[[[BTLanguage shared] currentLanguage] uppercaseString] forState:UIControlStateNormal];
}

#pragma mark -

#pragma mark - Template methods overrides

//Override these methods
- (NSArray*)notificationNames{
    return [[super notificationNames] arrayByAddingObjectsFromArray:@[@"CHANGED_LANGUAGE", @"SLIDING", @"SLIDED",@"DECOMPRESS_IMAGES_SUCCESS",@"IMPORT_COMPLETED",@"SEEDING_COMPLETED",@"USER_LOGGED_IN",@"USER_LOGGED_OUT"]];
}

- (void)handleNotification:(NSNotification *)notification{
    
    if([notification.name isEqualToString:@"SEEDING_COMPLETED"] || [notification.name isEqualToString:@"IMPORT_COMPLETED"] || [notification.name isEqualToString:@"DECOMPRESS_IMAGES_SUCCESS"]){
        self.data = [StitchType rootObjects];
        [self loadFiltersData];
        [self.treeView reloadData];
    }
    else if([notification.name isEqualToString:@"CHANGED_LANGUAGE"]){
        [self updateFlag];
    }
    else if([notification.name isEqualToString:@"USER_LOGGED_IN"]  || [notification.name isEqualToString:@"USER_LOGGED_OUT"]){
        [self loadFiltersData];
    }
    
}

// ---
#pragma mark -



- (void)prepareFilters{
    
    [self clearFilters];
    
    //free search ignoring other parameters
    /*
    if(self.searchField.text && ![self.searchField.text isEqualToString:@""]){
        [filtersFields setObject:self.searchField.text forKey:@"query"];
        return;
    }
     */
    
    if(self.codeField.text && ![self.codeField.text isEqualToString:@""])
        [filtersFields setObject:self.codeField.text forKey:@"string_code"];
    
    if(self.yearField.text && ![self.yearField.text isEqualToString:@""])
        [filtersFields setObject:self.yearField.text forKey:@"year"];
    
    if(self.thinnessField.text && ![self.thinnessField.text isEqualToString:@""])
        [filtersFields setObject:[NSNumber numberWithInt:[self.thinnessField.text intValue]] forKey:@"thinness"];
    
    if(self.compositionButton.titleLabel.text && ![self.compositionButton.titleLabel.text isEqualToString:@""])
        [filtersFields setObject:self.compositionButton.titleLabel.text forKey:@"composition"];
 
    
    if(self.expanded){ //[(StitchType*)self.expanded leafValue]
        StitchType *tmp = self.expanded;
        if(tmp.childrens.count == 0)
            [filtersFields setObject:tmp.code forKey:@"stitch_type_codes"];
        else
            [filtersFields setObject:@"" forKey:@"stitch_type_codes"];
    }
    else
        [filtersFields setObject:@"" forKey:@"stitch_type_codes"];
    
    /* Filters from pickers
    * season_code, yarn_codes, product_codes, brand_code, thinness, stretch
    *
    * NSString *season_code, *yarn_codes, *product_codes, *brand_code, machine_code;
    * NSNumber *vanise, *stretch;
    */
    
    //need to be moved in picker callback
    if(season_code)
        [filtersFields setObject:season_code forKey:@"season_code"];
    
    if(supplier_codes)
        [filtersFields setObject:supplier_codes forKey:@"supplier_codes"];
    
    if(yarn_codes)
        [filtersFields setObject:yarn_codes forKey:@"yarn_codes"];
    
    if(product_codes)
        [filtersFields setObject:product_codes forKey:@"product_codes"];
    
    if(brand_code)
        [filtersFields setObject:brand_code forKey:@"brand_code"];
    
    if(machine_code)
        [filtersFields setObject:machine_code forKey:@"machine_code"];
    
    if(composition_code)
        [filtersFields setObject:composition_code forKey:@"composition"];
    
    if(stretch)
        [filtersFields setObject:stretch forKey:@"stretch"];
    
    if(vanise)
        [filtersFields setObject:vanise forKey:@"vanise"];
    
    
}

- (void)applyFilters{
    [self prepareFilters];
    [self.detailViewController setFilters:filtersFields];
}

#pragma mark TreeView Delegate methods
- (CGFloat)treeView:(RATreeView *)treeView heightForRowForItem:(id)item treeNodeInfo:(RATreeNodeInfo *)treeNodeInfo
{
    return 39;
}

- (NSInteger)treeView:(RATreeView *)treeView indentationLevelForRowForItem:(id)item treeNodeInfo:(RATreeNodeInfo *)treeNodeInfo
{
    return 3 * treeNodeInfo.treeDepthLevel;
}

- (BOOL)treeView:(RATreeView *)treeView shouldExpandItem:(id)item treeNodeInfo:(RATreeNodeInfo *)treeNodeInfo
{
    return YES;
}

- (BOOL)treeView:(RATreeView *)treeView shouldItemBeExpandedAfterDataReload:(id)item treeDepthLevel:(NSInteger)treeDepthLevel
{
    if ([item isEqual:self.expanded]) {
        return YES;
    }
    
    return NO;
}

- (void)treeView:(RATreeView *)treeView didCollapseRowForItem:(id)item treeNodeInfo:(RATreeNodeInfo *)treeNodeInfo{
    [treeView reloadRowsForItems:@[item] withRowAnimation:RATreeViewRowAnimationAutomatic];
}

- (void)treeView:(RATreeView *)treeView didExpandRowForItem:(id)item treeNodeInfo:(RATreeNodeInfo *)treeNodeInfo{
    
    [treeView reloadRowsForItems:@[item] withRowAnimation:RATreeViewRowAnimationAutomatic];
    [self resizeTableViewFrameHeight];
    
}

- (void)treeView:(RATreeView *)treeView didSelectRowForItem:(id)item treeNodeInfo:(RATreeNodeInfo *)treeNodeInfo{
    
    StitchType *old_item = self.expanded;
    
    if(self.expanded && [self.expanded isEqual:item]){
           self.expanded = nil;
    }
    else
        self.expanded = item;
    
    //remove previous X
    if(old_item)
        [treeView reloadRowsForItems:@[old_item] withRowAnimation:RATreeViewRowAnimationAutomatic];

    //notify details view
    [self applyFilters];
    
    //update current node
    [treeView reloadRowsForItems:@[item] withRowAnimation:RATreeViewRowAnimationAutomatic];
    
}

- (void)resizeTableViewFrameHeight
{
    // Table view does not scroll, so its frame height should be equal to its contentSize height

    CGRect frame = self.treeView.frame;
    self.treeView.frame = CGRectMake(0,474.0,frame.size.width,self.view.frame.size.height - 474.0);
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, 474.0 + self.treeView.frame.size.height +1.0);
}

#pragma mark TreeView Data Source

- (UITableViewCell *)treeView:(RATreeView *)treeView cellForItem:(id)item treeNodeInfo:(RATreeNodeInfo *)treeNodeInfo
{
    StitchType *__node = item;
    NSInteger numberOfChildren = [__node.childrens count];
    
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MSRTreeCell" owner:self options:nil];
    MSRTreeCell *cell = (MSRTreeCell *)[nib objectAtIndex:0];
    cell.titleLabel.text = [[__node item_description] uppercaseString];
    cell.titleLabel.frame = CGRectMake(cell.titleLabel.frame.origin.x + (treeNodeInfo.treeDepthLevel * 25), cell.titleLabel.frame.origin.y, cell.titleLabel.frame.size.width, cell.titleLabel.frame.size.height);

    [cell.checkButton setUserInteractionEnabled:NO];
    
    if ([treeNodeInfo isExpanded]) {
        [cell.checkButton setImage:[UIImage imageNamed:@"menu_contract.png"] forState:UIControlStateNormal];
        
    }
    else{ //selected
        if(numberOfChildren > 0)
            [cell.checkButton setImage:[UIImage imageNamed:@"menu_expand.png"] forState:UIControlStateNormal];
        else if([item isEqual:self.expanded]) //selezione finale
            [cell.checkButton setImage:[UIImage imageNamed:@"treeview_selection.png"] forState:UIControlStateNormal];
        else
            [cell.checkButton setImage:Nil forState:UIControlStateNormal];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (NSInteger)treeView:(RATreeView *)treeView numberOfChildrenOfItem:(id)item
{
    StitchType *__node = item;
    if (__node == nil) {
        return [self.data count];
    }

    return [__node.childrens count];
}

- (id)treeView:(RATreeView *)treeView child:(NSInteger)index ofItem:(id)item
{
    StitchType *__node = item;
    if (__node == nil) {
        return [self.data objectAtIndex:index];
    }
    
    return [__node.childrens.allObjects objectAtIndex:index];
}

- (void)treeView:(RATreeView *)treeView accessoryButtonTappedForRowForItem:(id)item treeNodeInfo:(RATreeNodeInfo *)treeNodeInfo{
    NSLog(@"Accessory button tapped");
}

#pragma mark -

#pragma mark - ibactions

- (IBAction)toggleFilters:(id)sender{

    [[NSNotificationCenter defaultCenter] postNotificationName:@"SLIDING" object:nil];
    
    if(self.slidingViewController.topViewController.view.frame.origin.x == 0.0){
        
        [self.slidingViewController anchorTopViewToRightAnimated:YES onComplete:^{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"SLIDED" object:nil];
        }];
    }
    else{
        
        [self.slidingViewController resetTopViewAnimated:YES onComplete:^{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"SLIDED" object:nil];
        }];
        
    }

}

#pragma mark -

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"languagePopover"]){
        self.popover = [(UIStoryboardPopoverSegue *)segue popoverController];
        MSRLanguageViewController *lang = [segue destinationViewController];
        lang.popover = self.popover;
    }
}

- (void)upcaseLabels{
    for (UIView *v in self.view.subviews) {
        if([v isKindOfClass:[UILabel class]] && [((UILabel*)v) text])
            [((UILabel*)v) setText:[[((UILabel*)v) text] uppercaseString]];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self checkBrandsVisibility];
    
    if(!checked_user)
        [self checkAuth:self];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    checked_user = NO;
    
    self.transitionsNavigationController = (UINavigationController *)self.slidingViewController.topViewController;
    self.detailViewController = (MSRItemsViewController*)self.transitionsNavigationController.topViewController;
    
    /*
    if ([self.searchField respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        self.searchField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"FREE SEARCH HERE",@"search box") attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
    } else {
        NSLog(@"Cannot set placeholder text's color, because deployment target is earlier than iOS 6.0");
        // TODO: Add fall-back code to set placeholder color.
    }
    
    [self.searchField setNeedsDisplay];
    */
    [self setupView];
    [self loadFiltersData];
    
    [self upcaseLabels];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
