//
//  MSRProductDetailViewController.h
//  MSR
//
//  Created by Davide Cenzi on 19/01/14.
//  Copyright (c) 2014 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import "BTNoficationViewController.h"

@interface MSRProductDetailViewController : BTNoficationViewController

@property (weak, nonatomic) IBOutlet UIView *model_wrapper;
@property (strong, nonatomic) StitchTypeProduct *choosenProduct;
@property (strong, nonatomic) NSString *brandName;
@property (weak, nonatomic) IBOutlet UIView *brandView;

@property (weak, nonatomic) IBOutlet UILabel *titleSketchLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleImageLabel;

@property (weak, nonatomic) IBOutlet UILabel *stitchTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *bigTitleLabel;


@property (weak, nonatomic) IBOutlet UILabel *descriptionLabelTitle;
@property (weak, nonatomic) IBOutlet UILabel *categoryLabelTitle;
@property (weak, nonatomic) IBOutlet UILabel *yearLabelTitle;
@property (weak, nonatomic) IBOutlet UILabel *seasonLabelTitle;
@property (weak, nonatomic) IBOutlet UILabel *brandLabelTitle;
@property (weak, nonatomic) IBOutlet UILabel *modelLabelTitle;

@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *yearLabel;
@property (weak, nonatomic) IBOutlet UILabel *seasonLabel;
@property (weak, nonatomic) IBOutlet UILabel *brandLabel;
@property (weak, nonatomic) IBOutlet UILabel *modelLabel;

@property (weak, nonatomic) IBOutlet UIImageView *sketchView;
@property (weak, nonatomic) IBOutlet UIImageView *productView;


- (IBAction)showPicture:(id)sender;


@end
