//
//  MSRDownloadViewController.m
//  MSR
//
//  Created by Davide Cenzi on 12/01/14.
//  Copyright (c) 2014 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import "MSRDownloadViewController.h"
#import "BTAlert.h"

@interface MSRDownloadViewController ()

@end

@implementation MSRDownloadViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - Superclass overrides

- (NSArray*)notificationNames{
    return [[super notificationNames] arrayByAddingObjectsFromArray:@[@"IMPORT_COMPLETED",@"DOWNLOAD_IMAGES_STARTED",@"DOWNLOAD_IMAGES_PROGRESS", @"DOWNLOAD_IMAGES_SUCCESS", @"DOWNLOAD_IMAGES_FAILURE", @"DECOMPRESS_IMAGES_STARTED",@"DECOMPRESS_IMAGES_FAILURE",@"DECOMPRESS_IMAGES_PROGRESS", @"DECOMPRESS_IMAGES_SUCCESS"]];
}

- (void)handleNotification:(NSNotification *)notification{
 
    if([notification.name isEqualToString:@"DOWNLOAD_IMAGES_STARTED"]){
        self.titleLabel.text = [NSLocalizedString(@"Starting image download...",@"") uppercaseString];
    }
    else if([notification.name isEqualToString:@"DOWNLOAD_IMAGES_PROGRESS"]){
        self.titleLabel.text = [NSLocalizedString(@"Downloading image archive...",@"") uppercaseString];
        
        NSNumber *_progress = (NSNumber*)notification.object;
        [self.imageView setProgress:[_progress doubleValue]];
    }
    else if([notification.name rangeOfString:@"DOWNLOAD_IMAGES_SUCCESS"].location != NSNotFound){
        self.titleLabel.text = [NSLocalizedString(@"Image archive downloaded",@"") uppercaseString];
        //total_nr = 0;
        //progress = 0;
    }
    else if([notification.name isEqualToString:@"DECOMPRESS_IMAGES_STARTED"]){
        self.titleLabel.text = [NSLocalizedString(@"Decompressing image archive...",@"") uppercaseString];
    }
    else if([notification.name isEqualToString:@"DECOMPRESS_IMAGES_PROGRESS"]){
        self.titleLabel.text = [NSLocalizedString(@"Decompressing image archive...",@"") uppercaseString];
        
        NSNumber *_progress = (NSNumber*)notification.object;
        [self.imageView setProgress:[_progress doubleValue]];
    }
    else if([notification.name isEqualToString:@"DECOMPRESS_IMAGES_FAILURE"]){
        self.titleLabel.text =[NSLocalizedString(@"Unable to extract images from archive, please close other applications and try again.", @"Unzip failure message") uppercaseString];
        
        double delayInSeconds = 2.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self dismissViewControllerAnimated:YES completion:nil];
        });
        
    }
    else if([notification.name isEqualToString:@"DOWNLOAD_IMAGES_FAILURE"]){
        NSLog(@"Import failure");
        self.titleLabel.text = [NSLocalizedString(@"Unable to download images archive, please check your connection and try again later.", @"Images download failure message") uppercaseString];
        
        double delayInSeconds = 2.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self dismissViewControllerAnimated:YES completion:nil];
        });
    }
    else if([notification.name isEqualToString:@"DECOMPRESS_IMAGES_SUCCESS"]){
        self.titleLabel.text = [NSLocalizedString(@"Image archive decompressed",@"") uppercaseString];
        
        //[self.dataSync saveSettingValue:[NSNumber numberWithBool:YES] forKey:@"imported_assets"];
        
        //[self dismissViewControllerAnimated:YES completion:nil];
    }
    else if([notification.name isEqualToString:@"IMPORT_COMPLETED"]){
        self.titleLabel.text = [NSLocalizedString(@"Image import completed",@"") uppercaseString];
        
        double delayInSeconds = 1.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self dismissViewControllerAnimated:YES completion:nil];
        });
        
        
        
    }
    else if([notification.name rangeOfString:@"FAILURE"].location != NSNotFound){
        NSError *err = notification.object;
        
        if(err){
            
            BTAlert *_al = [[BTAlert alloc] initWithTitle:NSLocalizedString(@"Import error", @"import error alert title") text:err.localizedDescription style:UIAlertViewStyleDefault onConfirm:^(UIAlertView *alert) {
                [self dismissViewControllerAnimated:YES completion:nil];
            } onDismiss:^(UIAlertView *alert) {
                [self dismissViewControllerAnimated:YES completion:nil];
            }];
            [_al show];
            return;
        }
        [self dismissViewControllerAnimated:YES completion:nil];
        
    }
    
}

#pragma mark -

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if([self.importType isEqualToString:@"UPDATE"])
        [[BTDataSync shared] downloadUpdatedImages];
    else //import
        [[BTDataSync shared] retrieveImages];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	
    [self.imageView setOverlayColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.5]];
    [self.imageView setProgress:0.0];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
