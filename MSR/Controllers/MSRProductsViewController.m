//
//  MSRProductsViewController.m
//  MSR
//
//  Created by Davide Cenzi on 16/01/14.
//  Copyright (c) 2014 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import "MSRProductsViewController.h"
#import "MSRProductCell.h"
#import "MSRProductDetailViewController.h"

@interface MSRProductsViewController (){
    NSMutableDictionary *brands;
    StitchTypeProduct *selectedProduct;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;

@end

@implementation MSRProductsViewController
@synthesize choosenType = _choosenType;


- (NSFetchRequest*)fetchRequest{
    
    NSFetchRequest * f = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:[self modelName] inManagedObjectContext:self.managedObjectContext];
    [f setEntity:entity];
    
    // Set the batch size to a suitable number.
    [f setFetchBatchSize:20];
    
    NSPredicate *p = nil;
    NSMutableString *format = [[NSMutableString alloc] init];
    
    [format appendFormat:@"(stitch_type_code = %d) AND (language == '%@')",self.choosenType.codeValue,[[BTDataSync shared] language]];
    
    p = [NSPredicate predicateWithFormat:format];
    [f setPredicate:p];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"item_description" ascending:YES];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    [f setSortDescriptors:sortDescriptors];
    return f;
}

- (NSString*)remotePath{
    return @"";
}

- (NSString*)modelName{
    return @"StitchTypeProduct";
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if(!self.choosenType)
        return;
    
    self.stitchTypeLabel.text = [[NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"Products for stitch type", @"header title for products list"),self.choosenType.item_description] uppercaseString];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    brands = [[NSMutableDictionary alloc] init];
    
    for (Brand *b in [Brand allObjects]) {
        [brands setObject:b.item_description forKey:b.string_code.description];
    }

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    MSRProductCell *c = (MSRProductCell*)cell;
    
    StitchTypeProduct *object = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    NSString *b_name = [brands objectForKey:object.brand];
    
    [c.year_label setFont:[UIFont fontWithName:@"Futura-CondensedMedium" size:22.0]];
    [c.season_label setFont:[UIFont fontWithName:@"Futura-CondensedMedium" size:22.0]];
    [c.brand_label setFont:[UIFont fontWithName:@"Futura-CondensedMedium" size:22.0]];
    [c.model_label setFont:[UIFont fontWithName:@"Futura-CondensedMedium" size:22.0]];
    
    [c.year_value setFont:[UIFont fontWithName:@"Futura-CondensedMedium" size:22.0]];
    [c.season_value setFont:[UIFont fontWithName:@"Futura-CondensedMedium" size:22.0]];
    [c.brand_value setFont:[UIFont fontWithName:@"Futura-CondensedMedium" size:22.0]];
    [c.model_value setFont:[UIFont fontWithName:@"Futura-CondensedMedium" size:22.0]];
    
    c.year_value.text = [object.year description];
    c.season_value.text = [object.season uppercaseString];
    c.brand_value.text = [b_name uppercaseString];
    c.model_value.text = [object.model uppercaseString];
    

    if(self.chooseStitch && self.chooseStitch.hasGenericPictureFile){
        [c.stitch_image setImageWithURL:self.chooseStitch.genericPictureURL placeholderImage:[UIImage imageNamed:@"logo_big.png"]];
    }
    else
        [c.stitch_image setImage:[UIImage imageNamed:@"logo_big.png"]];
    
    if(object.sketchFilePath)
        [c.sketch_image setImageWithURL:object.sketchFileURL placeholderImage:[UIImage imageNamed:@"logo_big.png"]];

    
    if(object.imageFilePath){
        [c.product_image setImageWithURL:object.imageFileURL placeholderImage:[UIImage imageNamed:@"logo_big.png"]];
    }
    else
        [c.product_image setImage:[UIImage imageNamed:@"logo_big.png"]];
 
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = @"productCell";
    
    if(!self.dataSync.signedIn)
        CellIdentifier = @"productCellHidden";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    selectedProduct = [self.fetchedResultsController objectAtIndexPath:indexPath];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(selectedProduct)
        [self performSegueWithIdentifier:@"productDetails" sender:self];

    
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return NO;
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
 
    if([segue.identifier isEqualToString:@"productDetails"]){
        MSRProductDetailViewController *p = [segue destinationViewController];
        p.choosenProduct = selectedProduct;
        p.brandName = [brands objectForKey:selectedProduct.brand];
    }
    
}



@end
