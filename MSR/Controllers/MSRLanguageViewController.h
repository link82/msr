//
//  MSRLanguageViewController.h
//  MSR
//
//  Created by Davide Cenzi on 07/01/14.
//  Copyright (c) 2014 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MSRLanguageViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIPopoverController *popover;

- (IBAction)choosedLanguage:(id)sender;

@end
