//
//  MSRItemsViewController.h
//  MSR
//
//  Created by Davide Cenzi on 13/12/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ECSlidingViewController.h"
#import "BTCollectionViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface MSRItemsViewController : BTCollectionViewController <ECSlidingViewControllerDelegate, UIAlertViewDelegate>{
    NSDictionary *_filters;
}

@property (weak, nonatomic) IBOutlet UIButton *menuButton;
@property (weak, nonatomic) IBOutlet UILabel *resultsLabel;

- (void)setFilters:(NSDictionary *)filtersDictionary;
- (NSDictionary*)filtersDictionary;

- (IBAction)menuButtonTapped:(id)sender;
- (IBAction)imoportButtonTapper:(id)sender;

- (void)launchUpdateImages;

@end
