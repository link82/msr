//
//  MSRImportViewController.m
//  MSR
//
//  Created by Davide Cenzi on 11/12/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import "MSRImportViewController.h"
#import "MSRItemsViewController.h"


@implementation MDRadialProgressView(Miles)

- (void)notifyProgressChange
{
	// Update the accessibilityValue and the progressSummaryView text.
	float percentageCompleted = (100.0f / self.progressTotal) * self.progressCounter;
	
	self.accessibilityValue = [NSString stringWithFormat:@"%.2f", percentageCompleted];
	self.label.text = [NSString stringWithFormat:@"%.0f %%", percentageCompleted];
	
	NSString *notificationText = [NSString stringWithFormat:@"%@ %@",
								  NSLocalizedString(@"Progress changed to:", nil),
								  self.accessibilityValue];
	UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification, notificationText);
}

@end

@interface MSRImportViewController (){
    int progress;
    int total_nr;
    MDRadialProgressView *progressView;
    BTAlert *al;
    NSMutableDictionary *human_messages;
}

- (double)percentProgress;
- (void)setTitleForMessage:(NSString*)message;

@end

@implementation MSRImportViewController

- (MDRadialProgressView *)progressViewWithFrame:(CGRect)frame
{
	MDRadialProgressView *view = [[MDRadialProgressView alloc] initWithFrame:frame];
    
	// Only required in this demo to align vertically the progress views.
    //view.center = CGPointMake(self.view.center.x + 80, view.center.y);
	
	return view;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (IBAction)downloadImages:(id)sender{
    
    [self dismissViewControllerAnimated:YES completion:^{
        
        al = [[BTAlert alloc] initWithTitle:NSLocalizedString(@"Download images?",@"") text:NSLocalizedString(@"Would you like to download updated images?",@"") style:UIAlertViewStyleDefault onConfirm:^(UIAlertView *alert) {

            double delayInSeconds = 0.5;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [[NSNotificationCenter defaultCenter] postNotificationName:@"START_IMAGE_UPDATE" object:nil];
            });
            
            
        } onDismiss:nil];
        [al show];
        
        
    }];
    
}

#pragma mark - Superclass overrides

- (NSArray*)notificationNames{
    NSMutableArray *notifications = [[super notificationNames] mutableCopy];
    
    for (NSString *model in @[@"MACRO_SEASONS",@"SUPPLIERS",@"YARNS",@"MACHINES",@"PRODUCTS",@"BRANDS",@"STITCH_TYPES",@"STITCHES"]) {
        [notifications addObject:[NSString stringWithFormat:@"%@_COUNT",model]];
        [notifications addObject:[NSString stringWithFormat:@"IMPORT_%@_PAGE_SUCCESS",model]];
        [notifications addObject:[NSString stringWithFormat:@"IMPORT_%@_COMPLETED",model]];
    }
    [notifications addObject:@"DECOMPRESS_IMAGES_STARTED"];
    [notifications addObject:@"DECOMPRESS_IMAGES_SUCCESS"];
    [notifications addObject:@"DECOMPRESS_IMAGES_FAILURE"];
    
    [notifications addObject:@"IMPORT_STARTED"];
    [notifications addObject:@"IMPORT_COMPLETED"];
    [notifications addObject:@"IMPORT_FAILURE"];
    
    return notifications;
}
    
- (void)setTitleForMessage:(NSString*)message{
    
    if([human_messages objectForKey:message]){
        self.titleLabel.text = [[human_messages objectForKey:message] uppercaseString];
    }
    else{
        NSLog(@"Warning: missing translation for message %@",message);
    }
}

- (void)handleNotification:(NSNotification *)notification{

    //match i18n entry for current message
    if(![notification.name isEqualToString:@"REQUEST_STARTED"])
    [self setTitleForMessage:notification.name];
    
    if([notification.name isEqualToString:@"IMPORT_STARTED"]){
        progress = 0;
        total_nr = 0;
        [progressView setProgressCounter:[self percentProgress]];
    }
    else if([notification.name rangeOfString:@"COUNT"].location != NSNotFound){
        
        NSNumber *count = (NSNumber*)notification.object;
        total_nr += count.doubleValue;
        NSLog(@"Received counter: %d",total_nr);

    }
    else if([notification.name isEqualToString:@"IMPORT_COMPLETED"]){

        [self.imageView setProgress:[self percentProgress]];
        [self downloadImages:self];
        
        
    }
    else if([notification.name rangeOfString:@"PAGE"].location != NSNotFound){
        progress+= 100;
        NSLog(@"Current progress %d",progress);
        
        if(progress > total_nr) progress = total_nr;
        
        [self.imageView setProgress:[self percentProgress]];
    }
    else if([notification.name rangeOfString:@"COMPLETED"].location != NSNotFound){
        //total_nr = 0;
        //progress = 0;
    }
    
    else if([notification.name isEqualToString:@"DOWNLOAD_IMAGES_STARTED"]){
        self.titleLabel.text = [NSLocalizedString(@"Starting image download...",@"") uppercaseString];
    }
    else if([notification.name isEqualToString:@"DOWNLOAD_IMAGES_PROGRESS"]){
        self.titleLabel.text = [NSLocalizedString(@"Downloading image archive...",@"") uppercaseString];
        
        NSNumber *_progress = (NSNumber*)notification.object;
        [self.imageView setProgress:[_progress doubleValue]];
    }
    else if([notification.name rangeOfString:@"DOWNLOAD_IMAGES_SUCCESS"].location != NSNotFound){
        self.titleLabel.text = [NSLocalizedString(@"Image archive downloaded",@"") uppercaseString];
    }
    // Image download / decompress messages
    else if([notification.name isEqualToString:@"DECOMPRESS_IMAGES_STARTED"]){
        self.titleLabel.text = [NSLocalizedString(@"Decompressing image archive...",@"") uppercaseString];
    }
    else if([notification.name isEqualToString:@"DECOMPRESS_IMAGES_FAILURE"]){
        self.titleLabel.text =[NSLocalizedString(@"Unable to extract images from archive, please close other applications and try again.", @"Unzip failure message") uppercaseString];
        
        double delayInSeconds = 2.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self dismissViewControllerAnimated:YES completion:nil];
        });
        
    }
    else if([notification.name isEqualToString:@"DOWNLOAD_IMAGES_FAILURE"]){
        NSLog(@"Import failure");
        self.titleLabel.text = [NSLocalizedString(@"Unable to download images archive, please check your connection and try again later.", @"Images download failure message") uppercaseString];
        
        double delayInSeconds = 2.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self dismissViewControllerAnimated:YES completion:nil];
        });
    }
    else if([notification.name isEqualToString:@"DECOMPRESS_IMAGES_SUCCESS"]){
        self.titleLabel.text = [NSLocalizedString(@"Image archive decompressed",@"") uppercaseString];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else if([notification.name rangeOfString:@"FAILURE"].location != NSNotFound){
        NSError *err = notification.object;
        
        if(err){
            
            BTAlert *_al = [[BTAlert alloc] initWithTitle:NSLocalizedString(@"Import error", @"import error alert title") text:err.localizedDescription style:UIAlertViewStyleDefault onConfirm:^(UIAlertView *alert) {
                [self dismissViewControllerAnimated:YES completion:nil];
            } onDismiss:^(UIAlertView *alert) {
                [self dismissViewControllerAnimated:YES completion:nil];
            }];
            [_al show];
            return;
        }
        [self dismissViewControllerAnimated:YES completion:nil];
        
    }
    
}

#pragma mark -

- (double)percentProgress{
    double _total = (double)total_nr;
    double _progress_percent = (progress / _total);
    if(_total == 0.0)
        _progress_percent = 0.0;

    return _progress_percent;
}

- (IBAction)increase:(id)sender{
    
    NSLog(@"Current progress: %f",self.imageView.progress);
    double val = self.imageView.progress;
    
    if(self.imageView.progress == 1.0)
        val = 0.0;
    
    val = (val + 0.01);
    
    [self.imageView setProgress:val];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];

    al = [[BTAlert alloc] initWithTitle:@"Importing data?" text:@"Would you like to import all data?" style:UIAlertViewStyleDefault onConfirm:^(UIAlertView *alert) {
        [self.imageView setOverlayColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.5]];
        [self.imageView setProgress:0.0];
        [[BTDataSync shared] syncWithRemote];
        
    } onDismiss:^(UIAlertView *alert) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [al show];

}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    progress = 0;
    total_nr = 0;
    
    
    human_messages = [[NSMutableDictionary alloc] init];
    
    NSDictionary *human_message_format = @{@"%@_COUNT":@"Retrieving %@ count",
                                           @"IMPORT_%@_PAGE_SUCCESS":@"Importing %@...",
                                           @"IMPORT_%@_COMPLETED":@"%@ import completed"};
    
    NSDictionary *human_models = @{@"MACRO_SEASONS":@"Seasons",
                                   @"SUPPLIERS":@"Suppliers",
                                   @"YARNS":@"Yarns",
                                   @"MACHINES":@"Machines",
                                   @"PRODUCTS":@"Final products",
                                   @"BRANDS":@"Customers brand",
                                   
                                   @"STITCH_TYPES":@"Stitch types (Italian)",
                                   @"EN_STITCH_TYPES":@"Stitch types (English)",
                                   @"FR_STITCH_TYPES":@"Stitch types (French)",
                                   
                                   @"STITCHES":@"Stitches (Italian)",
                                   @"EN_STITCHES":@"Stitches (English)",
                                   @"FR_STITCHES":@"Stitches (French)"
                                   };
    
 
    [human_messages setObject:NSLocalizedString(@"Updating started...", @"IMPORT_STARTED") forKey:@"IMPORT_STARTED"];
    [human_messages setObject:NSLocalizedString(@"Updating completed", @"IMPORT_COMPLETED") forKey:@"IMPORT_COMPLETED"];
    [human_messages setObject:NSLocalizedString(@"Image decompression completed", @"DECOMPRESS_IMAGES_SUCCESS") forKey:@"DECOMPRESS_IMAGES_SUCCESS"];
    [human_messages setObject:NSLocalizedString(@"Image decompression failed", @"DECOMPRESS_IMAGES_SUCCESS") forKey:@"DECOMPRESS_IMAGES_FAILURE"];
    
    for (NSString *model in human_models.allKeys) {
        
        for (NSString *message_format in human_message_format.allKeys) {
            
            if([human_message_format objectForKey:message_format]){
                
                NSString *message = [NSString stringWithFormat:message_format, model];
                NSString *human_message = [NSString stringWithFormat:[human_message_format objectForKey:message_format],[human_models objectForKey:model]];
                NSString *localized_human_message =  NSLocalizedString(human_message,message);
                
                if([[[BTDataSync shared] environment] isEqualToString:@"development"])
                    NSLog(@"Adding localized message '%@' for message: '%@'",localized_human_message,message);
                
                [human_messages setObject:localized_human_message forKey:message];
            }
            else{
                NSLog(@"Warning: missing translation for message %@",message_format);
            }
        }

    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
