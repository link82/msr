//
//  MSRDownloadViewController.h
//  MSR
//
//  Created by Davide Cenzi on 12/01/14.
//  Copyright (c) 2014 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import "BTNoficationViewController.h"
#import "BTOverlayImageView.h"


@interface MSRDownloadViewController : BTNoficationViewController

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet BTOverlayImageView *imageView;
@property (nonatomic, assign) NSString *importType;

@end
