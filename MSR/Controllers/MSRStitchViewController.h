//
//  MSRStitchViewController.h
//  MSR
//
//  Created by Davide Cenzi on 21/12/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <QuartzCore/QuartzCore.h>


@interface MSRStitchViewController : UIViewController <UIScrollViewDelegate>{
    int kNumberOfPages;
    BOOL pageControlUsed;
}

@property (weak, nonatomic) IBOutlet UIView *brandView;
@property (nonatomic, strong) NSMutableArray *viewControllers;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (strong, nonatomic) Stitch *stitch;

- (IBAction)close:(id)sender;
- (IBAction)toggleZoom:(id)sender;
- (IBAction)viewProducts:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *code_label;
@property (weak, nonatomic) IBOutlet UILabel *item_description_label;
@property (weak, nonatomic) IBOutlet UILabel *year_label;
@property (weak, nonatomic) IBOutlet UILabel *season_label;
@property (weak, nonatomic) IBOutlet UILabel *composition_label;
@property (weak, nonatomic) IBOutlet UILabel *supplier_label;

@property (weak, nonatomic) IBOutlet UILabel *yarn_label0;
@property (weak, nonatomic) IBOutlet UILabel *yarn_label1;
@property (weak, nonatomic) IBOutlet UILabel *yarn_label2;

@property (weak, nonatomic) IBOutlet UIView *slidingView;

@property (weak, nonatomic) IBOutlet UILabel *brand_label;
@property (weak, nonatomic) IBOutlet UILabel *product_label;
@property (weak, nonatomic) IBOutlet UILabel *stitch_type_label;
@property (weak, nonatomic) IBOutlet UILabel *stretch_label;
@property (weak, nonatomic) IBOutlet UILabel *vanise_label;
@property (weak, nonatomic) IBOutlet UILabel *thinness_label;

@property (weak, nonatomic) IBOutlet UIButton *products_button;

@end
