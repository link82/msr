//
//  MSRProductsViewController.h
//  MSR
//
//  Created by Davide Cenzi on 16/01/14.
//  Copyright (c) 2014 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import "BTSyncTableViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface MSRProductsViewController : BTSyncTableViewController

@property (nonatomic, strong) StitchType *choosenType;
@property (nonatomic, strong) Stitch *chooseStitch;
@property (nonatomic, weak) IBOutlet UILabel *stitchTypeLabel;

@end
