//
//  MSRTestViewController.h
//  MSR
//
//  Created by Davide Cenzi on 07/12/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MSRTestViewController : UIViewController

- (IBAction)import:(id)sender;
- (IBAction)changeLanguage:(id)sender;
@end
