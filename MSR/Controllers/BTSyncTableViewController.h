//
//  BTSyncTableViewController.h
//  NeedleIOS
//
//  Created by Davide Cenzi on 11/07/13.
//  Copyright (c) 2013 Davide Cenzi. All rights reserved.
//

#import "BTSyncViewController.h"


@interface BTSyncTableViewController : BTSyncViewController <UITableViewDataSource,UITableViewDelegate,NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (nonatomic,weak) IBOutlet UITableView *tableView;

- (void)refreshData;
- (NSFetchRequest*)fetchRequest;

@end
