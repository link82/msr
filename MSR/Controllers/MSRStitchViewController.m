//
//  MSRStitchViewController.m
//  MSR
//
//  Created by Davide Cenzi on 21/12/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import "MSRStitchViewController.h"
#import "MSRZoomViewController.h"
#import "MSRProductsViewController.h"

@interface MSRStitchViewController (){
    UIImage *selectedImage;
}

@end

@implementation MSRStitchViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - paged scrollview methods

- (UIImageView *)viewForPicture:(Picture*)picture{
    UIImageView *tmp = [[UIImageView alloc] initWithFrame:self.scrollView.frame];
    
    if(picture && picture.filePath){
        //if([[BTDataSync shared] forceRemoteAssets])
            [tmp setImageWithURL:picture.fileURL placeholderImage:[UIImage imageNamed:@"logo_big.png"]];
        //else
        //    [tmp setImage:[UIImage imageWithContentsOfFile:picture.filePath]];
    }
    
    tmp.contentMode = UIViewContentModeScaleAspectFit;
    tmp.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;

    return tmp;
}


- (void)loadScrollViewWithPage:(int)page {
    if (page < 0) return;
    if (page >= kNumberOfPages) return;
    
    
    
    UIImageView *controller = (UIImageView*)[NSNull null];
    if([self.viewControllers count] > page)
        controller = [self.viewControllers objectAtIndex:page];
    else
        [self.viewControllers addObject:[NSNull null]];
    
    if ((NSNull *)controller == [NSNull null]) {

        controller = [self viewForPicture:[self.stitch.sortedPictures objectAtIndex:page]];
        [self.viewControllers replaceObjectAtIndex:page withObject:controller];
    }
    
    if (nil == controller.superview) {
        
        CGRect frame = self.scrollView.frame;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0;
        
        controller.frame = frame;
        [self.scrollView addSubview:controller];
    }
}

- (void)setupSlider{

    if(!self.stitch || [self.stitch.sortedPictures count] == 0)
        return;
    
    kNumberOfPages = self.stitch.pictures.count;
    
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width * kNumberOfPages, self.scrollView.frame.size.height);
    
    self.pageControl.numberOfPages = kNumberOfPages;
    self.scrollView.pagingEnabled = YES;
    
    
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.showsVerticalScrollIndicator = NO;
    self.scrollView.scrollsToTop = NO;
    self.scrollView.delegate = self;
    
    
    self.scrollView.clipsToBounds = YES;
    self.scrollView.bounces = YES;
    self.scrollView.scrollEnabled = YES;
    
    self.pageControl.currentPage = 0;

    //preparo per il page control
    NSMutableArray *controllers = [[NSMutableArray alloc] init];
    for (unsigned i = 0; i < kNumberOfPages; i++) {
        [controllers addObject:[NSNull null]];
    }
    self.viewControllers = controllers;
    
    
    [self loadScrollViewWithPage:0];
    
    if(kNumberOfPages == 2)
        [self loadScrollViewWithPage:1];
    
    
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toggleZoom:)];
    singleTap.cancelsTouchesInView = NO;
    [self.scrollView addGestureRecognizer:singleTap];
    
}


#pragma delegato scrollview

- (void)scrollViewDidScroll:(UIScrollView *)sender {
    if (pageControlUsed) {
        return;
    }
    CGFloat pageWidth = self.scrollView.frame.size.width;
    int page = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    self.pageControl.currentPage = page;
    
    @try {
        [self loadScrollViewWithPage:page - 1];
        [self loadScrollViewWithPage:page];
        [self loadScrollViewWithPage:page + 1];
    }
    @catch (NSException *exception) {
        NSLog(@"Errore: %@",[exception description]);
    }
    
    
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    pageControlUsed = NO;
}

-(IBAction)close:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)toggleZoom:(id)sender{
    
    UIImageView *img = [self.viewControllers objectAtIndex:self.pageControl.currentPage];
    selectedImage = img.image;
    [self performSegueWithIdentifier:@"zoom" sender:self];
}

- (void)loadDetails{
    
    if(!self.stitch)
        return;
    
    
    self.code_label.text = self.stitch.string_code;
    self.item_description_label.text = self.stitch.item_description;
    self.year_label.text = [self.stitch.year description];
    self.season_label.text = self.stitch.season.item_description;
    self.composition_label.text = self.stitch.composition;
    self.supplier_label.text = (self.stitch.supplier ? self.stitch.supplier.cleanedDescription : NSLocalizedString(@"Not specified",@""));
    
    //NEED TO FIX
    NSArray *yarns = self.stitch.yarns;
    NSArray *labels = @[self.yarn_label0,self.yarn_label1,self.yarn_label2];

    self.brand_label.text = self.stitch.brand.item_description;
    self.product_label.text = (self.stitch.product ? self.stitch.product.item_description : NSLocalizedString(@"Not specified",@""));
    self.stitch_type_label.text = self.stitch.stitch_type.item_description;
    self.stretch_label.text = (self.stitch.stretchValue ? NSLocalizedString(@"YES",@"") : NSLocalizedString(@"NO",@""));
    self.vanise_label.text = (self.stitch.vaniseValue ? NSLocalizedString(@"YES",@"") : NSLocalizedString(@"NO",@""));
    self.thinness_label.text = [self.stitch.thinness description];

    self.yarn_label0.text = self.yarn_label1.text = self.yarn_label2.text = @"";
    
    CGFloat top = 224.0 + 30.0 + (float)[yarns count] * 30.0;
    
    for (int i = 0; i < [yarns count]; i++) {
        if([labels count] <= i)
            break;
        
        Yarn *y = [yarns objectAtIndex:i];
        UILabel *l = [labels objectAtIndex:i];
        l.text = [y item_description];
    }
    
    
    double delayInSeconds = 0.5;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        [UIView animateWithDuration:0.3 animations:^{
            
            self.slidingView.frame = CGRectMake(self.slidingView.frame.origin.x, top, self.slidingView.frame.size.width, self.slidingView.frame.size.height);
        }];
        
    });
    
    [self.products_button setHidden:(self.stitch.stitch_type.products.count == 0)];
    
    
}

- (IBAction)viewProducts:(id)sender{
    [self performSegueWithIdentifier:@"productList" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"zoom"]){
        MSRZoomViewController *tmp = [segue destinationViewController];
        tmp.currentImage = selectedImage;
        tmp.titleText = [NSLocalizedString(@"Stitch detail",@"") uppercaseString];
    }
    else if([segue.identifier isEqualToString:@"productList"]){
        MSRProductsViewController *p = [segue destinationViewController];
        p.choosenType = self.stitch.stitch_type;
        p.chooseStitch = self.stitch;
    }
}

#pragma mark -
- (void)checkBrandsVisibility{
    NSLog(@"Checking brand visibility based on user authentication: %hhd",[BTDataSync shared].signedIn);
    if(![BTDataSync shared].signedIn)
        [self.brandView setHidden:YES];
    else
        [self.brandView setHidden:NO];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self checkBrandsVisibility];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem.title = @"";
    self.navigationItem.backBarButtonItem.title = @"";
    //self.navigationItem.title = @"MILES STITCH RESEARCH";
    
    CALayer *l = self.products_button.layer;
    
    [l setBorderColor:[UIColor whiteColor].CGColor];
    [l setBorderWidth:1.0];
    
    [self setupSlider];
    [self loadDetails];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
